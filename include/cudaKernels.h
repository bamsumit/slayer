/*
 * Author: Sumit Bam Shrestha
 * 07/11/2017 11:30 AM
 * Has function definition of convolution and multiplcation routine between two vector of floats
 * assuming filter.size() << signal.size()
 * assiming convllution filter is on right handed
 */
 
#ifndef CUDAKERNELS_H_INCLUDED
#define CUDAKERNELS_H_INCLUDED

// #include <cublas_v2.h>
#include <cublasXt.h>
#include <cudnn.h>
#include <curand.h>
#include <string>
#include <slayerType.h>

// #ifndef PROFILESLAYER
// #define PROFILESLAYER
// #endif

class slayerType;

typedef struct{ unsigned x, y, z, w;	} dim4;

// CUDA error check 
#define checkCudaErrors(err) { checkCudaErrors_((err), __FILE__, __LINE__); }
void checkCudaErrors_(cudaError_t err, const char *file, int line);

// CUBLAS error check 
#define checkCublasErrors(err) { checkCublasErrors_((err), __FILE__, __LINE__); }
void checkCublasErrors_(cublasStatus_t status, const char *file, int line);

// CUDNN error check 
#define checkCudnnErrors(err) { checkCudnnErrors_((err), __FILE__, __LINE__); }
void checkCudnnErrors_(cudnnStatus_t status, const char *file, int line);

// CURAND error check
#define checkCurandErrors(err) { checkCurandErrors_((err), __FILE__, __LINE__); }
void checkCurandErrors_(curandStatus_t status, const char *file, int line);

void handleCublasStatus(cublasStatus_t status, std::string location);

void handleCudaErrorCheck(std::string location);

__global__ void sumReduceKernel(float* d_data, int N);

float sumReduce(float* d_data, int N);

template <class T1, class T2>
__global__ void memsetKernel(T1* var, T2 value, unsigned length)
{
	unsigned tID = blockIdx.x * blockDim.x + threadIdx.x;
	if(tID < length)	var[tID] = value;
	return;
}

__global__ void addKernel(float* difference, float substrahend, unsigned numel);

__global__ void diffKernel(float* difference, const float* substrahend, unsigned xDim, unsigned yDim);

__global__ void multKernel(float* product, const float* arg1, unsigned xDim, unsigned yDim);

__global__ void convertFp32ToFp16Kernel (half *out, float *in, int n, float scale = 1.0f);

__global__ void convertFp16ToFp32Kernel (float *out, half *in, int n, float scale = 1.0f);

__global__ void scaleKernel(float *data, float factor, int n);

inline void convertFp32ToFp16(half *out, float *in, int n, float scale = 1.0f);

inline void convertFp16ToFp32(float *out, half *in, int n, float scale = 1.0f);

inline void scale(float *data, float factor, int n);

template <class T>
__global__ void errKernel(float *err, T *var1, T *var2, unsigned nitems);

template <class T>
float maxErr(T *var1, T *var2, unsigned int nitems);

unsigned getComputeCapability(unsigned deviceID);

unsigned getComputeCapability(std::vector<unsigned> deviceID);

std::string getGPUspecs();

// inline functions

inline void convertFp32ToFp16(half *out, float *in, int n, float scale)
{
	convertFp32ToFp16Kernel<<< (n + 255)/256, 256 >>>(out, in, n, scale);
}

inline void convertFp16ToFp32(float *out, half *in, int n, float scale) 
{
	convertFp16ToFp32Kernel<<< (n + 255)/256, 256 >>>(out, in, n, scale);
}

inline void scale(float *data, float factor, int n)
{
	scaleKernel<<< (n + 255)/256, 256 >>>(data, factor, n);
}

// template functions

template <class T>
__global__ void errKernel(float *err, T *var1, T *var2, unsigned nitems)
{
	unsigned i = blockIdx.x * blockDim.x + threadIdx.x;
	if(i >= nitems)	return;
	err[i] = (2 * (var1[i] > var2[i]) - 1) * (var1[i] - var2[i]) / (var1[i] * var1[i] + 1.0f);
	return;
}

template <class T>
float maxErr(T *var1, T *var2, unsigned int nitems)
{
	float *err;
	checkCudaErrors( cudaMallocManaged(&err, sizeof(*err) * nitems) );
	unsigned thread = 32;
	errKernel<<< ceil(1.0f * nitems / thread), thread >>>(err, var1, var2, nitems);
	checkCudaErrors( cudaDeviceSynchronize() );
	int maxIdx = 0;
	cublasHandle_t handle;
	checkCublasErrors( cublasCreate(&handle) ); 
	cublasIsamax(handle, nitems, err, 1, &maxIdx);
	checkCudaErrors( cudaDeviceSynchronize() );
	float max = err[maxIdx];
	// std::cout << maxIdx << "  " << var1[maxIdx] << "  " << var2[maxIdx] << std::endl;
	checkCublasErrors( cublasDestroy(handle) );
	checkCudaErrors( cudaFree(err) );
	checkCudaErrors(cudaDeviceSynchronize());
	return max;
}

#endif //CUDAKERNELS_H_INCLUDED
/*
 * Author: Sumit Bam Shrestha
 * 26/09/2017 2:30 PM
 * Has definition for slayer (spike layer) class. It contains the engine for forward propagation and backpropagation of the layer type.
 * The object has different properties depending upon the slayerType it is assigned.
 * A slayerType can be any of these: Linear, Planar, Convolution, Aggregation, Output
 * The links to the actual weights and delays are stored via index values.
 *
 * Author: Sumit Bam Shrestha
 * 26/12/2017 6:00 PM
 * Changed the code to use unified memory space
 */

#ifndef SLAYER_H_INCLUDED
#define SLAYER_H_INCLUDED


#include <vector>
#include <string>
#if defined SLAYERMEX
	#include <mex.h>
	#include "mexutils.h"
#endif
#include <slayerType.h>
#include <cudaKernels.h>
#include <optimizerKernels.h>
#include <cudnnDescriptor.h>

// static std::mutex barrier;

/***** class declaration *****/
class slayer;
class slayerType;
class classificationEngine;


/*****************************/
/***** slayer definition *****/
/*****************************/
class slayer
{
	friend class slnetwork;
	friend class classificationEngine;
public:
	slayerType info;
private:
	bool        profile;
	unsigned 	*wInd;
	unsigned 	*dInd;
	float 		theta;
	float 		sigma;
	float 		tauRho;
	float		scaleRho;
	unsigned    batchSize;
	unsigned    batchStride;
	unsigned	Ns;
	float 		Ts;
	unsigned    WSize;
	unsigned    dSize;
	unsigned 	epsSize;
	unsigned 	epsDotSize;
	unsigned 	nuSize;
	unsigned    sSize;
	unsigned    aSize;
	unsigned    aDotSize;
	unsigned    uSize;
	unsigned    eSize;
	unsigned    rhoSize;
	unsigned    deltaSize;
	unsigned    nSpikesSize;
	unsigned    totalSpikesSize; 
	unsigned    classIDSize;		
	unsigned    probabilitySize;
	unsigned    deviceID;
	gradAlgoType gradAlgo;
	/***** GPU variables *****/
	float* W;
	float* d;
	float* gradW;
	float* gradD;
	float* meanSquareW;
	float* meanSquareD;
	float* momentumW;
	float* momentumD;
	float* eps;
	float* epsDot;
	float* nu;
	float* t;
	float* s;
	float* a;
	float* aDot;
	float* u;
	float* e;
	float* rho;
	float* delta;
	unsigned* nSpikes;
	unsigned* totalSpikes;
	unsigned* classID;
	float* probability;
	// std::vector<float*> epsInDevice;
	// std::vector<float*> epsDotInDevice;
	// std::vector<float*> nuInDevice;
	/***** GPU variables *****/
	cublasHandle_t handle;
	bool cublasHandleCreated;
	// std::vector<cublasHandle_t> handle;
	// std::vector<cublasXtHandle_t> handleXt;
	cudnnDescriptor_t cudnn;
	slayer *pre;
	slayer *post;
	// void cudaDeviceSynchronizeAll();
	void evalRho();
	void  numSpikesEventToTensor(const unsigned batchIndex, const slayer &spikeLayer, 
								 const std::vector<unsigned> neuronID, 
								 const std::vector<float> tSt, 
								 const std::vector<float> tEn, 
								 const std::vector<unsigned> numSpikes, const unsigned tSampleLen);
	float errorSpikeTime (slayer &spikeLayer);
	float errorNumSpikes (slayer &spikeLayer);
	float errorProbSpikes(slayer &spikeLayer);
	void  backPropSlayer();
	void  backPropSlayerZero();
	void  backPropSlayerRefractory();
public:
	slayer();
	slayer(slayerType layerInfo, float Theta, float Sigma, float TauRho, float ScaleRho, gradAlgoType GradAlgo,
		   std::vector<float> time, std::vector<float> srmKernel, std::vector<float> srmKernelDot, std::vector<float> refKernel, unsigned batchSize);
	slayer& operator = (const slayer &layer); 
	int   setupSynapse(std::vector<float> weight, std::vector<float> delay);
	void  loadSpikes    (std::string inPath, std::vector<unsigned> index, float tSample);
	void  loadAppxSpikes(std::string inPath, std::vector<unsigned> index, const slayer &spikeLayer, float tSample);
	void  loadAvgSpikes (std::string inPath, std::vector<unsigned> index, float tSample);
	void  loadNumSpikes (std::string inPath, std::vector<unsigned> index, const slayer &spikeLayer, float tSample);
	void  loadNumSpikes (std::vector<unsigned> index, const slayer &spikeLayer, const YAML::Node tgtSpikeRegion, const YAML::Node tgtSpikeCount, float tSample);
	void  loadProbSpikes(std::vector<unsigned> index, slayer &spikeLayer, float tSample, unsigned numSamplesInWindow);
	void  fwdProp ();
	float error(slayer &spikeLayer, tgtPattern patternType);
	void  backProp();
	void  gdUpdate(float etaW, float lambda);
	void  gdUpdate(float etaW, float etaD, float lambda);
	void  rmsPropUpdate(float etaW, float lambda);
	void  rmsPropUpdate(float etaW, float etaD, float lambda);
	void  adamUpdate(float etaW, float lambda, float beta1, float beta2, unsigned iter, float epsilon);
	void  adamUpdate(float etaW, float etaD, float lambda, float beta1, float beta2, unsigned iter, float epsilon);
	void  nadamUpdate(float etaW, float lambda, float beta1, float beta2, unsigned iter, float epsilon);
	void  nadamUpdate(float etaW, float etaD, float lambda, float beta1, float beta2, unsigned iter, float epsilon);
	void  weightLimit();	// distribute weight to have certain statisitics
	void  delayLimit();		// distribute delay to have certain statistics
	void  updateWvec(std::vector<float> &weight);
	void  updateDvec(std::vector<float> &delay);
	/***** ToDO *****/
	inline unsigned index(unsigned x, unsigned y, unsigned z);	// should give linear index of all the variables except weight
	/***** End TODO *****/
	#if defined mex_h
	       void copyTo     (mxArray *mxPtr, layerVar var);
	inline void copySpikeTo(mxArray *mxPtr)
	{	copyTo(mxPtr, SPIKE);	}
	#endif
	std::vector<float> get(layerVar var);
	void printWeights();
	void setDevices(unsigned device);
	inline unsigned spikeCount()
	{	cudaSetDevice(deviceID);	return static_cast<unsigned>( sumReduce(s, sSize) * Ts );		}
	inline float avgWeight()
	{	return sumReduce(W, info.nWeights * info.nDelayedSyn) / (info.nWeights * info.nDelayedSyn);	}
	#if defined mex_h
		inline void plotRaster(unsigned figureID)
		{	rasterPlot(this->s, Ts, figureID, Ns, sSize);	}
	#endif
	std::vector<float> fpruntime;
	std::vector<float> bpruntime;
	std::vector<float> erruntime;
	std::vector<unsigned> nanCount;
	~slayer();
};

#endif //SLAYER_H_INCLUDED
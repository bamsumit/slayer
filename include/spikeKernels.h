/*
 * Author: Sumit Bam Shrestha
 * 12/03/2018 12:00 PM
 * Contains routines that converts membrane potential of neuron into spikes
 */
 
#ifndef SPIKEKERNELS_H_INCLUDED
#define SPIKEKERNELS_H_INCLUDED

__global__ void ahpKernel(float* d_u, const float* d_nu, unsigned nuSize);
__global__ void getSpikesKernel(float* d_s, float* d_u, const float* d_nu, unsigned nBatch, unsigned nNeurons, unsigned nuSize, unsigned batchStride, unsigned Ns, float theta, float Ts);
void getSpikes(float* d_s, float* d_u, const float* d_nu, unsigned nNeurons, unsigned nuSize, unsigned Ns, unsigned batchStride, float theta, float Ts);
__global__ void evalRhoKernel(float* d_rho, const float* d_u, float theta, float tau, unsigned nNeurons, unsigned Ns, float scale = 1.0);
__global__ void evalDeltaRefractoryKernel(float* delta, float* deltaBar, float* rho, float* nu, unsigned nNeurons, unsigned nuSize, unsigned Ns, float Ts);
__global__ void evalDeltaRefractoryKernel(float* delta, float* deltaBar, float* rho, float* nu, unsigned nBatch, unsigned nNeurons, unsigned nuSize, unsigned batchStride, unsigned Ns, float Ts);
void evalDeltaRefractory(float* delta, float* rho, float* nu, unsigned nNeurons, unsigned nuSize, unsigned Ns, float Ts);
void evalDeltaRefractory(float* delta, float* rho, float* nu, unsigned nNeurons, unsigned nuSize, unsigned batchStride, unsigned Ns, float Ts);

#endif // SPIKEKERNELS_H_INCLUDED
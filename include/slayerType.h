/*
 * Author: Sumit Bam Shrestha
 * 13/12/2017 11:30 AM
 * Has function definition of slayerType class
 * It contains the information about the dimensions of spike layer
 */
 
#ifndef SLAYERTYPE_H_INCLUDED
#define SLAYERTYPE_H_INCLUDED
 
#include <vector>
#include <string>
#include <sstream>
#include <slayerconfig.h>

#ifndef MAXBUF
	#define MAXBUF 100
#endif

/***** enumerator type definition for layer type definition *****/
enum layerType {Linear, Planar, Convolution, Aggregation, Output};

/*********************************/
/***** slayerType definition *****/
/*********************************/
class slayerType
{
	friend class classificationEngine;
	friend class slayer;
	friend class slnetwork;
	friend class slayerConfig;
private:
	std::vector<unsigned> parameter;		// Contains parameters specific to the layer for description. i.e. contains the numbers in this description: 28x28-12c5-2a-64c5-2a-10o for each layer
	unsigned 		 dimension[3];	// Contains the x, y and z dimension of the neurons in the layer
	layerType 		 type;			// Defines the type of layer: Linear, Planar, Convolution, Aggregation, Output
	unsigned 		 nNeurons;		// No of neurons in the layer. It is equal to the product of dimensions[0:2]
	unsigned		 nWeights;		// No of weights in the layer
	unsigned 		 nDelays;		// No of delays in the layer
	unsigned 		 nDelayedSyn;	// No of delayed synaptic connections
public:
	slayerType& operator = (const slayerType &layerType);
	std::string description();			// This function returns a string describing the layer	
	std::string typeStr();				// This function returns the string type text		
	unsigned dim(unsigned ind)
	{	return ( (ind < 3) ? dimension[ind] : 0 );	}
	unsigned par(unsigned ind)
	{	return ( (ind < parameter.size()) ? parameter[ind] : 0 );	}
	unsigned numNeurons()
	{	return nNeurons;	}
	unsigned numDelSyn()
	{	return nDelayedSyn;	}
};

 
#endif // SLAYERTYPE_H_INCLUDED
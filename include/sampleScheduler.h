/*
 * Author: Sumit Bam Shrestha
 * 11/10/2017 10:00 AM
 * Has definition for scheduler engine that reads the sample ID and its corresponding class to be used for current iteration.
 * For both training and testing samples.
 */

#ifndef SAMPLESCHEDULER_H_INCLUDED
#define SAMPLESCHEDULER_H_INCLUDED

#include<iostream>
#include<sstream>
#include<fstream>
#include<string>
#include<vector>

class sampleScheduler
{
private:
	std::ifstream fin;
	unsigned samplesPerBatch;	// no of samples in a mini batch. 1=> Stochastic Gradient Descent
public:
	sampleScheduler();
	int initialize(std::string filename, unsigned numSample);
	int loadSamples(std::vector<unsigned> &inID, std::vector<unsigned> &classID);
	~sampleScheduler();
};

#endif // SAMPLESCHEDULER_H_INCLUDED

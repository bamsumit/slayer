/*
 * Author: Sumit Bam Shrestha
 * 21/08/2018 1:30 PM
 * This code defines a class called cudnnDescriptor_t
 * It is a helper class for managing the handles and descriptor of cudnn variables for convolution operations, both forward and backward 
 * First version compiled on 31/08/2018
 * extensively checked with pervious version of routines used in SLAYER for consistency
 * Maximum error of appx 5% was observed in the calculated values
 * needs separate compilation of code for utilization of tensor cores
 */


#ifndef CUDNNDESCRIPTOR_H
#define CUDNNDESCRIPTOR_H

#include <cudaKernels.h>

class cudnnDescriptor_t{
public:
	cudnnHandle_t cudnnHandle;
	cudnnTensorDescriptor_t 		inputDesc;
	cudnnTensorDescriptor_t 		outputDesc;
	cudnnFilterDescriptor_t 		filterDesc;
	cudnnConvolutionDescriptor_t 	convDesc;
	cudnnTensorDescriptor_t 		halfInputDesc;
	cudnnTensorDescriptor_t 		halfOutputDesc;
	cudnnFilterDescriptor_t 		halfFilterDesc;
	
	void* halfInput;
	void* halfOutput;
	void* halfFilter;
	
	dim4 iDim;
	dim4 oDim;
	dim4 fDim;
	
	cudnnConvolutionFwdAlgo_t 		fwdAlgo;
	cudnnConvolutionBwdDataAlgo_t	bwdDataAlgo;
	cudnnConvolutionBwdFilterAlgo_t bwdFilterAlgo;
	size_t fwdWorkspaceBytes;
	size_t bwdDataWorkspaceBytes;
	size_t bwdFilterWorkspaceBytes;
	void* workspace;
	cudnnDescriptor_t();
	~cudnnDescriptor_t();
	void setDescriptors(dim4 inDim, dim4 outDim, dim4 filterDim, unsigned deviceComputeCapability, cudnnDataType_t computePrecision = CUDNN_DATA_FLOAT);
	void allocateWorkspace();
	void fwdConvolution(void* input, void* output, void* filter);
	void bwdConvolutionData(void* dInput, void* dOutput, void* filter);
	void bwdConvolutionFilter(void* input, void* dOutput, void* dFilter, float scale = 1.0f);
	void testCudnnDescriptor();
};

#endif // CUDNNDESCRIPTOR_H
/*
 * Author: Sumit Bam Shrestha
 * 26/09/2017 2:30 PM
 * Has definition for slnetwork (spike learn network) class. It contains the parameter definition of spiking neural network
 * Has routines to get input parameters from MATLAB workspace to MEX workspace
 * Has definition of slayerType (spike layer type) class. It contans the description of spiking neuron network layer.
 */

#ifndef SLNETWORK_H_INCLUDED
#define SLNETWORK_H_INCLUDED


#include <vector>
#include <string>
#include <slayer.h>
#include <slayerio.h>
#include <sampleScheduler.h>
#include <classification.h>

#define EPSILON 0.01

/***** class declaration *****/
class slnetwork;	// This class has overall description of the spiking neural network parameters, architecture and learning parameters

/********************************/
/***** slnetwork definition *****/
/********************************/
class slnetwork
{
private:
	YAML::Node config;
	unsigned nLayers;			// Contains no of layers in the network
	std::vector<float> srmKernel     (float tau, 			  float minVal = EPSILON);	// the spike response kernel
	std::vector<float> refKernel     (float tau, float theta, float minVal = EPSILON);	// the refractory response kernel
	std::vector<float> srmKernelDiff (float tau, 			  float minVal = EPSILON);	// the derivative of spike response kernel
	std::vector<float> refKernelDiff (float tau, float theta, float minVal = EPSILON);	// the derivative of refractory response kernel
public:
	std::vector<slayer> layer;	// the layers of the neural network
	slayer layerDes;			// the desired output of the neural network
	float Ts;					// sampling time
	float tEnd;					// end time of mini batch spike simualtion. tEnd = tSample*nSample
	float tSample;				// maximum sample time
	unsigned nSample;			// no of samples in a mini batch. 1=> Stochastic Gradient Descent
	unsigned maxIter;			// maximum number of iterations
	float minCost;				// minimum value of cost to attain
	std::string inPath;			// path of input spike data
	std::string outPath;		// path to output spike data
	std::string desiredPath;	// path of desired spike pattern data for each class
	std::string trainList;		// path of list of indices of training samples; must contain input sample ID and desired class ID
	std::string testList;		// path of list of indices of testing samples; must contain input sample ID and desired class ID
	srkType neuronType;			// the model of neuron
	tgtPattern patternType;		// type of desired pattern
	updateModeType updateMode;	// parameter update method
	gradAlgoType gradAlgo;		// gradient calculation aglorithm
	std::vector<float> weight;	// vector of weights of the network. All the weights of the network corresponds to some index value in this vector
	std::vector<float> delay;	// vector of delays of the network. ALl the delays of the newtowk corresponds to some index value in this vector
	sampleScheduler trScheduler;// scheduler to read training sample ID and class ID from file
	sampleScheduler teScheduler;// scheduler to read testing sample ID and class ID from file
	classificationEngine classifier;	// the classification engine
	bool testing;				// to do testing or not
	bool classification;		// to do classification or not
	/***** ADD METHODS TO WRITE SOME SIMULATION DATA INTO FILES FOR VERIFICATION *****/
	
	slnetwork();
	slnetwork(slayerConfig configDescriptor);	// reads the description fields slayerConfig
	inline void configDisplay()					// this function displays the network configuration and learning configuration
	{	slayerConfig::display(config, true);	slayerio::cout << std::endl;	}
	unsigned displayProfileData();
	void loadDesiredSpikes(std::vector<unsigned> classInd);
	void updateParams(unsigned iter);
	std::vector<float> getWvec();	// these functions will return the current weight and delay that have been learned
	std::vector<float> getDvec();	//
	void updateWvec();
	void updateDvec();
	std::string writeFileName();
	void writeWeightDelay();	// write the weight and delay into file
	void writeNetworkConfig();
	// ~slnetwork(); // till now no destructor seems to be needed
};

#endif //SLNETWORK_H_INCLUDED
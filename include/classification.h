/*
 * Author: Sumit Bam Shrestha
 * 13/10/2017 2:30 PM
 * Has routines for accuracy engine
 */

#ifndef CLASSIFICATION_H_INCLUDED
#define CLASSIFICATION_H_INCLUDED

#include <vector>
#include <string>
#include <slayer.h>

class classificationEngine
{
private: 
	tgtPattern patternType;
	float tSample;
	std::vector<slayer> idealClass;
	unsigned threshold;
	unsigned getNumSpikes(std::string desiredPath, unsigned classID);
	std::vector< std::vector<unsigned> > getNumSpikes(const slayer &outputLayer);
public:
	void initialize(std::string desiredPath, unsigned numOutputs, tgtPattern pattern, float tSample);
	void initialize(YAML::Node error       , unsigned numOutputs, tgtPattern pattern, float Tsample);
	std::vector<unsigned> outputClass(const slayer &outputLayer, const std::vector<unsigned> &groundTruth);
	unsigned numCorrectIn(const slayer &outputLayer, const std::vector<unsigned> &groundTruth);
	unsigned numCorrectIn(const std::vector<unsigned> &classID, const std::vector<unsigned> &groundTruth);
};



#endif // CLASSIFICATION_H_INCLUDED
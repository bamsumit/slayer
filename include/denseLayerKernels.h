/*
 * Author: Sumit Bam Shrestha
 * 08/03/2018 2:00 PM
 * Has routines for forward propagation, signal backpropagation and parameter backpropagation routines for dense (fully connected) layer
 * forward propagation    : multWa(...)
 * signal backpropagation : multWdelta(...)
 * weight backpropagation : multAdelta(...)
 * delay backpropagation  : multAdotDelta(...)
 */
 
#ifndef DENSELAYERKERNELS_H_INCLUDED
#define DENSELAYERKERNELS_H_INCLUDED

#include <cudaKernels.h>

/***** Signal Forward Propagation *****/
void multWa(cublasHandle_t &handle, float* d_u, const float* d_W, const float* d_a, unsigned Nout, unsigned Nin, unsigned Ns);

/***** Signal Backpropagation *****/
// void multWdelta(cublasHandle_t &handle, float* d_e, const float* d_W, const float* d_delta, unsigned Nout, unsigned Nin, unsigned Ns)
void multWdelta(cublasHandle_t &handle, float* d_e, const float* d_W, const float* d_delta, unsigned Ne, unsigned Nd, unsigned Ns);

/***** Weight Backpropagation *****/
void multAdelta(cublasHandle_t &handle, float* d_gradW, const float* d_delta, const float* d_a, unsigned Nout, unsigned Nin, unsigned Ns, float Ts);

/***** Delay Backpropagation *****/
void multAdotDelta(cublasHandle_t &handle, float* d_gradD, const float* d_e, float* d_aDot, unsigned nNeurons, unsigned nDelayedSyn, unsigned Ns, float Ts);

#endif // DENSELAYERKERNELS_H_INCLUDED
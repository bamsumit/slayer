/*
 * Author: Sumit Bam Shrestha
 * 26/09/2017 12:00 PM
 * Has definition of functions to read and write spike event data into binary file.
 *
 * 1D Spike representation:
 * Each spike event is represented by a 40 bit number.
 * The first 16 bits (bits 39-24) represent the neuronID.
 * Bit 23 represents the sign of spike event: 0=>OFF event, 1=>ON event
 * The last 23 bits (bits 22-0) represent the spike event timestamp in microseconds
 *
 */

#ifndef SPIKEIO_H_INCLUDED
#define SPIKEIO_H_INCLUDED

#include<fstream>
#include<string>
#include<vector>

// function declarations:
unsigned read1DBinSpikes(std::string filePath, std::vector<unsigned> &neuronID,  std::vector<float> &timeID, std::vector<char> &polarity);
void    write1DBinSpikes(std::string filePath, std::vector<unsigned> &neuronID,  std::vector<float> &timeID, std::vector<char> &polarity);
unsigned read1DNumSpikes(std::string filePath, std::vector<unsigned> &neuronID,  std::vector<float> &tSt,    std::vector<float> &tEn, std::vector<unsigned> &nSpikes);
void    write1DNumSpikes(std::string filePath, std::vector<unsigned> &neuronID,  std::vector<float> &tSt,    std::vector<float> &tEn, std::vector<unsigned> &nSpikes);
unsigned generate1DNumSpikes(std::vector<unsigned> &neuronID, std::vector<float> &tSt, std::vector<float> &tEn, std::vector<unsigned> &nSpikes, 
							 unsigned classID, unsigned nNeurons, int trueSpikes, int falseSpikes, float regionStart, float regionStop);
unsigned read2DBinSpikes(std::string filePath, std::vector<unsigned char> &xPos, std::vector<unsigned char> &yPos, std::vector<float> &timeID, std::vector<char> &polarity);

#endif

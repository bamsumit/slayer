/*
 * Author: Sumit Bam Shrestha
 * 12/03/2018 12:00 PM
 * Contains routines for optimization routines
 * Gradient Descent
 * RmsProp
 */
 
#ifndef OPTIMIZERKERNELS_H_INCLUDED
#define OPTIMIZERKERNELS_H_INCLUDED

__global__ void gradientDescentKernel(float* __restrict__ param, 
									  float* __restrict__ gradient, 
									  const float eta, const float lambda, const unsigned numel);;
__global__ void rmsPropKernel(float* __restrict__ d_param, 
							  float* __restrict__ d_grad, 
							  float* __restrict__ d_meansquare, 
							  const float eta, const float lambda, const unsigned numel);
__global__ void adamKernel(float* __restrict__ d_param, 
						   float* __restrict__ d_grad, 
						   float* __restrict__ d_meansquare, 
						   float* __restrict__ d_momentum, 
						   const float eta, const float lambda, const float beta1, const float beta2, const float epsilon, const unsigned iter, const unsigned numel);
__global__ void nadamKernel(float* __restrict__ d_param, 
							float* __restrict__ d_grad, 
							float* __restrict__ d_meansquare, 
							float* __restrict__ d_momentum, 
							const float eta, const float lambda, const float beta1, const float beta2, const float epsilon, const unsigned iter, const unsigned numel);
__global__ void delayLimitKernel(float* delay, const unsigned numel);

#endif // OPTIMIZERKERNELS_H_INCLUDED
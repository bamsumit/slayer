#ifndef SLAYERCONFIG_H_INCLUDED
#define SLAYERCONFIG_H_INCLUDED

// #include <slayerio.h>
#include <string>
#include <vector>
// #include <slayer.h>
// #include <slnetwork.h>
#include <slayerType.h>
#include <yaml-cpp/yaml.h>
#if defined SLAYERMEX
	#include <mex.h>
#endif


enum layerVar  {SPIKE, E, DELTA, RHO, U, A, ADOT};
/***** enumerator type definition for target pattern definition *****/
enum tgtPattern{SpikeTime, AppxSpikeTime, NumSpikes, AvgSpikes, ProbSpikes};
enum updateModeType{GradientDescent, Rmsprop, ADAM, NADAM};
enum gradAlgoType{SLAYER, SLAYER_ZERO, SLAYER_REFRACTORY};
/***** enumerator type definition for neuron type *****/
enum srkType{SRMALPHA, LIF, TNLIF};

class slayerType;


class slayerConfig
{
	YAML::Node defaultConfig;
	YAML::Node config;
	double *weight;
	double *delay;
	unsigned wSize;
	unsigned dSize;
	void setDefaultConfig();
	void setDefaultConfig(std::string path);
	void setUnknownValuesToDefault();
public:
	slayerConfig();
	slayerConfig(YAML::Node data);
	// slayerConfig(std::string defaultConfigPath);	// TODO
	#if defined mex_h
		slayerConfig(int nrhs, const mxArray *prhs[]);
	private:
		void loadYAMLNode(const mxArray* mxField);
	#endif
public:
	inline bool randomInitialization()
	{ 	return (weight==nullptr)||(delay==nullptr);		}
	tgtPattern     getPatternType();
	updateModeType getUpdateMode();
	gradAlgoType   getGradAlgo();
	srkType        getNeuronType();
	YAML::Node     get();
	std::vector<slayerType> getNetworkInfo();
	static std::vector<slayerType> getNetworkInfo(YAML::Node snnConfig);
	inline double* weightPtr()
	{	return weight;	}
	inline double* delayPtr()
	{	return delay;	}
	inline unsigned weightSize()
	{	return wSize;	}
	inline unsigned delaySize()
	{	return dSize;	}
	void           display();
	static void    display(YAML::Node snnConfig, bool displayArchDetails = false);
};

#endif // SLAYERCONFIG_H_INCLUDED
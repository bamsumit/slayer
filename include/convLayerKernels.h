/*
 * Author: Sumit Bam Shrestha
 * 08/03/2018 2:00 PM
 * Has routines for forward propagation, signal backpropagation and parameter backpropagation routines for convolution layer
 * forward propagation    : multWaConv(...)
 * signal backpropagation : multWdeltaConv(...)
 * weight backpropagation : multAdeltaConv(...)
 * delay backpropagation  : multAdotDeltaConv(...)
 */
 
#ifndef CONVLAYERKERNELS_H_INCLUDED
#define CONVLAYERKERNELS_H_INCLUDED

#include <cudaKernels.h>

// This routine expands weights of convolution layer into equivalent bigger weight matrix of dense layer
__global__ void expandConvWKernel(float* d_Wexp, const float* d_W, dim3 uDim, dim3 aDim, dim4 wDim, unsigned Nout, unsigned Nin);

/***** Signal Forward Propagation *****/
// __global__ void multWaConvKernel(float* d_u, const float* d_W, const float* d_a, dim3 uDim, dim3 aDim, dim3 wDim, unsigned numU, unsigned numW, unsigned Ns)
__global__ void multWaConvKernel(float* d_u, const float* d_W, const float* d_a, dim4 uDim, dim4 aDim, dim4 wDim, unsigned numU, unsigned numW, unsigned Ns);
void multWaConv(float* d_u, const float* d_W, const float* d_a, dim4 uDim, dim4 aDim, dim4 wDim);
void multWaConv(float* d_u, const float* d_W, const float* d_a, slayerType uInfo, slayerType aInfo, unsigned Ns);
// Following version uses expandConvWKernel(...) to expand the conv weight matrix into dense weight matrix and performs standard matrix multiplication
void multWaConv(cublasHandle_t &handle, float* d_u, const float* d_W, const float* d_a, slayerType uInfo, slayerType aInfo, unsigned Ns);

/***** Signal Backpropagation *****/
__global__ void multWdeltaConvKernel(float* d_e, const float* d_W, const float* d_delta, dim3 eDim, dim3 dDim, dim3 wDim, unsigned numE, unsigned numW, unsigned Ns);
// Following version uses expandConvWKernel(...) to expand the conv weight matrix into dense weight matrix and performs standard matrix multiplication
void multWdeltaConv(cublasHandle_t &handle, float* d_e, const float* d_W, const float* d_delta, slayerType eInfo, slayerType dInfo, unsigned Ns);

/***** Weight Backpropagation *****/
__global__ void multAdeltaConvKernel(float* d_gradW, const float* d_delta, const float* d_a, dim3 aDim, dim3 dDim, dim4 wDim, unsigned numW, unsigned Ns, float Ts, unsigned tStride);
void multAdeltaConv(cublasHandle_t &handle, float* d_gradW, const float* d_delta, const float* d_a, slayerType aInfo, slayerType dInfo, unsigned Ns, float Ts);

/***** Delay Backpropagation *****/
/* 
 * None
 * Because SLAYER has axonal delays only
 */


#endif // CONVLAYERKERNELS_H_INCLUDED
/*
 * Author: Sumit Bam Shrestha
 * 08/03/2018 2:00 PM
 * Has routines for forward propagation, signal backpropagation and parameter backpropagation routines for pooling (aggregation) layer
 * forward propagation    : multWaAggr(...)
 * signal backpropagation : multWdeltaAggr(...)
 * weight backpropagation : multAdeltaAggr(...)
 * delay backpropagation  : multAdotDeltaAggr(...)
 */
 
#ifndef POOLLAYERKERNELS_H_INCLUDED
#define POOLLAYERKERNELS_H_INCLUDED


#include <slayerType.h>

/***** Signal Forward Propagation *****/
__global__ void multWaAggrKernel(float* d_u, const float* d_W, const float* d_a, dim3 uDim, dim3 aDim, unsigned poolWidth, unsigned numU, unsigned Ns);
void multWaAggr(float* d_u, const float* d_W, const float* d_a, slayerType uInfo, slayerType aInfo, unsigned Ns);

/***** Signal Backpropagation *****/
__global__ void multWdeltaAggrKernel(float* d_e, const float* d_W, const float* d_delta, dim3 eDim, dim3 dDim, unsigned poolWidth, unsigned numE, unsigned Ns);
void multWdeltaAggr(float* d_e, const float* d_W, const float* d_delta, slayerType eInfo, slayerType dInfo, unsigned Ns);

/***** Parameter Backpropagation *****/
/* 
 * None
 * Because the weights and delays are constant in pooling layer
 */

#endif // POOLLAYERKERNELS_H_INCLUDED
/*
 * Author: Sumit Bam Shrestha
 * 3/10/2018 5:00 PM
 * Has routines to handle input output
 * Directs output routines to stdio::printf or mexPrintf based on context
 * Also handles input capture via stdio::scanf or [MATLAB: input()] based on context
 * slayerio::cin and slayerio::cout are also wrapped to get similar behaviour to standard one
 * NOTE!!! slayerio:scan() can only read what is entered befor \n key is pressed.
 * NOTE!!! slayerio::cin expects the inputs to be in separate line.
 */

#ifndef SLAYERIO_H_INCLUDED
#define SLAYERIO_H_INCLUDED

#include <string>
#include <iostream>
#include <sstream>
#include <stdarg.h>
#if defined SLAYERMEX
	#include <mexutils.h>
#endif
#include <cstdlib>

#define EXIT_CODE -18
#define MAX_STRING_BUFFER_SIZE 256

namespace slayerio
{
	int print(const char* format, ...);
	
	int scan(const char* format, ...);
	
	int error(const char* format, ...);
	
	class consoleOut
	{
	public:
		template<class T>
		consoleOut& operator << (const T& x)
		{
			std::stringstream sout;
			sout << x;
			#if defined mex_h
				mexPrintf("%s", sout.str().c_str());
			#else
				std::cout << sout.str();
			#endif
			return *this;
		}
		
		// this is needed for handling ostream manipulators line std::endl
		// for more readability, the function pointer can be defined to be manip using either of these
		// typedef std::ostream& (*manip) (std::ostream&);
		// using manip = std::ostream& (*) (std::ostream&);
		consoleOut& operator << ( std::ostream&(*manipulator)(std::ostream&) )
		{
			std::stringstream sout;
			sout << manipulator;
			#if defined mex_h
				mexPrintf("%s", sout.str().c_str());
			#else
				std::cout << sout.str();
			#endif
			return *this;
		}
	};
	extern consoleOut cout;
	
	class consoleIn
	{
	public:
		template <class T>
		consoleIn& operator >> (T& x)
		{
			char strbuf[MAX_STRING_BUFFER_SIZE];
			#if defined mex_h
				mex::getline(strbuf);
			#else
				std::cin.getline(strbuf, MAX_STRING_BUFFER_SIZE);
			#endif
			std::stringstream sin(strbuf);
			sin >> x;
			return *this;
		}
	};
	
	extern consoleIn cin;
	
	inline void clear(unsigned &thingsToClear)
	{
		#if defined mex_h
			for(unsigned n=0; n<thingsToClear; ++n)	mexPrintf("\b");
		#elif defined _WIN32 || defined _WIN64
			auto dump = system("cls");
		#elif defined __linux__
			// auto dump = system("clear");
			// for(unsigned n=0; n<thingsToClear; ++n)	printf("\033[1A\033[K");	// "\033[<N>A" move cursor N lines up; "\033[K" clear line
			static char strbuf[50];
			sprintf(strbuf, "\033[%dA", thingsToClear);
			printf("%s", strbuf);	// "\033[<N>A" move cursor N lines up; "\033[K" clear line
		#endif			
		thingsToClear = 0;
	}
}

#define slayerErr std::endl << "Error at file : " <<__FILE__<< ", line : " <<__LINE__ << std::endl << slayerio::error("")


#endif	// SLAYERIO_H_INCLUDED
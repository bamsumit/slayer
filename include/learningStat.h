/*
 * Author: Sumit Bam Shrestha
 * 18/10/2018 10:30 AM
 * Has routines for capturing learning statistics
 */
 
#ifndef LEARNINGSTAT_H_INCLUDED
#define LEARNINGSTAT_H_INCLUDED

#include <iostream>

#define MAXCOST 1e15

class learningStat
{
	double minCost;
	double maxAcc;
public:
	double costSum;
	size_t correctSamples;
	size_t numSamples;
public:
	learningStat() : costSum(0), minCost(MAXCOST), maxAcc(0), correctSamples(0), numSamples(0) {}
	inline void reset()
	{	costSum = correctSamples = numSamples = 0;	}
	inline double cost() const
	{	return (numSamples > 0) ? costSum/numSamples : -1;	}
	inline double accuracy() const
	{	return (numSamples > 0) ? 1.0 * correctSamples/numSamples : 0;	}
	inline double minimumCost() const
	{	return (minCost > 0.99 * MAXCOST) ? -1 : minCost; 	}
	inline double maximumAccuracy() const
	{	return maxAcc;	}
	inline void updateMinCost()
	{	minCost = (minCost > cost()) ? cost() : minCost;	}
	inline void updateMaxAcc()
	{	maxAcc  = (maxAcc < accuracy()) ? accuracy() : maxAcc;	}
};

unsigned displayStats(const learningStat& trainingStat, const learningStat& testingStat, unsigned iter, float runtime);
 
 #endif // LEARNINGSTAT_H_INCLUDED
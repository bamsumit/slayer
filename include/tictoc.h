#ifndef TICTOC_H_INCLUDED
#define TICTOC_H_INCLUDED


#if defined _WIN32 || defined _WIN64
    #include <windows.h>
	double GetTickCount();
#elif defined __linux__
    double GetTickCount();
#else
    #error "unknown platform"
#endif

int tic();
double toc();

// end tic() toc() definiton

class timeit
{
	double tstart;
public:
	timeit() : tstart(GetTickCount()) {	}
	inline double record()
	{
		double elapsedtime = GetTickCount() - tstart;
		tstart = GetTickCount();
		return elapsedtime;
	}
};

#endif //TICTOC_H_INCLUDED

/*
 * Author: Sumit Bam Shrestha
 * 12/02/2018 6:00 PM
 * Has routines GPU initialization and de-initialization for slayer code
 */

#ifndef INITGPU_H_INCLUDED
#define INITGPU_H_INCLUDED

#include <vector>
#if defined SLAYERMEX
	#include <mex.h>
#endif

// std::vector<unsigned> getPreferredGPUs(const mxArray* mxField);
std::vector<unsigned> getAvailableGPUs();
std::vector<unsigned> getAvailableGPUs(std::vector<unsigned> preferredGPUs);
void establishGPUPeerAccess(std::vector<unsigned> deviceID);
void removeGPUPeerAccess(std::vector<unsigned> deviceID);
std::vector<unsigned> initializeMultiGPU(std::vector<unsigned> preferredGPUs);
void deInitializeMultiGPU(std::vector<unsigned> deviceID);

#endif // INITGPU_H_INCLUDED
/*
 * Author: Sumit Bam Shrestha
 * 28/06/2018 3:30 PM
 * MATLAB equivalent plot routines to call from mex
 */
 
#ifndef MEXUTILS_H_INCLUDED
#define MEXUTILS_H_INCLUDED

#include <mex.h>
#include <string>
#include <vector>
#include <sstream>

#ifndef MAXBUF
	#define MAXBUF 256
#endif

namespace mex
{
	std::string getline();;
	char* getline(char* strbuf);
	double getScalar(const mxArray* mxField, const char* name);
	double getScalar(const mxArray* mxField, const char* name, double defaultVal);
	std::vector<float> getFVector(const mxArray* mxField, const char* name);
	std::vector<float> getFVector(const mxArray* mxField, const char* name, std::vector<float> defaultVal);
	std::vector<unsigned> getUVector(const mxArray* mxField, const char* name);
	std::string getString(const mxArray* mxField, const char* name);
	std::string getString(const mxArray* mxField, const char* name, const char* defaultString);
}

void silentFigure(unsigned figureID);

template <typename T1, typename T2>
void plot(T1 x, T2 y, std::string plotSpecs = "default")
{
	// write x into MATLAB variable
	mxArray *mxX = mxCreateDoubleMatrix(1, 1, mxREAL);
	double  *ptX = mxGetPr(mxX);
	*ptX = x;
	// write y into MATLAB variable
	mxArray *mxY = mxCreateDoubleMatrix(1, 1, mxREAL);
	double  *ptY = mxGetPr(mxY);
	*ptY = y;
	// wrtie plotSpecs into MATLAB variable
	mxArray *mxPlotSpecs;
	if(plotSpecs.compare("default") == 0)	mxPlotSpecs = mxCreateString(".");
	else 									mxPlotSpecs = mxCreateString(plotSpecs.c_str());
	// create plot arguments
	// MATLAB: semilogy(mxX, mxY, plotSpecs)
	mxArray *lhs[] = {mxX, mxY, mxPlotSpecs};
	mexCallMATLAB(0, NULL, 3, lhs, "plot");
	mxDestroyArray(mxX);
	mxDestroyArray(mxY);
	mxDestroyArray(mxPlotSpecs);
}

template <typename T1, typename T2>
void semilogy(T1 x, T2 y, std::string plotSpecs = "default")
{
	// write x into MATLAB variable
	mxArray *mxX = mxCreateDoubleMatrix(1, 1, mxREAL);
	double  *ptX = mxGetPr(mxX);
	*ptX = x;
	// write y into MATLAB variable
	mxArray *mxY = mxCreateDoubleMatrix(1, 1, mxREAL);
	double  *ptY = mxGetPr(mxY);
	*ptY = y;
	// wrtie plotSpecs into MATLAB variable
	mxArray *mxPlotSpecs;
	if(plotSpecs.compare("default") == 0)	mxPlotSpecs = mxCreateString(".");
	else 									mxPlotSpecs = mxCreateString(plotSpecs.c_str());
	// create plot arguments
	// MATLAB: semilogy(mxX, mxY, plotSpecs)
	mxArray *lhs[] = {mxX, mxY, mxPlotSpecs};
	mexCallMATLAB(0, NULL, 3, lhs, "semilogy");
	mxDestroyArray(mxX);
	mxDestroyArray(mxY);
	mxDestroyArray(mxPlotSpecs);
}

template <typename T>
void bar3(std::vector<std::vector<T>> mat, std::string plotSpecs = "default")
{
	// wrtie mat to MATLAB variable
	unsigned M = mat.size();
	unsigned N = mat[0].size();
	for(unsigned m=1; m<M; ++m)
		N = (mat[m].size() > N) ? mat[m].size() : N;
	mxArray *mxMat = mxCreateDoubleMatrix(M, N, mxREAL);
	double  *ptMat = mxGetPr(mxMat);
	for(unsigned m=0; m<mat.size(); ++m)
		for(unsigned n=0; n<mat[m].size(); ++n)
			ptMat[m + n * M] = mat[m][n];
	
	// write plotSpecs into MATLAB variable
	mxArray *mxPlotSpecs;
	if(plotSpecs.compare("default") == 0)	mxPlotSpecs = mxCreateString(" ");
	else 									mxPlotSpecs = mxCreateString(plotSpecs.c_str());
	// create plot arguments
	// MATLAB: bar3(mxMat, plotSpecs)
	mxArray *lhs[] = {mxMat, mxPlotSpecs};
	if(plotSpecs.compare("default") == 0)	mexCallMATLAB(0, NULL, 1, lhs, "bar3");
	else 									mexCallMATLAB(0, NULL, 2, lhs, "bar3");
	mxDestroyArray(mxMat);
	mxDestroyArray(mxPlotSpecs);	
}

// void rasterPlot(float* s, unsigned Ns, unsigned numel);
void rasterPlot(float* s, unsigned Ts, unsigned figID, unsigned Ns, unsigned numel);

void mxCopy(mxArray* mxPtr, std::vector<float> data);

void mxCopy(mxArray* mxPtr, std::vector<std::vector<float>> data);

void mxCopy2(mxArray* mxPtr, std::vector<float> data1, std::vector<float> data2);

void plotCost(double J, unsigned epoch, unsigned figureID, std::string plotTitle, std::string plotSpecs);

void plotAcc(double Acc, unsigned epoch, unsigned figureID, std::string plotTitle, std::string plotSpecs);


#endif // MEXUTILS_H_INCLUDED
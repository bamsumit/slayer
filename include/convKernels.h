/*
 * Author: Sumit Bam Shrestha
 * 10/03/2018 2:00 PM
 * This header contains routines to perform time based convolution and correlation of signal
 * These operations are key in forward propagation and backpropagation routines in SLAYER
 */
 
#ifndef CONVKERNELS_H_INCLUDED
#define CONVKERNELS_H_INCLUDED



__global__ void convKernel(	float* output, 
							const float* input, const float* filter, 
							unsigned signalSize, unsigned filterSize, 
							unsigned nNeurons, unsigned nDelayedSyn, 
							float Ts);
void conv(	float* output, 
			const float* input, const float* filter, 
			unsigned signalSize, unsigned filterSize, 
			unsigned nNeurons, unsigned nDelayedSyn, 
			float Ts);

__global__ void delayedConvKernel(	float* output, 
									const float* input, const float* filter, const float* delay, 
									unsigned signalSize, unsigned filterSize, 
									unsigned nNeurons, unsigned nDelayedSyn, 
									float Ts);
void delayedConv(	float* output, 
					const float* input, const float* filter, const float* delay, 
					unsigned signalSize, unsigned filterSize, 
					unsigned nNeurons, unsigned nDelayedSyn, 
					float Ts);

__global__ void corrKernel(	float* output, 
							const float* input, const float* filter, 
							unsigned signalSize, unsigned filterSize, 
							unsigned nNeurons, unsigned nDelayedSyn, 
							float Ts);
void corr(	float* output, 
			const float* input, const float* filter, 
			unsigned signalSize, unsigned filterSize, 
			unsigned nNeurons, unsigned nDelayedSyn, 
			float Ts);

__global__ void delayedCorrKernel(	float* output, 
									const float* input, const float* filter, const float* delay, 
									unsigned signalSize, unsigned filterSize, 
									unsigned nNeurons, unsigned nDelayedSyn, 
									float Ts);
void delayedCorr(	float* output, 
					const float* input, const float* filter, const float* delay, 
					unsigned signalSize, unsigned filterSize, 
					unsigned nNeurons, unsigned nDelayedSyn, 
					float Ts);

#endif // CONVKERNELS_H_INCLUDED
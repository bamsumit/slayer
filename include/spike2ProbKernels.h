/*
 * Author: Sumit Bam Shrestha
 * 02/03/2018 4:00 PM
 * Has routines to interpret probability values from spike train and corresponding cross entropy loss function
 */
 
#ifndef SPIKE2PROBKERNELS_H_INCLUDED
#define SPIKE2PROBKERNELS_H_INCLUDED

#include <cudaKernels.h>

__global__ void countSpikesKernel(unsigned* d_nSpikes, const float* d_s, unsigned windowLength, unsigned nNeurons, unsigned Ns);
__global__ void sumSpikesKernel(unsigned* totalSpikes, const unsigned* nSpikes, unsigned nNeurons, unsigned Ns);
__global__ void spikeProbabilityKernel(float* probability, const unsigned* nSpikes, const unsigned* totalSpikes, unsigned nNeurons, unsigned Ns, float offset = 0.01);
__global__ void spikeProbErrorKernel(float* e, const float* probability, const unsigned* classID, const unsigned *totalSpikes, unsigned nNeurons, unsigned Ns, float offset = 0.01);
__global__ void crossEntropyLossKernel(float* L, const unsigned* classID, const float* probability, unsigned Ns);
float crossEntropyLoss(const unsigned* classID, const float* probability, unsigned Ns, float Ts);

#endif // SPIKE2PROBKERNELS_H_INCLUDED
# README #

This is a backpropagation based learning system for spiking neural networks (SNNs).
The learning method is named **S**pike **LAY**er **E**rror **R**eassignment (**SLAYER**)

A brief introduction of the method is in the following video.

[![](http://img.youtube.com/vi/JGdatqqci5o/0.jpg)](http://www.youtube.com/watch?v=JGdatqqci5o "")

The base description of the framework has been published in [NeurIPS 2018](https://nips.cc/Conferences/2018/Schedule?showEvent=11157).
The final paper is available [here](http://papers.nips.cc/paper/7415-slayer-spike-layer-error-reassignment-in-time.pdf).
The arXiv preprint is available [here](https://arxiv.org/abs/1810.08646).

## Citation ##
Sumit Bam Shrestha and Garrick Orchard. "SLAYER: Spike Layer Error Reassignment in Time." 
In _Advances in Neural Information Processing Systems_, pp. 1417-1426. 2018.

```bibtex
@InCollection{Shrestha2018,
  author    = {Shrestha, Sumit Bam and Orchard, Garrick},
  title     = {{SLAYER}: Spike Layer Error Reassignment in Time},
  booktitle = {Advances in Neural Information Processing Systems 31},
  publisher = {Curran Associates, Inc.},
  year      = {2018},
  editor    = {S. Bengio and H. Wallach and H. Larochelle and K. Grauman and N. Cesa-Bianchi and R. Garnett},
  pages     = {1419--1428},
  url       = {http://papers.nips.cc/paper/7415-slayer-spike-layer-error-reassignment-in-time.pdf},
}
```

# PyTorch version of this repository is publically available at [https://github.com/bamsumit/slayerPytorch](https://github.com/bamsumit/slayerPytorch) #

## What is this repository for? ##

* For learning weight and delay parameters of a multilayer spiking neural network.
* Natively handles multiple spikes in each layer and error backpropagation through the layers. 
* Version 1.1

## How do I get set up? ##

* Requires:
	* GPU hardware with cc 6.0 or higher.	
	* `CUDA` library installed (Tested on `CUDA 9.2`)	
	* `CUDNN` library installed (Tested on `CUDNN 7.2`)
	* `yaml-cpp` library installed (available [here](https://github.com/jbeder/yaml-cpp))
		* MAKE SURE `yaml-cpp` is installed with dynamically linked shared library (`-DBUILD_SAHRED_LIBS=ON` during `cmake` project setup)
* __CPP application:__
	* Go to `slayerCPP/`
	* Edit `makefile`
		* change the path to `CUDA_PATH` and `YAML_PATH` if different.
	* Run `makefile` to compile the project: __spikelearn__ executable.
* __MEX application:__
	* Go to `slayerMEX/`
	* Additionally requires `MEX C++` compilation with `C++11` support.	
	* Setup:
		* Install `mex` compiler in `MATLAB`.
			* Requires **Visual C++** compiler on *Windows*, **MinGW GCC C++** compiler on *Linux*. 
			Install the appropriate one.
			* run `mex -setup` in `MATLAB` command line and select the detected compiler.
		* Install `libut.lib` for `Ctrl+C` detection. 
		Follow the instructions [here](http://www.caam.rice.edu/~wy1/links/mex_ctrl_c_trick/).
		* Edit `nvmex.m`. 
			* Change the path to `libut.lib` in it. 
			Note, `libut.lib` _is requried for ___windows___ only_.
			* Change the path to `CUDA` library location and `YAML-CPP` library location if different.
	* Run `nvmex spikelearn.cu` OR `nvmex('spikeLearn.cu')` in `MATLAB` commandline.

## [FAQ](https://bitbucket.org/bamsumit/slayer/wiki/faq)

## CPP application ##
Make sure you have compiled according to the installation instructions above.

### Example Run ###
* Go to `example/` folder.
* Unzip `NMNISTsmall.zip`
* Inside `NMNISTsmall`:
	* **`1.bs2` to `1000.bs2` :** Training input spikes.
	* **`60001.bs2` to `60100.bs2` :** Testing input spikes.
	* **`Desired_10_60` :** Desired output spike files.
		* the output neuron is desired to spike 10 times for false class and 60 times for true class.
	* **`train1K.txt` :** list of training samples and desried spikes.
	* **`test100.txt` :** list of testing samles and desired spikes.
* Start training by passing `example.yaml` file as an argument to `spikelearn` executable.
	* `$ ./../slayerCPP/spikelearn example.yaml`
* Change the network parameters and leraning parameters by changin `example.yaml`

### Network Description ###

Network structure is defined by a `yaml` descriptor file.
An example description is in `example.yaml`.  
```python
simulation:
	Ts: 1.0
	tSample: 350
	nSample: 10
neuron:
	type:     SRMALPHA
	theta:    10
	tauSr:    1.0
	tauRef:   1.0
	scaleRef: 2     # relative to theta
	tauRho:   1     # relative to theta 
	scaleRho: 1
layer:
	# wScale = 1 and sigma = 0 by default
	- {dim: 34x34x2, wScale: 0.5}
	- {dim: 512}
	- {dim: 512}
	- {dim: 10}
training:
	learning:
		etaW: 0.01
		etaD: 0.01
		# lambda: 0.0001
	testing: true
	error:
		type: NumSpikes #ProbSpikes #NumSpikes
	stopif:
		maxIter: 200000
		minCost: 0.001
	path:
		out:     OutFiles/
		in:      NMNISTsmall/
		desired: NMNISTsmall/Desired_10_60/
		train:   NMNISTsmall/train1K.txt
		test:    NMNISTsmall/test100.txt
	optimizer: 
		name:  NADAM  #GradientDescent
	gradAlgo:  SLAYER
preferredGPU: [0]
```

### Usage ###
__`spikelearn`__: The output of the training are saved in `OutFiles/` (or whatever is passed in `["training"]["path"]["out"]` field).
NOTE: The folder is not created automatically.

* `$ ./../slayerCPP/spikelearn example.yaml`

## MEX applications ##
Make sure you have compiled according to the installation instructions above.

### Example Run ###
Once compiled, try running the example codes in `example/` folder
* Go to `example/` folder.
* Run `exampleTrain.m` to train on a small subset of NMNIST digit classification dataset.
* Run `exampleRun.m` to inference using learned weights and delays on a small subset of NMNIST digit classification dataset.
* Inside `NMNISTsmall`:
	* **`1.bs2` to `1000.bs2` :** Training input spikes.
	* **`60001.bs2` to `60100.bs2` :** Testing input spikes.
	* **`Desired_10_60` :** Desired output spike files.
		* the output neuron is desired to spike 10 times for false class and 60 times for true class.
	* **`train1K.txt` :** list of training samples and desried spikes.
	* **`test100.txt` :** list of testing samles and desired spikes.

### Network Description ###

Network structure is defined by a `MATLAB` structure that is passed as an argument to the mex file.
An example description is in `exampleTrain.m`.  
```matlab
snn.Ts      = 0.1;	% simulation precision
snn.tSample = 20;	% time in ms for each sample
snn.nSample = 4;	% number of samples to process at once. 1=>SGD, NumSamples=>Batch, otherwise mini batch

snn.nnArch  = '2-10-1';	% '28x28-12c5-2a-64c5-2a-10o';
snn.theta   = 10*ones(1, 3);
snn.sigma   = [0 0 0];
snn.wScale  = [2 10 0];
snn.nSynDel = 1;	% 1 by default if not defined
weight      = rand(30, 1)*snn.wScale(1);
delay       = rand(21, 1)*0;

snn.eta         = 1;
snn.tauSr       = 1;
snn.tauRef      = 1;
snn.neuronType  = 'SRMALPHA';
snn.maxIter     = 1000;
snn.minCost     = 0.1; 

snn.inPath      = '../testFiles/';
snn.outPath     = 'OutFiles/';
snn.desiredPath = '../testFiles/Desired/';
snn.patternType = 'NumSpikes';%'AppxSpikeTime';% 'SpikeTime';
snn.trainList   = '../testFiles/trainList.txt';
snn.testList    = '../testFiles/testList.txt';

snn.testing     = true; % false by default 
snn.updateMode  = 'Rmsprop'; % 'GradientDescent';
```
_COMING SOON_: support for yaml like network descriptor.

### Usage ###
__`spikelearn`__ returns the __learning cost__ and __accuracy__ depending upon the number of output arguments. First column is the training result and second column is the testing result.

* With pre-assigned weight and delay  
`J = spikelearn(snn, weight, delay);`
* With pre-assigned random seed to generate weight and delay  
`J = spikelearn(snn, 7);`
* With time based random seed to initialize weight and delay  
`J = spikelearn(snn);`
* Capture cost and accuracy  
`[J Acc] = spikelearn(snn);`
* Capture cost, learned weight and learned delay  
`[J weight delay] = spikelearn(snn);`
* Capture cost, accuracy, learned weight and learned delay  
`[J Acc weight delay] = spikelearn(snn);`

__`spikerun`__ returns the spike output for a particular set of weight and delay parameters.	

* `spike = spikerun(snn, weight, delay);`



## Database configuration ##

* Collect all the samples into one folder with the names as sample ID. e.g. `0.bs1`.
	* The extension of the file determines the type of spike file.  
	
	  |File     |Description                          |  
	  |:-------:|:------------------------------------|  
	  |`*.bs1`  | 1 dimensional binary spike file     |    
      |`*.bs2`	| 2 dimensional binary spike file     |    
      |`*.bsn`	| number of spikes file     	      |   
	  
* Collect the desired spike definition of each class in a folder with class names as class ID. e.g. `0.bsn`
* Create `*.txt` files for listing the training or testing samples and their corresponding class ID.
	* The first number represents the _sample ID_ and the second number represents the _class ID_.
	* Below is a sample training list for XOR training.  
~~~
           # trainlist.txt
           # Sample  Class
             0       0  
             1       1  
             2       1  
             3       0 
~~~

### Description of the code organization ###
For additional details and code organization, please refer to the wiki https://bitbucket.org/bamsumit/slayer/wiki/Home

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* If any bugfixes and suggestions, email [Sumit](mailto:bam_sumit@hotmail.com).

### License & Copyright ###
Copyright 2018 Sumit Bam Shrestha
SLAYER is free software: you can redistribute it and/or modoify it under the terms of 
GNU General Public License as published by the Free Software Foundation, 
either version 3 of the License, or (at your option) any later version.

SLAYER is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
FITNESS FOR A PARTICULAR PURPOSE. 
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License SLAYER.
If not, see http://www.gnu.org/licenses/.
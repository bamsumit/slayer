#include <slayer.h>
#include <slayerio.h>
#include <spikeio.h>
#include <spikeKernels.h>
#include <convKernels.h>
#include <spike2ProbKernels.h>
#include <denseLayerKernels.h>
#include <convLayerKernels.h>
#include <poolLayerKernels.h>
#include <tictoc.h>

// void slayer::cudaDeviceSynchronizeAll()
// {
	// for(unsigned i=0; i<deviceID.size(); ++i)
	// {
		// cudaSetDevice(deviceID[i]);
		// cudaDeviceSynchronize();
	// }
// }

void slayer::evalRho()
{
	// float tau = static_cast<float>(theta/log(10));
	// float tau = theta;
	
	// mexPrintf("tau = %g\n", tau);
	// for(unsigned id=0; id<info.nNeurons; ++id)	for(unsigned i=0; i<Ns; ++i)	rho[id][i] = 1/tau*exp(-fabs(theta - u[id][i])/tau);
	
	dim3 thread, block;
	thread.x = 128;
	thread.y = 8;
	block.x = ceil(1.0f * Ns           /thread.x);
	block.y = ceil(1.0f * info.nNeurons/thread.y);
	if(block.y >= 65535)	slayerio::error("Error: maximum blockDim.y exceeded at slayer.cpp/evalRho.");
	if(block.z >= 65535)	slayerio::error("Error: maximum blockDim.z exceeded at slayer.cpp/evalRho.");
	
	// slayerio::cout << "scaleRho = " << scaleRho << ", tauRho = " << tauRho << std::endl;
	
	// evalRhoKernel<<< block, thread >>>(rho, u, theta, tau, info.nNeurons, Ns);
	// evalRhoKernel<<< block, thread >>>(rho, u, theta, tau, info.nNeurons, Ns, 1.0/10);
	evalRhoKernel<<< block, thread >>>(rho, u, theta, tauRho * theta, info.nNeurons, Ns, scaleRho);
}

void  slayer::numSpikesEventToTensor(const unsigned batchIndex, const slayer &spikeLayer, 
									 const std::vector<unsigned> neuronID, 
									 const std::vector<float> tSt, 
									 const std::vector<float> tEn, 
									 const std::vector<unsigned> numSpikes, const unsigned tSampleLen)
{
	auto i=batchIndex;
	unsigned nSpikes = tSt.size();
	for(unsigned rID=0; rID < nSpikes; ++rID)
	{
		unsigned nID = neuronID[rID];
		unsigned startID = unsigned(tSt[rID]/Ts);
		unsigned endID   = unsigned(tEn[rID]/Ts);
		if(endID >= tSampleLen)	endID = tSampleLen - 1;	// boundary limiting
		// count the number of spikes acutally present in the range
		float actualSpikes = 0;
		for(unsigned tID=startID; tID < endID; ++tID)	
		{
			// actualSpikes += spikeLayer.s[nID][tID + i*tSampleLen] * Ts;
			// s[nID][tID + i*tSampleLen] = spikeLayer.s[nID][tID + i*tSampleLen];
			actualSpikes += spikeLayer.s[nID*Ns + tID + i*tSampleLen] * Ts;
			s[nID*Ns + tID + i*tSampleLen] = spikeLayer.s[nID*Ns + tID + i*tSampleLen];
		}
		
		// float avgValue = (int(numSpikes[rID]) - actualSpikes)/(tEn[rID] - tSt[rID]);
		float avgValue = (int(numSpikes[rID]) - actualSpikes)/(endID - startID);
		// mexPrintf("Actual spikes = %g, Desired spikes = %d\n", actualSpikes, numSpikes[rID]);
		// mexPrintf("AvgValue = %g\n", avgValue);
		for(unsigned tID=startID; tID < endID; ++tID)
			e[nID*Ns + tID + i*tSampleLen] += avgValue/Ts;		// e[nID][tID + i*tSampleLen] += avgValue/Ts;
	}
}

float slayer::errorSpikeTime (slayer &desiredLayer)
{
	erruntime.clear();
	timeit profiler;
	
	for(unsigned i=0; i<sSize; ++i)		desiredLayer.s[i] = this->s[i] - desiredLayer.s[i];
	
	if(profile)
	{
		cudaDeviceSynchronize();
		erruntime.push_back(profiler.record());
	}
	
	cudaMemAdvise(s, sizeof(float) * sSize, cudaMemAdviseSetPreferredLocation, deviceID);
	cudaMemAdvise(e, sizeof(float) * eSize, cudaMemAdviseSetPreferredLocation, deviceID);
	
	cudaSetDevice(deviceID);
	conv(this->e, desiredLayer.s, eps, Ns, epsSize, info.nNeurons, info.nDelayedSyn, Ts);
	cudaDeviceSynchronize();
	
	if(profile)	erruntime.push_back(profiler.record());
	
	float J;
	cudaSetDevice(deviceID);
	cublasStatus_t status = cublasSnrm2(handle, this->eSize, this->e, 1, &J);
	checkCublasErrors(status);
	// handleCublasStatus(status, "slayer.h/errorSpikeTime");
	J *= J;
	J /= this->info.nNeurons;
	
	if(profile)
	{
		cudaDeviceSynchronize();
		erruntime.push_back(profiler.record());
	}
	
	return J;
}

float slayer::errorNumSpikes (slayer &desiredLayer)
{
	erruntime.clear();
	timeit profiler;
	
	for(unsigned i=0; i<sSize; ++i)		desiredLayer.s[i] = this->s[i] - desiredLayer.s[i];
	
	if(profile)
	{
		cudaDeviceSynchronize();
		erruntime.push_back(profiler.record());
	}
	
	cudaMemAdvise(s, sizeof(float) * sSize, cudaMemAdviseSetPreferredLocation, deviceID);
	cudaMemAdvise(e, sizeof(float) * eSize, cudaMemAdviseSetPreferredLocation, deviceID);
	
	cudaSetDevice(deviceID);
	conv(this->e, desiredLayer.s, eps, Ns, epsSize, info.nNeurons, info.nDelayedSyn, Ts);
	cudaDeviceSynchronize();
	
	dim3 thread, block;
	thread.x = 128;
	thread.y = 8;
	block.x = ceil(1.0f * Ns           /thread.x);
	block.y = ceil(1.0f * info.nNeurons/thread.y);
	if(block.y >= 65535)	slayerio::error("Error: maximum blockDim.y exceeded at slayer.cpp/errorNumSpikes.");
	if(block.z >= 65535)	slayerio::error("Error: maximum blockDim.z exceeded at slayer.cpp/errorNumSpikes.");
	
	cudaSetDevice(deviceID);
	diffKernel<<< block, thread >>>(this->e, desiredLayer.e, Ns, info.nNeurons);
	cudaDeviceSynchronize();
	
	if(profile)	erruntime.push_back(profiler.record());
	
	float J;
	cudaSetDevice(deviceID);
	cublasStatus_t status = cublasSnrm2(handle, this->eSize, this->e, 1, &J);
	// handleCublasStatus(status, "slayer.h/errorSpikeTime");
	checkCublasErrors(status);
	J *= J;
	J /= this->info.nNeurons;
	
	if(profile)
	{
		cudaDeviceSynchronize();
		erruntime.push_back(profiler.record());
	}
	
	return J;
}

float slayer::errorProbSpikes(slayer &desiredLayer)
{
	erruntime.clear();
	dim3 block, thread;
	thread.x = 128;
	thread.y = 4;
	block.x = ceil(1.0f * Ns / thread.x);
	block.y = ceil(1.0f * info.nNeurons / thread.y);
	
	timeit profiler;
	
	spikeProbErrorKernel<<< block, thread >>>(this->e, this->probability, desiredLayer.classID, this->totalSpikes, info.nNeurons, Ns);
	
	if(profile)
	{
		cudaDeviceSynchronize();
		erruntime.push_back(profiler.record());
	}
	
	float J = crossEntropyLoss(desiredLayer.classID, probability, Ns, Ts);
	
	if(profile)	erruntime.push_back(profiler.record());
	
	return J;
}

slayer::slayer()
: theta(10), sigma(0)	
{
	info.nNeurons = 0;
	wInd  		= NULL;
	dInd  		= NULL;
	W           = NULL;
	d           = NULL;
	gradW       = NULL;
	gradD       = NULL;
	meanSquareW = NULL;
	meanSquareD = NULL;
	momentumW   = NULL;
	momentumD   = NULL;
	eps         = NULL;
	epsDot      = NULL;
	nu          = NULL;
	t           = NULL;
	s           = NULL;
	a           = NULL;
	aDot        = NULL;
	u           = NULL;
	e           = NULL;
	rho         = NULL;
	delta       = NULL;
	nSpikes		= NULL;
	totalSpikes = NULL;
	classID		= NULL;
	probability = NULL;
	profile     = false;
	gradAlgo    = SLAYER;
	
	for(unsigned i=0; i<3; ++i)		nanCount.push_back(0);
	
	cublasHandleCreated = false;
	// cublasCreate(&handle);
	
	WSize = dSize = epsSize = epsDotSize = nuSize = sSize = aSize = aDotSize = uSize = eSize = rhoSize = deltaSize = 0;
	pre  = nullptr;
	post = nullptr;
}

slayer::slayer(slayerType layerInfo, float Theta, float Sigma, float TauRho, float ScaleRho, gradAlgoType GradAlgo,
               std::vector<float> time, std::vector<float> srmKernel, std::vector<float> srmKernelDot, std::vector<float> refKernel, unsigned nSamples)
: info(layerInfo), 
  theta(Theta), 
  sigma(Sigma),
  tauRho(TauRho),
  scaleRho(ScaleRho),
  gradAlgo(GradAlgo),
  Ns(static_cast<unsigned>(time.size())), 
  Ts(time[1]-time[0]), 
  epsSize   (static_cast<unsigned>(srmKernel.size())), 
  nuSize    (static_cast<unsigned>(refKernel.size())), 
  epsDotSize(static_cast<unsigned>(srmKernelDot.size())), 
  batchSize(nSamples),
  profile(false)
{
	batchStride  = (Ns - 1)/batchSize;	
	
	if(info.nWeights * info.nDelayedSyn > 0)	wInd  = new unsigned[info.nWeights * info.nDelayedSyn];		else wInd = NULL;
	if(info.nDelays  * info.nDelayedSyn > 0)	dInd  = new unsigned[info.nDelays  * info.nDelayedSyn];		else dInd = NULL;
	
	W           = NULL;
	d           = NULL;
	gradW       = NULL;
	gradD       = NULL;
	meanSquareW = NULL;
	meanSquareD = NULL;
	momentumW   = NULL;
	momentumD   = NULL;
	
	WSize = info.nWeights * info.nDelayedSyn;
	dSize = info.nDelays  * info.nDelayedSyn;
	sSize = uSize = rhoSize = deltaSize = 	Ns * info.nNeurons;
	aSize = aDotSize = eSize = 				Ns * info.nNeurons * info.nDelayedSyn;
	
	checkCudaErrors( cudaMallocManaged(&eps   , sizeof(float) *    epsSize) );
	checkCudaErrors( cudaMallocManaged(&epsDot, sizeof(float) * epsDotSize) );
	checkCudaErrors( cudaMallocManaged(&nu    , sizeof(float) *     nuSize) );
	checkCudaErrors( cudaMallocManaged(&t     , sizeof(float) *         Ns) );		
	checkCudaErrors( cudaMallocManaged(&s     , sizeof(float) *      sSize) );
	checkCudaErrors( cudaMallocManaged(&a     , sizeof(float) *      aSize) );
	checkCudaErrors( cudaMallocManaged(&aDot  , sizeof(float) *   aDotSize) );
	checkCudaErrors( cudaMallocManaged(&u     , sizeof(float) *      uSize) );
	checkCudaErrors( cudaMallocManaged(&e     , sizeof(float) *      eSize) );
	checkCudaErrors( cudaMallocManaged(&rho   , sizeof(float) *    rhoSize) );
	checkCudaErrors( cudaMallocManaged(&delta , sizeof(float) *  deltaSize) );
	
	if(info.type == Output)
	{
		nSpikesSize	    = Ns * info.nNeurons;
		totalSpikesSize = Ns;
		classIDSize		= Ns * info.nNeurons;
		probabilitySize = Ns * info.nNeurons;
		
		cudaMallocManaged(&nSpikes	  , sizeof(unsigned) * 	   nSpikesSize );
		cudaMallocManaged(&totalSpikes, sizeof(unsigned) * totalSpikesSize );
		cudaMallocManaged(&classID	  , sizeof(unsigned) * 	   classIDSize );
		cudaMallocManaged(&probability, sizeof(float)    * probabilitySize );
	}
	else
	{
		nSpikesSize	    = 0;
		totalSpikesSize = 0;
		classIDSize		= 0;
		probabilitySize = 0;
		
		nSpikes	  		= NULL;
		totalSpikes		= NULL;
		classID	  		= NULL;
		probability		= NULL;
	}	
	
	// if(info.type == Output)
	// {
		
	// }
	for(unsigned i=0; i<epsSize   ; ++i)	eps[i]    = srmKernel[i];
	for(unsigned i=0; i<epsDotSize; ++i)	epsDot[i] = srmKernelDot[i];
	for(unsigned i=0; i<nuSize    ; ++i)	nu[i]     = refKernel[i];
	for(unsigned i=0; i<Ns        ; ++i)	t[i]      = time[i];
	
	for(unsigned i=0; i<3; ++i)		nanCount.push_back(0);
	
	// cublasCreate(&handle);
	cublasHandleCreated = false;
	pre  = nullptr;
	post = nullptr;
	// slayerio::cout << "Constructor\n";
	handleCudaErrorCheck("slayer.h/slayer");
}

slayer::~slayer()
{
	// mexPrintf("\nslayer destructor called, nNeurons = %d\n", info.nNeurons);	mexEvalString("drawnow;");
	delete [] wInd;		// mexPrintf("wInd\t");	mexEvalString("drawnow;");
	delete [] dInd;		// mexPrintf("dInd\t");	mexEvalString("drawnow;");
	
	cudaFree(W          );
	cudaFree(d          );
	cudaFree(gradW      );
	cudaFree(gradD      );
	cudaFree(meanSquareW);
	cudaFree(meanSquareD);
	cudaFree(momentumW  );
	cudaFree(momentumD  );
	cudaFree(eps        );
	cudaFree(epsDot     );
	cudaFree(nu         );
	cudaFree(t          );
	cudaFree(s          );
	cudaFree(a          );
	cudaFree(aDot       );
	cudaFree(u          );
	cudaFree(e          );
	cudaFree(rho        );
	cudaFree(delta      );
	cudaFree(nSpikes	);	
	cudaFree(totalSpikes); 
	cudaFree(classID	);	
	cudaFree(probability); 
	
	if(cublasHandleCreated)		cublasDestroy(handle);
}

slayer& slayer::operator = (const slayer &layer)
{
	if(this == &layer)	return *this;
	this->info        = layer.info;
	
	this->theta       = layer.theta;
	this->sigma       = layer.sigma;
	this->tauRho	  = layer.tauRho;
	this->scaleRho    = layer.scaleRho;
	this->batchSize   = layer.batchSize;
	this->batchStride = layer.batchStride;
	this->Ns          = layer.Ns;
	this->Ts          = layer.Ts;
	
	this->WSize		  = layer.WSize;
	this->dSize		  = layer.dSize;
	this->epsSize     = layer.epsSize;
	this->epsDotSize  = layer.epsDotSize;
	this->nuSize      = layer.nuSize;
	this->sSize       = layer.sSize;
	this->aSize       = layer.aSize;
	this->aDotSize    = layer.aDotSize;
	this->uSize       = layer.uSize;
	this->eSize       = layer.eSize;
	this->rhoSize     = layer.rhoSize;
	this->deltaSize   = layer.deltaSize;
	this->nSpikesSize	  = layer.nSpikesSize;	      
	this->totalSpikesSize = layer.totalSpikesSize; 
	this->classIDSize	  = layer.classIDSize;
	this->probabilitySize = layer.probabilitySize; 
	
	this->gradAlgo    = layer.gradAlgo;
	
	this->deviceID    = layer.deviceID;
	
	this->pre         = layer.pre;
	this->post        = layer.post;
	
	this->wInd  = new unsigned[info.nWeights * info.nDelayedSyn];
	this->dInd  = new unsigned[info.nDelays  * info.nDelayedSyn];
	for(unsigned i=0; i<info.nWeights * info.nDelayedSyn; ++i)	this->wInd[i] = layer.wInd[i];
	for(unsigned i=0; i<info.nDelays  * info.nDelayedSyn; ++i)	this->dInd[i] = layer.dInd[i];
	
	
	// mexPrintf("\n Ns * info.nNeurons = %d \n", Ns * info.nNeurons);
	checkCudaErrors( cudaMallocManaged(&eps   , sizeof(float) *    epsSize) );
	checkCudaErrors( cudaMallocManaged(&epsDot, sizeof(float) * epsDotSize) );
	checkCudaErrors( cudaMallocManaged(&nu    , sizeof(float) *     nuSize) );
	checkCudaErrors( cudaMallocManaged(&t     , sizeof(float) *         Ns) );	
	checkCudaErrors( cudaMallocManaged(&s     , sizeof(float) *      sSize) );
	checkCudaErrors( cudaMallocManaged(&a     , sizeof(float) *      aSize) );
	checkCudaErrors( cudaMallocManaged(&aDot  , sizeof(float) *   aDotSize) );
	checkCudaErrors( cudaMallocManaged(&u     , sizeof(float) *      uSize) );
	checkCudaErrors( cudaMallocManaged(&e     , sizeof(float) *      eSize) );
	checkCudaErrors( cudaMallocManaged(&rho   , sizeof(float) *    rhoSize) );
	checkCudaErrors( cudaMallocManaged(&delta , sizeof(float) *  deltaSize) );	
	if(info.type == Output)
	{
		checkCudaErrors( cudaMallocManaged(&nSpikes	   , sizeof(unsigned) * 	nSpikesSize ) );
		checkCudaErrors( cudaMallocManaged(&totalSpikes, sizeof(unsigned) * totalSpikesSize ) );
		checkCudaErrors( cudaMallocManaged(&classID	   , sizeof(unsigned) * 	classIDSize ) );
		checkCudaErrors( cudaMallocManaged(&probability, sizeof(float)    * probabilitySize ) );
	}
	else
	{
		nSpikes	  		= NULL;
		totalSpikes		= NULL;
		classID	  		= NULL;
		probability		= NULL;
	}	
	
	for(unsigned i=0; i<epsSize   ; ++i)	this->eps[i]    = layer.eps[i];
	for(unsigned i=0; i<epsDotSize; ++i)	this->epsDot[i] = layer.epsDot[i];
	for(unsigned i=0; i<nuSize    ; ++i)	this->nu[i]     = layer.nu[i];
	for(unsigned i=0; i<Ns        ; ++i)	this->t[i]      = layer.t[i];
	for(unsigned i=0; i<Ns * info.nNeurons; ++i)	
	{
		this->s[i]      = layer.s[i];
		this->u[i]      = layer.u[i];
		this->rho[i]    = layer.rho[i];
		this->delta[i]  = layer.delta[i];
	}
	for(unsigned i=0; i<Ns * info.nNeurons * info.nDelayedSyn; ++i)
	{
		this->a[i]      = layer.a[i];
		this->aDot[i]   = layer.aDot[i];
		this->e[i]      = layer.e[i];
	}	
	
	nanCount = layer.nanCount;
	
	// TODO
	// Copy weight elements
	W           = NULL;
	d           = NULL;
	gradW       = NULL;
	gradD       = NULL;
	meanSquareW = NULL;
	meanSquareD = NULL;
	momentumW   = NULL;
	momentumD   = NULL;
	
	cublasHandleCreated = false;
	// cublasCreate(&handle);
	// slayerio::cout << "= Operator\n";
	
	return *this;
}

int slayer::setupSynapse(std::vector<float> weight, std::vector<float> delay)	// wInd and dInd must be initialized before this
{
	// setting up a stream for weight and delay values to initialize the parameters
	float *wStream = new float[info.nWeights * info.nDelayedSyn];
	float *dStream = new float[info.nDelays  * info.nDelayedSyn];
	for(unsigned i=0; i<info.nWeights * info.nDelayedSyn; ++i)	wStream[i] = weight[wInd[i]];
	for(unsigned i=0; i<info.nDelays  * info.nDelayedSyn; ++i)	dStream[i] = delay [dInd[i]];
		
	if(this->info.type == Output)
	{
		// Output layer has no weights and zero valued delays to store
		// mexPrintf("This is Output layer. There is no next layer\n");
		if( cudaSuccess != cudaMallocManaged(&d, sizeof(float) * this->info.nNeurons) )		slayerio::error("Error allocating memory for delay vector");	
		for(unsigned i=0; i<this->info.nNeurons; ++i)	d[i] = 0;
	}
	else if((this+1)->info.type == Linear || 
			(this+1)->info.type == Planar ||
			(this+1)->info.type == Output )
	{
		// allocate memory for weights
		unsigned numelW = (this+1)->info.nNeurons * this->info.nNeurons * this->info.nDelayedSyn; //(this+1)->info.nNeurons x(this->info.nNeurons * this->info.nDelayedSyn) matrix
		if( cudaSuccess != cudaMallocManaged(&W          , sizeof(float) * numelW) )		slayerio::error("Error allocating GPU memory for weight matrix");
		if( cudaSuccess != cudaMallocManaged(&gradW      , sizeof(float) * numelW) )		slayerio::error("Error allocating GPU memory for weight gradient matrix");
		if( cudaSuccess != cudaMallocManaged(&meanSquareW, sizeof(float) * numelW) )		slayerio::error("Error allocating GPU memory for weight meansquare matrix");
		if( cudaSuccess != cudaMallocManaged(&momentumW  , sizeof(float) * numelW) )		slayerio::error("Error allocating GPU memory for weight momentum matrix");
		
		// initialize weight, weight gradient and mean square of weight
		for(unsigned i=0; i<numelW; ++i)
		{
			W[i]           = wStream[i];
			gradW[i]       = 0.0f;
			meanSquareW[i] = 1.0f;
			momentumW[i]   = 0.0f;
		}
				
		// allocate memory for delay
		unsigned numelD = this->info.nNeurons * this->info.nDelayedSyn;
		if( cudaSuccess != cudaMallocManaged(&d          , sizeof(float) * numelD) )	slayerio::error("Error allocating GPU memory for delay vector");
		if( cudaSuccess != cudaMallocManaged(&gradD      , sizeof(float) * numelD) )	slayerio::error("Error allocating GPU memory for delay gradient vector");
		if( cudaSuccess != cudaMallocManaged(&meanSquareD, sizeof(float) * numelD) )	slayerio::error("Error allocating GPU memory for delay meansquare vector");
		if( cudaSuccess != cudaMallocManaged(&momentumD  , sizeof(float) * numelD) )	slayerio::error("Error allocating GPU memory for delay momentum vector");
		
		// initialize delay, delay gradient and mean square of delay
		for(unsigned i=0; i<numelD; ++i)
		{
			d[i]           = dStream[i];
			gradD[i]       = 0.0f;
			meanSquareD[i] = 1.0f;
			momentumD[i]   = 0.0f;
		}		
	}
	else if((this+1)->info.type == Convolution)
	{
		// allocate memory for weights
		unsigned numelW = this->info.nWeights;
		if( cudaSuccess != cudaMallocManaged(&W          , sizeof(float) * numelW) )		slayerio::error("Error allocating GPU memory for weight matrix");
		if( cudaSuccess != cudaMallocManaged(&gradW      , sizeof(float) * numelW) )		slayerio::error("Error allocating GPU memory for weight gradient matrix");
		if( cudaSuccess != cudaMallocManaged(&meanSquareW, sizeof(float) * numelW) )		slayerio::error("Error allocating GPU memory for weight meansquare matrix");
		if( cudaSuccess != cudaMallocManaged(&momentumW  , sizeof(float) * numelW) )		slayerio::error("Error allocating GPU memory for weight momentum matrix");
		
		// initialize weight, weight gradient and mean square of weight
		for(unsigned i=0; i<numelW; ++i)
		{
			W[i]           = wStream[i];
			gradW[i]       = 0.0f;
			meanSquareW[i] = 1.0f;
			momentumW[i]   = 0.0f;
		}
		
		// allocate memory for delay
		unsigned numelD = this->info.nDelays;
		if( cudaSuccess != cudaMallocManaged(&d          , sizeof(float) * numelD) )	slayerio::error("Error allocating GPU memory for delay vector");
		if( cudaSuccess != cudaMallocManaged(&gradD      , sizeof(float) * numelD) )	slayerio::error("Error allocating GPU memory for delay gradient vector");
		if( cudaSuccess != cudaMallocManaged(&meanSquareD, sizeof(float) * numelD) )	slayerio::error("Error allocating GPU memory for delay meansquare vector");
		if( cudaSuccess != cudaMallocManaged(&momentumD  , sizeof(float) * numelD) )	slayerio::error("Error allocating GPU memory for delay momentum vector");
		
		// initialize delay, delay gradient and mean square of delay
		for(unsigned i=0; i<numelD; ++i)
		{
			d[i]           = dStream[i];
			gradD[i]       = 0.0f;
			meanSquareD[i] = 1.0f;
			momentumD[i]   = 0.0f;
		}		
	}
	else if((this+1)->info.type == Aggregation)
	{
		// allocate memory for weights
		unsigned numelW = this->info.nWeights;
		if( cudaSuccess != cudaMallocManaged(&W          , sizeof(float) * numelW) )		slayerio::error("Error allocating GPU memory for weight matrix");
		if( cudaSuccess != cudaMallocManaged(&gradW      , sizeof(float) * numelW) )		slayerio::error("Error allocating GPU memory for weight gradient matrix");
		if( cudaSuccess != cudaMallocManaged(&meanSquareW, sizeof(float) * numelW) )		slayerio::error("Error allocating GPU memory for weight meansquare matrix");
		if( cudaSuccess != cudaMallocManaged(&momentumW  , sizeof(float) * numelW) )		slayerio::error("Error allocating GPU memory for weight momentum matrix");
		
		// initialize weight, weight gradient and mean square of weight
		for(unsigned i=0; i<numelW; ++i)
		{
			W[i]           = 1.1f * theta;
			gradW[i]       = 0.0f;
			meanSquareW[i] = 1.0f;
			momentumW[i]   = 0.0f;
		}
		
		// allocate memory for delay
		unsigned numelD = this->info.nNeurons;
		if( cudaSuccess != cudaMallocManaged(&d          , sizeof(float) * numelD) )	slayerio::error("Error allocating GPU memory for delay vector");
		if( cudaSuccess != cudaMallocManaged(&gradD      , sizeof(float) * numelD) )	slayerio::error("Error allocating GPU memory for delay gradient vector");
		if( cudaSuccess != cudaMallocManaged(&meanSquareD, sizeof(float) * numelD) )	slayerio::error("Error allocating GPU memory for delay meansquare vector");
		if( cudaSuccess != cudaMallocManaged(&momentumD  , sizeof(float) * numelD) )	slayerio::error("Error allocating GPU memory for delay momentum vector");
		
		// initialize delay, delay gradient and mean square of delay
		for(unsigned i=0; i<numelD; ++i)
		{
			d[i]           = 0.0f;
			gradD[i]       = 0.0f;
			meanSquareD[i] = 1.0f;
			momentumD[i]   = 0.0f;
		}		
	}	
	
	delete [] wStream;
	delete [] dStream;
	
	return 1;
}

void slayer::loadSpikes(std::string inPath, std::vector<unsigned> index, float tSample)
{
	// cudaMemAdvise(s, sizeof(float) * sSize, cudaMemAdviseSetPreferredLocation,  cudaCpuDeviceId);
	std::stringstream sout;	
	unsigned tSampleLen = unsigned(tSample/Ts);

	switch(info.type)	
	{
		case Output:
		case Linear:
			// initialize all the elements of input spike to 0	
			for(unsigned i=0; i<info.nNeurons * Ns; ++i)	s[i] = 0.0f;
			
			// read spike data from file
			for(unsigned i=0; i<index.size(); ++i)
			{
				sout.str(std::string());	// clear string stream
				sout << index[i] << ".bs1";
				std::string spikePath = inPath + sout.str();
				
				std::vector<unsigned> neuronID;
				std::vector<char> polarity;
				std::vector<float> timeID;
				unsigned nSpikes = read1DBinSpikes(spikePath, neuronID, timeID, polarity);
				
				if(nSpikes == 0)
				{
					sout.str(std::string());		// clear string stream
					sout << "Error: could not read " << spikePath << std::endl;
					slayerio::error(sout.str().c_str());
				}
				
				unsigned negSpikeOffset = (info.dimension[2] == 2) ? info.nNeurons/2 : 0;
				for(unsigned eID=0; eID < nSpikes; ++eID)
				{
					if(info.dimension[2] == 1)	// positive spikes only
					{
						unsigned nID = neuronID[eID];
						unsigned tID = unsigned(timeID[eID]/Ts);
						if(nID < info.nNeurons && tID < tSampleLen)
							s[nID * Ns + tID + i*tSampleLen] = polarity[eID]/Ts;		// s[nID][tID + i*tSampleLen] = polarity[eID]/Ts;
					}
					else						// positive and negative spikes
					{
						unsigned nID = (polarity[eID] > 0) ? neuronID[eID] : neuronID[eID] + negSpikeOffset;
						unsigned tID = unsigned(timeID[eID]/Ts);
						if(nID < info.nNeurons && tID < tSampleLen)
							s[nID * Ns + tID + i*tSampleLen] = 1/Ts;							// s[nID][tID + i*tSampleLen] = 1/Ts;
					}
				}
			}
			
			break;
		case Planar:
			// xMax = info.dimension[0]
			// yMax = info.dimension[1]
			
			// initialize all the elements of input spike to 0
			for(unsigned i=0; i<info.nNeurons * Ns; ++i)	s[i] = 0.0f;	
			
			// read spike data from file
			for(unsigned i=0; i<index.size(); ++i)
			{
				sout.str(std::string());	// clear string stream
				sout << index[i] << ".bs2";
				std::string spikePath = inPath + sout.str();
				std::vector<unsigned char> xPos;
				std::vector<unsigned char> yPos;
				std::vector<char> polarity;
				std::vector<float> timeID;
				unsigned nSpikes = read2DBinSpikes(spikePath, xPos, yPos, timeID, polarity);
				if(nSpikes == 0)
				{
					sout.str(std::string());		// clear string stream
					sout << "Error: could not read " << spikePath << std::endl;
					slayerio::error(sout.str().c_str());
				}
				
				unsigned negSpikeOffset = (info.dimension[2] == 2) ? info.nNeurons/2 : 0;
				for(unsigned eID=0; eID < nSpikes; ++eID)
				{
					if(xPos[eID] < info.dimension[0] && yPos[eID] < info.dimension[1])
					{
						if(info.dimension[2] == 1)	// positive spikes only
						{
							unsigned nID = xPos[eID] + yPos[eID] * info.dimension[0];	// conversion to linear index
							unsigned tID = unsigned(timeID[eID]/Ts);
							if(nID < info.nNeurons && tID < tSampleLen)
								s[nID * Ns + tID + i*tSampleLen] = polarity[eID]/Ts;		// s[nID][tID + i*tSampleLen] = polarity[eID]/Ts;
						}
						else						// positive and negative spikes
						{
							unsigned nID = xPos[eID] + yPos[eID] * info.dimension[0];	// conversion to linear index
							unsigned tID = unsigned(timeID[eID]/Ts);
							         nID+= (polarity[eID] > 0) ? 0 : negSpikeOffset;
							if(nID < info.nNeurons && tID < tSampleLen)
								s[nID * Ns + tID + i*tSampleLen] = 1/Ts;							// s[nID][tID + i*tSampleLen] = 1/Ts;
						}
					}
				}
			}
			
			break;
		default:
			sout.str(std::string());		// clear string stream
			sout << "Error: Input layer can be either Linear or Planar. Layer type " << info.typeStr() << "detected." << std::endl;
			sout << "Aborting further process.";
			slayerio::error(sout.str().c_str());
	}
	
	// cudaMemPrefetchAsync(s, sizeof(float) * sSize, deviceID[0]);
	// cudaMemAdvise(s, sizeof(float) * sSize, cudaMemAdviseSetPreferredLocation,  deviceID[0]);
}

void slayer::loadAvgSpikes(std::string inPath, std::vector<unsigned> index, float tSample)
{
	// cudaMemAdvise(s, sizeof(float) * sSize, cudaMemAdviseSetPreferredLocation,  cudaCpuDeviceId);
	// cudaMemPrefetchAsync(s, sizeof(float) * sSize, cudaCpuDeviceId);
	
	std::stringstream sout;
	// unsigned tSampleLen = unsigned(tSample/Ts);
	switch(info.type)	
	{
		case Output:
			// initialize all the elements of input spike to 0
			for(unsigned i=0; i<info.nNeurons * Ns; ++i)	s[i] = 0.0f;	
			
			// read spike data from file
			for(unsigned i=0; i<index.size(); ++i)
			{
				sout.str(std::string());	// clear string stream
				sout << index[i] << ".bsa";
				std::string spikePath = inPath + sout.str();
				
				std::vector<unsigned> neuronID;
				std::vector<char> polarity;
				std::vector<float> timeID;
				slayerio::error("TODO: loadAvgSpikes not yet fully implelented\n");
				// TODO first write routine for avgBinSpikes
				// unsigned nSpikes = readAvgBinSpikes(spikePath, neuronID, timeID, polarity);
				// if(nSpikes == 0)
				// {
					// sout.str(std::string());		// clear string stream
					// sout << "Error: could not read " << spikePath << std::endl;
					// mexErrMsgTxt(sout.str().c_str());
				// }
				
				// for(unsigned eID=0; eID < nSpikes; ++eID)
				// {
					// unsigned nID = neuronID[eID];
					// unsigned tID = unsigned(timeID[eID]/Ts);
					// if(nID < info.nNeurons && tID < tSampleLen)
						// s[nID][tID + i*tSampleLen] = polarity[eID]/Ts;
				// }
			}
			break;
		default:
			sout.str(std::string());		// clear string stream
			sout << "Error: loadAvgSpikes routine can only be called for Output layer type. Layer type " << info.typeStr() << "detected." << std::endl;
			sout << "Aborting further process.";
			slayerio::error(sout.str().c_str());
	}
	
	// cudaMemPrefetchAsync(s, sizeof(float) * sSize, deviceID[0]);
	// cudaMemPrefetchAsync(s, sizeof(float) * sSize, deviceID[0]);
}

void slayer::loadAppxSpikes(std::string inPath, std::vector<unsigned> index, const slayer &spikeLayer, float tSample)
{
	// cudaMemAdvise(s, sizeof(float) * sSize, cudaMemAdviseSetPreferredLocation,  cudaCpuDeviceId);
	// cudaMemPrefetchAsync(s, sizeof(float) * sSize, cudaCpuDeviceId);
	
	std::stringstream sout;
	// unsigned tSampleLen = unsigned(tSample/Ts);
	switch(info.type)	
	{
		case Output:
			// initialize all the elements of input spike to 0
			for(unsigned i=0; i<info.nNeurons * Ns; ++i)	s[i] = 0.0f;	
			
			// read spike data from file
			for(unsigned i=0; i<index.size(); ++i)
			{
				sout.str(std::string());	// clear string stream
				sout << index[i] << ".bs1";
				std::string spikePath = inPath + sout.str();
				
				std::vector<unsigned> neuronID;
				std::vector<char> polarity;
				std::vector<float> timeID;
				slayerio::error("TODO: loadAppxSpikes not yet fully implelented\n");
				// TODO first write routine for avgBinSpikes
				// unsigned nSpikes = read1DBinSpikes(spikePath, neuronID, timeID, polarity);
				// if(nSpikes == 0)
				// {
					// sout.str(std::string());		// clear string stream
					// sout << "Error: could not read " << spikePath << std::endl;
					// mexErrMsgTxt(sout.str().c_str());
				// }
				
				// for(unsigned eID=0; eID < nSpikes; ++eID)
				// {
					// unsigned nID = neuronID[eID];
					// unsigned tID = unsigned(timeID[eID]/Ts);
					// if(nID < info.nNeurons && tID < tSampleLen)
						// s[nID][tID + i*tSampleLen] = polarity[eID]/Ts;
				// }
			}
			break;
		default:
			sout.str(std::string());		// clear string stream
			sout << "Error: loadAppxSpikes routine can only be called for Output layer type. Layer type " << info.typeStr() << "detected." << std::endl;
			sout << "Aborting further process.";
			slayerio::error(sout.str().c_str());
	}
	
	// cudaMemPrefetchAsync(s, sizeof(float) * sSize, deviceID[0]);
	// cudaMemPrefetchAsync(s, sizeof(float) * sSize, deviceID[0]);
}

void slayer::loadNumSpikes(std::string inPath, std::vector<unsigned> index, const slayer &spikeLayer, float tSample)
{
	// cudaMemAdvise(s, sizeof(float) * sSize, cudaMemAdviseSetPreferredLocation,  cudaCpuDeviceId);
	// cudaMemPrefetchAsync(s, sizeof(float) * sSize, cudaCpuDeviceId);
	
	std::stringstream sout;
	unsigned tSampleLen = unsigned(tSample/Ts);
	switch(info.type)	
	{
		case Output:
			// initialize all the elements of input spike to 0
			for(unsigned i=0; i<info.nNeurons * Ns; ++i)
			{
				e[i] = 0.0f;
				s[i] = 0.0f;	
			}
			
			// read spike data from file
			for(unsigned i=0; i<index.size(); ++i)
			{
				sout.str(std::string());	// clear string stream
				sout << index[i] << ".bsn";
				std::string spikePath = inPath + sout.str();
				
				std::vector<unsigned> neuronID;
				std::vector<unsigned> numSpikes;
				std::vector<float> tSt;
				std::vector<float> tEn;
				unsigned nSpikes = read1DNumSpikes(spikePath, neuronID, tSt, tEn, numSpikes);
				if(nSpikes == 0)
				{
					sout.str(std::string());		// clear string stream
					sout << "Error: could not read " << spikePath << std::endl;
					slayerio::error(sout.str().c_str());
				}				
				
				numSpikesEventToTensor(i, spikeLayer, neuronID, tSt, tEn, numSpikes, tSampleLen);
				
				// for(unsigned rID=0; rID < nSpikes; ++rID)
				// {
					// unsigned nID = neuronID[rID];
					// unsigned startID = unsigned(tSt[rID]/Ts);
					// unsigned endID   = unsigned(tEn[rID]/Ts);
					// if(endID >= tSampleLen)	endID = tSampleLen - 1;	// boundary limiting
					// // count the number of spikes acutally present in the range
					// float actualSpikes = 0;
					// for(unsigned tID=startID; tID < endID; ++tID)	
					// {
						// // actualSpikes += spikeLayer.s[nID][tID + i*tSampleLen] * Ts;
						// // s[nID][tID + i*tSampleLen] = spikeLayer.s[nID][tID + i*tSampleLen];
						// actualSpikes += spikeLayer.s[nID*Ns + tID + i*tSampleLen] * Ts;
						// s[nID*Ns + tID + i*tSampleLen] = spikeLayer.s[nID*Ns + tID + i*tSampleLen];
					// }
					
					// // float avgValue = (int(numSpikes[rID]) - actualSpikes)/(tEn[rID] - tSt[rID]);
					// float avgValue = (int(numSpikes[rID]) - actualSpikes)/(endID - startID);
					// // mexPrintf("Actual spikes = %g, Desired spikes = %d\n", actualSpikes, numSpikes[rID]);
					// // mexPrintf("AvgValue = %g\n", avgValue);
					// for(unsigned tID=startID; tID < endID; ++tID)
						// e[nID*Ns + tID + i*tSampleLen] += avgValue/Ts;		// e[nID][tID + i*tSampleLen] += avgValue/Ts;
				// }
			}
			break;
		default:
			sout.str(std::string());		// clear string stream
			sout << "Error: loadNumSpikes routine can only be called for Output layer type. Layer type " << info.typeStr() << "detected." << std::endl;
			sout << "Aborting further process.";
			slayerio::error(sout.str().c_str());
	}
	
	// for(unsigned id=0; id<info.nNeurons; ++id)
	// {
		// for(unsigned i=0; i<Ns; ++i)
			// mexPrintf("%g ", s[id][i]);
		// mexPrintf("\n");
	// }
	
	// mexPrintf("\nIn loadNumSpikes, sSize = %d\n", sSize);
	// cudaMemPrefetchAsync(s, sizeof(float) * sSize, deviceID[0]);
	// cudaMemPrefetchAsync(s, sizeof(float) * sSize, deviceID[0]);
}

void slayer::loadNumSpikes(std::vector<unsigned> index, const slayer &spikeLayer, const YAML::Node tgtSpikeRegion, const YAML::Node tgtSpikeCount, float tSample)
{
	std::stringstream sout;
	unsigned tSampleLen = unsigned(tSample/Ts);
	switch(info.type)	
	{
		case Output:
			// initialize all the elements of input spike to 0
			for(unsigned i=0; i<info.nNeurons * Ns; ++i)
			{
				e[i] = 0.0f;
				s[i] = 0.0f;	
			}
			
			// read spike data from file
			for(unsigned i=0; i<index.size(); ++i)
			{
				std::vector<unsigned> neuronID;
				std::vector<unsigned> numSpikes;
				std::vector<float> tSt;
				std::vector<float> tEn;
				unsigned nSpikes = generate1DNumSpikes(neuronID, tSt, tEn, numSpikes, index[i], info.nNeurons, 
													   tgtSpikeCount["true"].as<int>(),
													   tgtSpikeCount["false"].as<int>(),
													   tgtSpikeRegion["start"].as<float>(),
													   tgtSpikeRegion["stop"].as<float>());
								
				numSpikesEventToTensor(i, spikeLayer, neuronID, tSt, tEn, numSpikes, tSampleLen);
			}
			break;
		default:
			sout.str(std::string());		// clear string stream
			sout << "Error: loadNumSpikes routine can only be called for Output layer type. Layer type " << info.typeStr() << "detected." << std::endl;
			sout << "Aborting further process.";
			slayerio::error(sout.str().c_str());
	}
}

void  slayer::loadProbSpikes(std::vector<unsigned> index, slayer &spikeLayer, float tSample, unsigned numSamplesInWindow)
{
	// cudaMemAdvise(s, sizeof(float) * sSize, cudaMemAdviseSetPreferredLocation,  cudaCpuDeviceId);
	// cudaMemPrefetchAsync(s, sizeof(float) * sSize, cudaCpuDeviceId);
	
	std::stringstream sout;
	unsigned tSampleLen = unsigned(tSample/Ts);

	dim3 block, thread;

	switch(info.type)	
	{
		case Output:
		
			thread.x = 128;
			thread.y = 4;
			block.x = ceil(1.0f * Ns / thread.x);
			block.y = ceil(1.0f * info.nNeurons / thread.y);
			countSpikesKernel<<< block, thread >>>(spikeLayer.nSpikes, spikeLayer.s, numSamplesInWindow, info.nNeurons, Ns);
			cudaDeviceSynchronize();
			
			thread.y = 1;
			block.y  = 1;
			sumSpikesKernel<<< block, thread >>>(spikeLayer.totalSpikes, spikeLayer.nSpikes, info.nNeurons, Ns);
			cudaDeviceSynchronize();
			
			thread.x = 128;
			thread.y = 4;
			block.x = ceil(1.0f * Ns / thread.x);
			block.y = ceil(1.0f * info.nNeurons / thread.y);
			spikeProbabilityKernel<<< block, thread >>>(spikeLayer.probability, spikeLayer.nSpikes, spikeLayer.totalSpikes, info.nNeurons, Ns);
			cudaDeviceSynchronize();
			
			for(unsigned i=0; i<index.size(); ++i)
			{
				unsigned tID = i*tSampleLen;
				memsetKernel<<< ceil(1.0f * tSampleLen/128), 128 >>>(classID + tID, index[i], Ns);
			}
			cudaDeviceSynchronize();
			break;
		default:
			sout.str(std::string());		// clear string stream
			sout << "Error: loadNumSpikes routine can only be called for Output layer type. Layer type " << info.typeStr() << "detected." << std::endl;
			sout << "Aborting further process.";
			slayerio::error(sout.str().c_str());
	}
	// classID[0] = 100;
	// mexPrintf("\nloadProbSpikes::Pointer Address : %u", classID);
	// for(unsigned i=0; i<Ns; ++i)
	// {
		// if(i%350 == 0)	std::cout << '\n';
		// mexPrintf("%d ", classID[i]);
	// }
	handleCudaErrorCheck("slayer.h/loadProbSpikes");
}

void slayer::fwdProp()
{
	// slayerio::cout << deviceID << " Starting ForwardProp\n";
	fpruntime.clear();
	float memcpyTime = 0;
	timeit profiler;
	
	if(profile)	memcpyTime += profiler.record();
	
	// (this-1)->a = CONV( (this-1)->s DELAYED BY (this-1)->d, (this-1)->eps )
	cudaMemAdvise((this-1)->a, sizeof(float) * (this-1)->aSize, cudaMemAdviseSetPreferredLocation, deviceID);
	cudaMemAdvise((this-1)->s, sizeof(float) * (this-1)->sSize, cudaMemAdviseSetPreferredLocation, deviceID);
	cudaMemAdvise((this-1)->d, sizeof(float) * (this-1)->dSize, cudaMemAdviseSetReadMostly,        deviceID);	// device argument is ignored
	
	cudaSetDevice(deviceID);
	delayedConv((this-1)->a, 
				(this-1)->s, (this-1)->eps, (this-1)->d, 
				(this-1)->Ns, 
				(this-1)->epsSize, 
				(this-1)->info.nNeurons, 
				(this-1)->info.nDelayedSyn, 
				(this-1)->Ts );
		
	if(profile)
	{
		cudaDeviceSynchronize();
		fpruntime.push_back(profiler.record());
	}
	
	// (this-1)->aDot = CONV( (this-1)->s DELAYED BY (this-1)->d, (this-1)->epsDot );
	cudaMemAdvise((this-1)->aDot, sizeof(float) * (this-1)->aDotSize, cudaMemAdviseSetPreferredLocation, deviceID);
	
	cudaSetDevice(deviceID);
	delayedConv((this-1)->aDot, 
				(this-1)->s, (this-1)->epsDot, (this-1)->d, 
				(this-1)->Ns, 
				(this-1)->epsDotSize, 
				(this-1)->info.nNeurons, 
				(this-1)->info.nDelayedSyn, 
				(this-1)->Ts );
	
	cudaDeviceSynchronize();
	
	if(profile)	fpruntime.push_back(profiler.record());
	
	// this->u = (this-1)->W * (this-1)->a) 
	// slayerio::cout << "Calculating Initial Membrante Potential\n";
	cudaMemAdvise( this->u   , sizeof(float) * this->uSize    , cudaMemAdviseSetPreferredLocation, deviceID);
	cudaMemAdvise((this-1)->W, sizeof(float) * (this-1)->WSize, cudaMemAdviseSetReadMostly,        deviceID);	// device argument is ignored
	
	cudaSetDevice(deviceID);
	switch(this->info.type)
	{
		case Linear: 
		case Planar: 
		case Output:
			multWa(handle, this->u, (this-1)->W, (this-1)->a, this->info.nNeurons, (this-1)->info.nNeurons, this->Ns);
			break;
		case Convolution:
			#if defined CUSTOM_PROPAGATION
				// multWaConv(handle[0], this->u, (this-1)->W, (this-1)->a, this->info, (this-1)->info, Ns);
				multWaConv(this->u, (this-1)->W, (this-1)->a, this->info, (this-1)->info, Ns);
			#else
				this->cudnn.fwdConvolution((this-1)->a, this->u, (this-1)->W);
			#endif
			break;
		case Aggregation:
			multWaAggr(this->u, (this-1)->W, (this-1)->a, this->info, (this-1)->info, Ns);
			break;
	}
	
	
	cudaDeviceSynchronize();
	
	if(profile)
	{
		float sumU = sumReduce(this->u, this->uSize);
		if(isnan(sumU) == 1)	nanCount[0]++;
		fpruntime.push_back(profiler.record());
	}
	
	// spike detection and refractory response
	// slayerio::cout << "Calculating Final Membrante Potential and Spike Time\n";
	cudaSetDevice(deviceID);
	getSpikes(this->s, this->u, this->nu, this->info.nNeurons, this->nuSize, Ns, batchStride, theta, Ts);	// non iterative version
	cudaDeviceSynchronize();	
	
	if(profile)
	{
		cudaDeviceSynchronize();
		fpruntime.push_back(profiler.record());
		memcpyTime += profiler.record();
		fpruntime.push_back(memcpyTime);
	}
	
	cudaDeviceSynchronize();
	// slayerio::cout << "Forward Propagation Complete\n\n";
	
	handleCudaErrorCheck("slayer.h/fwdProp");
}

float slayer::error(slayer &desiredLayer, tgtPattern patternType)
{
	if(this->info.nNeurons != desiredLayer.info.nNeurons)	slayerio::error("Error: number of output neurons and desired nuerons do not match");
	float J;
	switch(patternType)
	{
		case SpikeTime:		J = errorSpikeTime (desiredLayer);	break;
		case NumSpikes:		J = errorNumSpikes (desiredLayer);	break;
		case ProbSpikes:	J = errorProbSpikes(desiredLayer);	break;
		default:
			slayerio::error("ERROR: error function is implemented for following pattern types only\nSpikeTime, NumSpikes, ProbSpikes");
	}
	handleCudaErrorCheck("slayer.h/error");
	
	// mexPrintf("J = %g", J);
	return J;
	// return 100;
}

void slayer::backPropSlayer()
{
	bpruntime.clear();
	float memcpyTime = 0;
	timeit profiler;
	
	// given error at the preceding layer l+1,
	// calculate layer[l+1].rho, layer[l+1].delta
	cudaSetDevice(deviceID);
	(this+1)->evalRho();
	
	if(profile)
	{
		cudaDeviceSynchronize();
		bpruntime.push_back(profiler.record());
	}
	
	// (this+1)->delta = CORR( (this+1)->e DELAYED BY (this+1)->d, (this+1)->eps )
	// for(int k=1; k<info.nDelayedSyn; ++k)	(this-1)->delayShift(-k);	// shift the a signals for extra delayed synaptic conenction
	
	cudaMemAdvise((this+1)->delta, sizeof(float) * (this+1)->deltaSize, cudaMemAdviseSetPreferredLocation, deviceID);
	cudaMemAdvise((this+1)->e,     sizeof(float) * (this+1)->eSize    , cudaMemAdviseSetPreferredLocation, deviceID);
	if((this+1)->info.type != Output)	// output layer has no delay
		cudaMemAdvise((this+1)->d, sizeof(float) * (this+1)->dSize    , cudaMemAdviseSetReadMostly,        deviceID);	// device argument is ignored
	
	cudaSetDevice(deviceID);
	if((this+1)->info.type != Output)
	{
		delayedCorr((this+1)->delta,
					(this+1)->e, (this+1)->eps, (this+1)->d,
					(this+1)->Ns,
					(this+1)->epsSize,
					(this+1)->info.nNeurons,
					(this+1)->info.nDelayedSyn,
					(this+1)->Ts );
	}
	else
	{
		corr((this+1)->delta,
			 (this+1)->e, (this+1)->eps,
			 (this+1)->Ns,
			 (this+1)->epsSize,
			 (this+1)->info.nNeurons,
			 (this+1)->info.nDelayedSyn,
			 (this+1)->Ts );
	}
	
	cudaDeviceSynchronize();
	
	if(profile)	bpruntime.push_back(profiler.record());
	
	// (this+1)->delta *= (this+1)->rho;
	dim3 thread, block;
	thread.x = 128;
	thread.y = 8;
	block.x = ceil(1.0f * (this+1)->Ns           /thread.x);
	block.y = ceil(1.0f * (this+1)->info.nNeurons/thread.y);
	if(block.y >= 65535)	slayerio::error("Error: maximum blockDim.y exceeded at slayer.cpp/backProp.");
	if(block.z >= 65535)	slayerio::error("Error: maximum blockDim.z exceeded at slayer.cpp/backProp.");
	
	cudaSetDevice(deviceID);
	// ignoring the refractory dynamics
	multKernel<<< block, thread >>>((this+1)->delta, (this+1)->rho, (this+1)->Ns, (this+1)->info.nNeurons);
	// including the refractory dynamics
	// evalDeltaRefractory((this+1)->delta, (this+1)->rho, (this+1)->nuInDevice[0], (this+1)->info.nNeurons, (this+1)->nuSize, (this+1)->Ns, (this+1)->Ts);
	
	cudaDeviceSynchronize();
	
	if(profile)	bpruntime.push_back(profiler.record());
	
	// this->e = TRANSPOSE( this->W ) * (this+1)->delta
	// slayerio::cout << "Calculating error\n";
	cudaMemAdvise(this->e, sizeof(float) * this->eSize, cudaMemAdviseSetPreferredLocation, deviceID);
	cudaMemAdvise(this->W, sizeof(float) * this->WSize, cudaMemAdviseSetReadMostly,        deviceID);
	
	cudaSetDevice(deviceID);
	switch((this+1)->info.type)
	{
		case Linear: 
		case Planar: 
		case Output:
			multWdelta(handle, this->e, this->W, (this+1)->delta, this->info.nNeurons * this->info.nDelayedSyn, (this+1)->info.nNeurons, this->Ns);
			// multWdelta(h, this->e, this->W, (this+1)->delta, this->info.nNeurons * this->info.nDelayedSyn, (this+1)->info.nNeurons, this->Ns);
			break;
		case Convolution:
			#if defined CUSTOM_PROPAGATION
				// multWdeltaConv(handle[0], this->e, this->W, (this+1)->delta, this->info, (this+1)->info, Ns);
				multWdeltaConv(this->e, this->W, (this+1)->delta, this->info, (this+1)->info, Ns);
			#else
				(this+1)->cudnn.bwdConvolutionData(this->e, (this+1)->delta, this->W);
			#endif
			break;
		case Aggregation:
			multWdeltaAggr(this->e, this->W, (this+1)->delta, this->info, (this+1)->info, Ns);
			break;
	}
	
	
	
	if(profile)
	{
		cudaDeviceSynchronize();
		float sumE = sumReduce(this->e, this->eSize);
		if(isnan(sumE) == 1)	nanCount[1]++;
		bpruntime.push_back(profiler.record());
	}
	
	// this->gradW = INT( (this+1)->delta * this->a )
	// slayerio::cout << "Calculating gradient\n";
	cudaSetDevice(deviceID);
	switch((this+1)->info.type)
	{
		case Linear: 
		case Planar: 
		case Output:
			multAdelta(handle, this->gradW, (this+1)->delta, this->a, (this+1)->info.nNeurons, this->info.nNeurons * this->info.nDelayedSyn, this->Ns, this->Ts);
			break;
		case Convolution:
			#if defined CUSTOM_PROPAGATION
				multAdeltaConv(handle, this->gradW, (this+1)->delta, this->a, this->info, (this+1)->info, this->Ns, this->Ts);
			#else
				(this+1)->cudnn.bwdConvolutionFilter(this->a, (this+1)->delta, this->gradW, this->Ts);
			#endif
			break;
		case Aggregation:
			// no weight update
			break;
	}
	
	cudaDeviceSynchronize();
	float sumW = sumReduce(this->gradW, this->WSize);
	if(isnan(sumW) == 1)	memsetKernel<<< ceil(1.0f * this->WSize / 128), 128 >>>(this->gradW, 0.0f, this->WSize);
	
	if(profile)
	{
		if(isnan(sumW) == 1)	nanCount[2]++;
		bpruntime.push_back(profiler.record());
	}
	
	// this->gradD = - INT( this->e * this->aDot )
	cudaSetDevice(deviceID);
	switch((this+1)->info.type)
	{
		case Linear: 
		case Planar: 
		case Output:
		case Convolution:
			multAdotDelta(handle, this->gradD, this->e, this->aDot, this->info.nNeurons, this->info.nDelayedSyn, this->Ns, this->Ts);
			break;
		case Aggregation:
			// no delay update??
			break;
	}
	
	if(profile)
	{
		cudaDeviceSynchronize();
		bpruntime.push_back(profiler.record());
		memcpyTime += profiler.record();
		bpruntime.push_back(memcpyTime);
	}
	
	// slayerio::cout << "Backpropagation Complete\n\n";
	handleCudaErrorCheck("slayer.h/backPropSlayer");
}

void slayer::backPropSlayerZero()
{
	bpruntime.clear();
	float memcpyTime = 0;
	timeit profiler;
	
	// given error at the preceding layer l+1,
	// calculate layer[l+1].rho, layer[l+1].delta
	cudaSetDevice(deviceID);
	(this+1)->evalRho();
	
	if(profile)
	{
		cudaDeviceSynchronize();
		bpruntime.push_back(profiler.record());
	}
	
	// (this+1)->delta = CORR( (this+1)->e DELAYED BY (this+1)->d, (this+1)->eps )
	// for(int k=1; k<info.nDelayedSyn; ++k)	(this-1)->delayShift(-k);	// shift the a signals for extra delayed synaptic conenction
	
	cudaMemAdvise((this+1)->delta, sizeof(float) * (this+1)->deltaSize, cudaMemAdviseSetPreferredLocation, deviceID);
	cudaMemAdvise((this+1)->e,     sizeof(float) * (this+1)->eSize    , cudaMemAdviseSetPreferredLocation, deviceID);
	if((this+1)->info.type != Output)	// output layer has no delay
		cudaMemAdvise((this+1)->d, sizeof(float) * (this+1)->dSize    , cudaMemAdviseSetReadMostly,        deviceID);	// device argument is ignored
	
	cudaSetDevice(deviceID);
	// if((this+1)->info.type != Output)
	// {
		// delayedCorr((this+1)->delta,
					// (this+1)->e, (this+1)->eps, (this+1)->d,
					// (this+1)->Ns,
					// (this+1)->epsSize,
					// (this+1)->info.nNeurons,
					// (this+1)->info.nDelayedSyn,
					// (this+1)->Ts );
	// }
	// else
	// {
		// corr((this+1)->delta,
			 // (this+1)->e, (this+1)->eps,
			 // (this+1)->Ns,
			 // (this+1)->epsSize,
			 // (this+1)->info.nNeurons,
			 // (this+1)->info.nDelayedSyn,
			 // (this+1)->Ts );
	// }
	
	// In SLAYER ZERO, we are looking to remove the correlation process from the pipeline
	// assuming single delayed synaptic connections
	cudaMemcpy((this+1)->delta, (this+1)->e, sizeof(float) * (this+1)->info.nNeurons * Ns, cudaMemcpyDefault);
	
	cudaDeviceSynchronize();
	
	if(profile)	bpruntime.push_back(profiler.record());
	
	// (this+1)->delta *= (this+1)->rho;
	dim3 thread, block;
	thread.x = 128;
	thread.y = 8;
	block.x = ceil(1.0f * (this+1)->Ns           /thread.x);
	block.y = ceil(1.0f * (this+1)->info.nNeurons/thread.y);
	if(block.y >= 65535)	slayerio::error("Error: maximum blockDim.y exceeded at slayer.cpp/backProp.");
	if(block.z >= 65535)	slayerio::error("Error: maximum blockDim.z exceeded at slayer.cpp/backProp.");
	
	cudaSetDevice(deviceID);
	// ignoring the refractory dynamics
	multKernel<<< block, thread >>>((this+1)->delta, (this+1)->rho, (this+1)->Ns, (this+1)->info.nNeurons);
	// including the refractory dynamics
	// evalDeltaRefractory((this+1)->delta, (this+1)->rho, (this+1)->nuInDevice[0], (this+1)->info.nNeurons, (this+1)->nuSize, (this+1)->Ns, (this+1)->Ts);
	
	cudaDeviceSynchronize();
	
	if(profile)	bpruntime.push_back(profiler.record());
	
	// this->e = TRANSPOSE( this->W ) * (this+1)->delta
	// slayerio::cout << "Calculating error\n";
	cudaMemAdvise(this->e, sizeof(float) * this->eSize, cudaMemAdviseSetPreferredLocation, deviceID);
	cudaMemAdvise(this->W, sizeof(float) * this->WSize, cudaMemAdviseSetReadMostly,        deviceID);
	
	cudaSetDevice(deviceID);
	switch((this+1)->info.type)
	{
		case Linear: 
		case Planar: 
		case Output:
			multWdelta(handle, this->e, this->W, (this+1)->delta, this->info.nNeurons * this->info.nDelayedSyn, (this+1)->info.nNeurons, this->Ns);
			// multWdelta(h, this->e, this->W, (this+1)->delta, this->info.nNeurons * this->info.nDelayedSyn, (this+1)->info.nNeurons, this->Ns);
			break;
		case Convolution:
			#if defined CUSTOM_PROPAGATION
				// multWdeltaConv(handle[0], this->e, this->W, (this+1)->delta, this->info, (this+1)->info, Ns);
				multWdeltaConv(this->e, this->W, (this+1)->delta, this->info, (this+1)->info, Ns);
			#else
				(this+1)->cudnn.bwdConvolutionData(this->e, (this+1)->delta, this->W);
			#endif
			break;
		case Aggregation:
			multWdeltaAggr(this->e, this->W, (this+1)->delta, this->info, (this+1)->info, Ns);
			break;
	}
	
	
	
	if(profile)
	{
		cudaDeviceSynchronize();
		float sumE = sumReduce(this->e, this->eSize);
		if(isnan(sumE) == 1)	nanCount[1]++;
		bpruntime.push_back(profiler.record());
	}
	
	// this->gradW = INT( (this+1)->delta * this->a )
	// slayerio::cout << "Calculating gradient\n";
	cudaSetDevice(deviceID);
	switch((this+1)->info.type)
	{
		case Linear: 
		case Planar: 
		case Output:
			multAdelta(handle, this->gradW, (this+1)->delta, this->a, (this+1)->info.nNeurons, this->info.nNeurons * this->info.nDelayedSyn, this->Ns, this->Ts);
			break;
		case Convolution:
			#if defined CUSTOM_PROPAGATION
				multAdeltaConv(handle, this->gradW, (this+1)->delta, this->a, this->info, (this+1)->info, this->Ns, this->Ts);
			#else
				(this+1)->cudnn.bwdConvolutionFilter(this->a, (this+1)->delta, this->gradW, this->Ts);
			#endif
			break;
		case Aggregation:
			// no weight update
			break;
	}
	
	cudaDeviceSynchronize();
	float sumW = sumReduce(this->gradW, this->WSize);
	if(isnan(sumW) == 1)	memsetKernel<<< ceil(1.0f * this->WSize / 128), 128 >>>(this->gradW, 0.0f, this->WSize);
	
	if(profile)
	{
		if(isnan(sumW) == 1)	nanCount[2]++;
		bpruntime.push_back(profiler.record());
	}
	
	// this->gradD = - INT( this->e * this->aDot )
	cudaSetDevice(deviceID);
	switch((this+1)->info.type)
	{
		case Linear: 
		case Planar: 
		case Output:
		case Convolution:
			multAdotDelta(handle, this->gradD, this->e, this->aDot, this->info.nNeurons, this->info.nDelayedSyn, this->Ns, this->Ts);
			break;
		case Aggregation:
			// no delay update??
			break;
	}
	
	if(profile)
	{
		cudaDeviceSynchronize();
		bpruntime.push_back(profiler.record());
		memcpyTime += profiler.record();
		bpruntime.push_back(memcpyTime);
	}
	
	// slayerio::cout << "Backpropagation Complete\n\n";
	handleCudaErrorCheck("slayer.h/backPropSlayerZero");
}

void slayer::backPropSlayerRefractory()
{
	bpruntime.clear();
	float memcpyTime = 0;
	timeit profiler;
	
	// given error at the preceding layer l+1,
	// calculate layer[l+1].rho, layer[l+1].delta
	cudaSetDevice(deviceID);
	(this+1)->evalRho();
	
	if(profile)
	{
		cudaDeviceSynchronize();
		bpruntime.push_back(profiler.record());
	}
	
	// (this+1)->delta = CORR( (this+1)->e DELAYED BY (this+1)->d, (this+1)->eps )
	// for(int k=1; k<info.nDelayedSyn; ++k)	(this-1)->delayShift(-k);	// shift the a signals for extra delayed synaptic conenction
	
	cudaMemAdvise((this+1)->delta, sizeof(float) * (this+1)->deltaSize, cudaMemAdviseSetPreferredLocation, deviceID);
	cudaMemAdvise((this+1)->e,     sizeof(float) * (this+1)->eSize    , cudaMemAdviseSetPreferredLocation, deviceID);
	if((this+1)->info.type != Output)	// output layer has no delay
		cudaMemAdvise((this+1)->d, sizeof(float) * (this+1)->dSize    , cudaMemAdviseSetReadMostly,        deviceID);	// device argument is ignored
	
	cudaSetDevice(deviceID);
	if((this+1)->info.type != Output)
	{
		delayedCorr((this+1)->delta,
					(this+1)->e, (this+1)->eps, (this+1)->d,
					(this+1)->Ns,
					(this+1)->epsSize,
					(this+1)->info.nNeurons,
					(this+1)->info.nDelayedSyn,
					(this+1)->Ts );
	}
	else
	{
		corr((this+1)->delta,
			 (this+1)->e, (this+1)->eps,
			 (this+1)->Ns,
			 (this+1)->epsSize,
			 (this+1)->info.nNeurons,
			 (this+1)->info.nDelayedSyn,
			 (this+1)->Ts );
	}
	
	cudaDeviceSynchronize();
	
	if(profile)	bpruntime.push_back(profiler.record());
	
	// (this+1)->delta *= (this+1)->rho;
	dim3 thread, block;
	thread.x = 128;
	thread.y = 8;
	block.x = ceil(1.0f * (this+1)->Ns           /thread.x);
	block.y = ceil(1.0f * (this+1)->info.nNeurons/thread.y);
	if(block.y >= 65535)	slayerio::error("Error: maximum blockDim.y exceeded at slayer.cpp/backProp.");
	if(block.z >= 65535)	slayerio::error("Error: maximum blockDim.z exceeded at slayer.cpp/backProp.");
	
	cudaSetDevice(deviceID);
	// ignoring the refractory dynamics
	// multKernel<<< block, thread >>>((this+1)->delta, (this+1)->rho, (this+1)->Ns, (this+1)->info.nNeurons);
	// including the refractory dynamics
	evalDeltaRefractory((this+1)->delta, (this+1)->rho, (this+1)->nu, (this+1)->info.nNeurons, (this+1)->nuSize, (this+1)->Ns, (this+1)->Ts);
	
	cudaDeviceSynchronize();
	
	if(profile)	bpruntime.push_back(profiler.record());
	
	// this->e = TRANSPOSE( this->W ) * (this+1)->delta
	// slayerio::cout << "Calculating error\n";
	cudaMemAdvise(this->e, sizeof(float) * this->eSize, cudaMemAdviseSetPreferredLocation, deviceID);
	cudaMemAdvise(this->W, sizeof(float) * this->WSize, cudaMemAdviseSetReadMostly,        deviceID);
	
	cudaSetDevice(deviceID);
	switch((this+1)->info.type)
	{
		case Linear: 
		case Planar: 
		case Output:
			multWdelta(handle, this->e, this->W, (this+1)->delta, this->info.nNeurons * this->info.nDelayedSyn, (this+1)->info.nNeurons, this->Ns);
			// multWdelta(h, this->e, this->W, (this+1)->delta, this->info.nNeurons * this->info.nDelayedSyn, (this+1)->info.nNeurons, this->Ns);
			break;
		case Convolution:
			#if defined CUSTOM_PROPAGATION
				// multWdeltaConv(handle[0], this->e, this->W, (this+1)->delta, this->info, (this+1)->info, Ns);
				multWdeltaConv(this->e, this->W, (this+1)->delta, this->info, (this+1)->info, Ns);
			#else
				(this+1)->cudnn.bwdConvolutionData(this->e, (this+1)->delta, this->W);
			#endif
			break;
		case Aggregation:
			multWdeltaAggr(this->e, this->W, (this+1)->delta, this->info, (this+1)->info, Ns);
			break;
	}
	
	
	
	if(profile)
	{
		cudaDeviceSynchronize();
		float sumE = sumReduce(this->e, this->eSize);
		if(isnan(sumE) == 1)	nanCount[1]++;
		bpruntime.push_back(profiler.record());
	}
	
	// this->gradW = INT( (this+1)->delta * this->a )
	// slayerio::cout << "Calculating gradient\n";
	cudaSetDevice(deviceID);
	switch((this+1)->info.type)
	{
		case Linear: 
		case Planar: 
		case Output:
			multAdelta(handle, this->gradW, (this+1)->delta, this->a, (this+1)->info.nNeurons, this->info.nNeurons * this->info.nDelayedSyn, this->Ns, this->Ts);
			break;
		case Convolution:
			#if defined CUSTOM_PROPAGATION
				multAdeltaConv(handle, this->gradW, (this+1)->delta, this->a, this->info, (this+1)->info, this->Ns, this->Ts);
			#else
				(this+1)->cudnn.bwdConvolutionFilter(this->a, (this+1)->delta, this->gradW, this->Ts);
			#endif
			break;
		case Aggregation:
			// no weight update
			break;
	}
	
	cudaDeviceSynchronize();
	float sumW = sumReduce(this->gradW, this->WSize);
	if(isnan(sumW) == 1)	memsetKernel<<< ceil(1.0f * this->WSize / 128), 128 >>>(this->gradW, 0.0f, this->WSize);
	
	if(profile)
	{
		if(isnan(sumW) == 1)	nanCount[2]++;
		bpruntime.push_back(profiler.record());
	}
	
	// this->gradD = - INT( this->e * this->aDot )
	cudaSetDevice(deviceID);
	switch((this+1)->info.type)
	{
		case Linear: 
		case Planar: 
		case Output:
		case Convolution:
			multAdotDelta(handle, this->gradD, this->e, this->aDot, this->info.nNeurons, this->info.nDelayedSyn, this->Ns, this->Ts);
			break;
		case Aggregation:
			// no delay update??
			break;
	}
	
	if(profile)
	{
		cudaDeviceSynchronize();
		bpruntime.push_back(profiler.record());
		memcpyTime += profiler.record();
		bpruntime.push_back(memcpyTime);
	}
	
	// slayerio::cout << "Backpropagation Complete\n\n";
	handleCudaErrorCheck("slayer.h/backPropSlayer");
}

void slayer::backProp()
{
	switch(gradAlgo)
	{
		case SLAYER:				backPropSlayer(); 				break;
		case SLAYER_ZERO:       	backPropSlayerZero();			break;
		case SLAYER_REFRACTORY: 	backPropSlayerRefractory();		break;
	}
	cudaDeviceSynchronize();
	
	cudaMemAdvise(this->d, sizeof(float) * this->dSize, cudaMemAdviseUnsetReadMostly, deviceID);	// device argument is ignored
	cudaMemAdvise(this->W, sizeof(float) * this->WSize, cudaMemAdviseUnsetReadMostly, deviceID);	// device argument is ignored
	
	handleCudaErrorCheck("slayer.h/backProp");
}

void slayer::gdUpdate(float etaW, float lambda)
{
	cudaMemAdvise(this->W, sizeof(float) * this->WSize, cudaMemAdviseSetPreferredLocation, deviceID);
	cudaMemAdvise(this->d, sizeof(float) * this->dSize, cudaMemAdviseSetPreferredLocation, deviceID);
	unsigned thread, block;
	cudaSetDevice(deviceID);
	
	thread = 128;
	block  = ceil( 1.0f * this->WSize / thread );
	gradientDescentKernel<<< block, thread >>>(W, gradW, etaW, lambda, this->WSize);
	
	checkCudaErrors( cudaDeviceSynchronize() );
}

void slayer::gdUpdate(float etaW, float etaD, float lambda)
{
	cudaMemAdvise(this->W, sizeof(float) * this->WSize, cudaMemAdviseSetPreferredLocation, deviceID);
	cudaMemAdvise(this->d, sizeof(float) * this->dSize, cudaMemAdviseSetPreferredLocation, deviceID);
	unsigned thread, block;
	cudaSetDevice(deviceID);
	// timeit profiler;
	
	thread = 128;
	block  = ceil( 1.0f * this->WSize / thread );
	gradientDescentKernel<<< block, thread >>>(W, gradW, etaW, lambda, this->WSize);
	// checkCudaErrors( cudaDeviceSynchronize() );
	// slayerio::cout << profiler.record() << "ms elapsed\t";
	
	block  = ceil( 1.0f * this->dSize / thread ); 
	gradientDescentKernel<<< block, thread >>>(d, gradD, etaD, lambda, this->dSize);
	// slayerio::cout << profiler.record() << "ms elapsed" << std::endl;
	// checkCudaErrors( cudaDeviceSynchronize() );
}

void slayer::rmsPropUpdate(float etaW, float lambda)
{
	cudaMemAdvise(this->W, sizeof(float) * this->WSize, cudaMemAdviseSetPreferredLocation, deviceID);
	cudaMemAdvise(this->d, sizeof(float) * this->dSize, cudaMemAdviseSetPreferredLocation, deviceID);
	unsigned thread, block;
	cudaSetDevice(deviceID);
	
	thread = 128;
	block  = ceil( 1.0f * this->WSize / thread );
	rmsPropKernel<<< block, thread >>>(W, gradW, meanSquareW, etaW, lambda, this->WSize);
	
	// checkCudaErrors( cudaDeviceSynchronize() );
}

void slayer::rmsPropUpdate(float etaW, float etaD, float lambda)
{
	cudaMemAdvise(this->W, sizeof(float) * this->WSize, cudaMemAdviseSetPreferredLocation, deviceID);
	cudaMemAdvise(this->d, sizeof(float) * this->dSize, cudaMemAdviseSetPreferredLocation, deviceID);
	unsigned thread, block;
	cudaSetDevice(deviceID);
	
	thread = 128;
	block  = ceil( 1.0f * this->WSize / thread );
	rmsPropKernel<<< block, thread >>>(W, gradW, meanSquareW, etaW, lambda, this->WSize);
	
	block  = ceil( 1.0f * this->dSize / thread ); 
	rmsPropKernel<<< block, thread >>>(d, gradD, meanSquareD, etaD, lambda, this->dSize);
	
	// checkCudaErrors( cudaDeviceSynchronize() );
}

void  slayer::adamUpdate(float etaW, float lambda, float beta1, float beta2, unsigned iter, float epsilon)
{
	cudaMemAdvise(this->W, sizeof(float) * this->WSize, cudaMemAdviseSetPreferredLocation, deviceID);
	cudaMemAdvise(this->d, sizeof(float) * this->dSize, cudaMemAdviseSetPreferredLocation, deviceID);
	unsigned thread, block;
	cudaSetDevice(deviceID);
	
	thread = 128;
	block  = ceil( 1.0f * this->WSize / thread );
	adamKernel<<< block, thread >>>(W, gradW, meanSquareW, momentumW, etaW, lambda, beta1, beta2, epsilon, iter, this->WSize);
	
	// checkCudaErrors( cudaDeviceSynchronize() );
}

void  slayer::adamUpdate(float etaW, float etaD, float lambda, float beta1, float beta2, unsigned iter, float epsilon)
{
	cudaMemAdvise(this->W, sizeof(float) * this->WSize, cudaMemAdviseSetPreferredLocation, deviceID);
	cudaMemAdvise(this->d, sizeof(float) * this->dSize, cudaMemAdviseSetPreferredLocation, deviceID);
	unsigned thread, block;
	cudaSetDevice(deviceID);
	
	thread = 128;
	block  = ceil( 1.0f * this->WSize / thread );
	adamKernel<<< block, thread >>>(W, gradW, meanSquareW, momentumW, etaW, lambda, beta1, beta2, epsilon, iter, this->WSize);
	
	block  = ceil( 1.0f * this->dSize / thread ); 
	adamKernel<<< block, thread >>>(d, gradD, meanSquareD, momentumD, etaD, lambda, beta1, beta2, epsilon, iter, this->dSize);
	
	// checkCudaErrors( cudaDeviceSynchronize() );
}

void  slayer::nadamUpdate(float etaW, float lambda, float beta1, float beta2, unsigned iter, float epsilon)
{
	cudaMemAdvise(this->W, sizeof(float) * this->WSize, cudaMemAdviseSetPreferredLocation, deviceID);
	cudaMemAdvise(this->d, sizeof(float) * this->dSize, cudaMemAdviseSetPreferredLocation, deviceID);
	unsigned thread, block;
	cudaSetDevice(deviceID);
	
	thread = 128;
	block  = ceil( 1.0f * this->WSize / thread );
	nadamKernel<<< block, thread >>>(W, gradW, meanSquareW, momentumW, etaW, lambda, beta1, beta2, epsilon, iter, this->WSize);
	
	// checkCudaErrors( cudaDeviceSynchronize() );
}

void  slayer::nadamUpdate(float etaW, float etaD, float lambda, float beta1, float beta2, unsigned iter, float epsilon)
{
	cudaMemAdvise(this->W, sizeof(float) * this->WSize, cudaMemAdviseSetPreferredLocation, deviceID);
	cudaMemAdvise(this->d, sizeof(float) * this->dSize, cudaMemAdviseSetPreferredLocation, deviceID);
	unsigned thread, block;
	cudaSetDevice(deviceID); 
	// timeit profiler;
	
	thread = 128;
	block  = ceil( 1.0f * this->WSize / thread );
	nadamKernel<<< block, thread >>>(W, gradW, meanSquareW, momentumW, etaW, lambda, beta1, beta2, epsilon, iter, this->WSize);
	
	// cudaDeviceSynchronize();
	// slayerio::cout << profiler.record() << "ms elapsed\t";
	
	block  = ceil( 1.0f * this->dSize / thread ); 
	nadamKernel<<< block, thread >>>(d, gradD, meanSquareD, momentumD, etaD, lambda, beta1, beta2, epsilon, iter, this->dSize);
	
	// checkCudaErrors( cudaDeviceSynchronize() );
	// slayerio::cout << profiler.record() << "ms elapsed" << std::endl;
}

void  slayer::weightLimit()
{
	cudaSetDevice(deviceID);
	checkCudaErrors( cudaDeviceSynchronize() );
	unsigned thread = 128;
	unsigned block  = ceil( 1.0f * this->WSize / thread );
	float avgWeight = sumReduce(W, this->WSize) / this->WSize;
	if(avgWeight < 0)	addKernel<<< block, thread >>>(W, -avgWeight, this->WSize);
}

void  slayer::delayLimit()
{
	checkCudaErrors( cudaDeviceSynchronize() );
	cudaSetDevice(deviceID);
	unsigned thread = 128;
	unsigned block  = ceil( 1.0f * this->dSize / thread );
	delayLimitKernel<<< block, thread >>>(d, this->dSize);
}

void  slayer::updateWvec(std::vector<float> &weight)
{
	unsigned numWeights = info.nWeights * info.nDelayedSyn;
	// mexPrintf("LayerID: %x\n", this);
	// mexPrintf("numWeights : %d\n", numWeights);
	
	// unsigned imax = (numWeights > 10) ? 10 : numWeights;
	// for(unsigned i=0; i<imax; ++i)	W[i] = 0;
	
	for(unsigned i=0; i<numWeights; ++i)	weight[wInd[i]] = W[i];
		
	// numWeights = (numWeights > 100) ? 100 : numWeights;
	// for(unsigned i=0; i<numWeights; ++i)	mexPrintf("%d  ", wInd[i]);
	// mexPrintf("\n");
}

void  slayer::updateDvec(std::vector<float> &delay)
{
	unsigned numDelays = info.nDelays  * info.nDelayedSyn;
	// mexPrintf("LayerID: %x\n", this);
	// mexPrintf("numDelays : %d\n", numDelays);
	
	// unsigned imax = (numDelays > 10) ? 10 : numDelays;
	// for(unsigned i=0; i<imax; ++i)	d[i] = 0;
	// for(unsigned i=0; i<imax; ++i)	d[i] = 0;
	
	for(unsigned i=0; i<numDelays; ++i)	delay[dInd[i]] = d[i];
		
	// numDelays = (numDelays > 100) ? 100 : numDelays;
	// for(unsigned i=0; i<numDelays; ++i)	mexPrintf("%d  ", dInd[i]);
	// mexPrintf("\n");
}

#if defined mex_h
	void slayer::copyTo(mxArray *mxPtr, layerVar var)
	{
		/*
		 * copies the variable data into a matlab matrix.
		 * the order is converted automatically to column major for compatibility with MATLAB.
		 */
		unsigned numRows;
		switch(var)
		{
			case SPIKE: case DELTA: case RHO: case U: 		numRows = info.nNeurons;					break;
			case E: case A: case ADOT:						numRows = info.nNeurons * info.nDelayedSyn;	break; 
			default:										numRows = 0;
		}
		
		switch(var)
		{
			case SPIKE: cudaMemPrefetchAsync(    s, sizeof(float) * numRows * Ns, cudaCpuDeviceId);	break;
			case E: 	cudaMemPrefetchAsync(    e, sizeof(float) * numRows * Ns, cudaCpuDeviceId);	break;
			case DELTA: cudaMemPrefetchAsync(delta, sizeof(float) * numRows * Ns, cudaCpuDeviceId);	break;
			case RHO: 	cudaMemPrefetchAsync(  rho, sizeof(float) * numRows * Ns, cudaCpuDeviceId);	break;
			case U: 	cudaMemPrefetchAsync(    u, sizeof(float) * numRows * Ns, cudaCpuDeviceId);
						cudaMemPrefetchAsync(    s, sizeof(float) * numRows * Ns, cudaCpuDeviceId);	break;
			case A: 	cudaMemPrefetchAsync(    a, sizeof(float) * numRows * Ns, cudaCpuDeviceId);	break;
			case ADOT: 	cudaMemPrefetchAsync( aDot, sizeof(float) * numRows * Ns, cudaCpuDeviceId);	break;
		}
		
		mxSetM(mxPtr, numRows);
		// mxSetN(mxPtr, Ns-1);
		mxSetN(mxPtr, Ns);
		mxSetData(mxPtr, mxMalloc(sizeof(double) * numRows * Ns));
		double *ptr  = mxGetPr(mxPtr);					// get the pointer to output variable
		for(unsigned i=0; i < numRows; ++i)
			for(unsigned j=0; j<Ns; ++j)
				switch(var)
				{
					case SPIKE: ptr[i + j*numRows] =     s[i*Ns + j];	break;
					case E: 	ptr[i + j*numRows] =     e[i*Ns + j];	break;
					case DELTA: ptr[i + j*numRows] = delta[i*Ns + j];	break;
					case RHO: 	ptr[i + j*numRows] =   rho[i*Ns + j];	break;
					case U: 	ptr[i + j*numRows] =     u[i*Ns + j] + s[i*Ns + j] *5*theta*Ts;	break;
					case A: 	ptr[i + j*numRows] =     a[i*Ns + j];	break;
					case ADOT: 	ptr[i + j*numRows] =  aDot[i*Ns + j];	break;
				}
	}
#endif

std::vector<float> slayer::get(layerVar var)
{
	/*
	 * returns a vector containing the variable data.
	 * the order is converted automatically to "column major"
	 * this is done to allow concatenation of signals in different minibatch.
	 * the new data can be simply appended to the previous data.
	 */
	std::vector<float> data;
	unsigned numRows;
	switch(var)
	{
		case SPIKE: case DELTA: case RHO: case U: 		numRows = info.nNeurons;					break;
		case E: case A: case ADOT:						numRows = info.nNeurons * info.nDelayedSyn;	break; 
		default:										numRows = 0;
	}
	
	switch(var)
	{
		case SPIKE: cudaMemPrefetchAsync(    s, sizeof(float) * numRows * Ns, cudaCpuDeviceId);	break;
		case E: 	cudaMemPrefetchAsync(    e, sizeof(float) * numRows * Ns, cudaCpuDeviceId);	break;
		case DELTA: cudaMemPrefetchAsync(delta, sizeof(float) * numRows * Ns, cudaCpuDeviceId);	break;
		case RHO: 	cudaMemPrefetchAsync(  rho, sizeof(float) * numRows * Ns, cudaCpuDeviceId);	break;
		case U: 	cudaMemPrefetchAsync(    u, sizeof(float) * numRows * Ns, cudaCpuDeviceId);
					cudaMemPrefetchAsync(    s, sizeof(float) * numRows * Ns, cudaCpuDeviceId);	break;
		case A: 	cudaMemPrefetchAsync(    a, sizeof(float) * numRows * Ns, cudaCpuDeviceId);	break;
		case ADOT: 	cudaMemPrefetchAsync( aDot, sizeof(float) * numRows * Ns, cudaCpuDeviceId);	break;
	}
	
	// data.resize(numRows * (Ns-1));
	data.resize(numRows * Ns);
	
	
	for(unsigned i=0; i < numRows; ++i)
		for(unsigned j=0; j<Ns; ++j)
			switch(var)
			{
				case SPIKE: data[i + j*numRows] =     s[i*Ns + j];	break;
				case E: 	data[i + j*numRows] =     e[i*Ns + j];	break;
				case DELTA: data[i + j*numRows] = delta[i*Ns + j];	break;
				case RHO: 	data[i + j*numRows] =   rho[i*Ns + j];	break;
				case U: 	data[i + j*numRows] =     u[i*Ns + j] + s[i*Ns + j] *5*theta*Ts;	break;
				case A: 	data[i + j*numRows] =     a[i*Ns + j];	break;
				case ADOT: 	data[i + j*numRows] =  aDot[i*Ns + j];	break;
			}
			
	return data;
}

void slayer::printWeights()
{
	slayerio::cout << "layerID: " << this << std::endl;
	
	unsigned numWeights = info.nWeights * info.nDelayedSyn;
	numWeights = (numWeights <= 100) ? numWeights : 100;	// print only the first hundred values
	slayerio::cout << "Weights     : ";		for(unsigned i=0; i<numWeights; ++i)	slayerio::cout << W[i]           << "  ";			slayerio::cout << std::endl;
	slayerio::cout << "gradW       : ";		for(unsigned i=0; i<numWeights; ++i)	slayerio::cout << gradW[i]       << "  ";			slayerio::cout << std::endl;
	slayerio::cout << "meanSquareW : ";		for(unsigned i=0; i<numWeights; ++i)	slayerio::cout << meanSquareW[i] << "  ";			slayerio::cout << std::endl;
	slayerio::cout << "momentumW   : ";		for(unsigned i=0; i<numWeights; ++i)	slayerio::cout << momentumW[i] << "  ";				slayerio::cout << std::endl;
	
	unsigned numDelays = info.nDelays * info.nDelayedSyn;
	numDelays = (numDelays <= 100) ? numDelays : 100;	// print only the first hundred values
	slayerio::cout << "Delays      : ";		for(unsigned i=0; i<numDelays; ++i)	slayerio::cout << d[i]           << "  ";			slayerio::cout << std::endl;
	slayerio::cout << "gradD       : ";		for(unsigned i=0; i<numDelays; ++i)	slayerio::cout << gradD[i]       << "  ";			slayerio::cout << std::endl;
	slayerio::cout << "meanSquareD : ";		for(unsigned i=0; i<numDelays; ++i)	slayerio::cout << meanSquareD[i] << "  ";			slayerio::cout << std::endl;
	slayerio::cout << "momentumD   : ";		for(unsigned i=0; i<numDelays; ++i)	slayerio::cout << momentumD[i]   << "  ";			slayerio::cout << std::endl;
}

void slayer::setDevices(unsigned device)
{	
	deviceID = device;	
	
	cudaSetDevice(deviceID);
	cublasCreate(&handle);
	cublasHandleCreated = true;

	unsigned deviceCC = getComputeCapability(deviceID);
	if(deviceCC >= 70)	checkCublasErrors( cublasSetMathMode(handle, CUBLAS_TENSOR_OP_MATH) );
	
	if(this->info.type == Convolution)
	{
		// mexPrintf("layer %d is Convolution layer\n", i);
		dim4 iDim, oDim, wDim;
		iDim.x = (this-1)->info.dimension[0];	iDim.y = (this-1)->info.dimension[1];	iDim.z = (this-1)->info.dimension[2];	iDim.w = Ns;
		oDim.x =     this->info.dimension[0];	oDim.y =     this->info.dimension[1];	oDim.z =     this->info.dimension[2];	oDim.w = Ns;
		wDim.x =     this->info.parameter[1];	wDim.y =     this->info.parameter[1];	wDim.z = iDim.z;						wDim.w = oDim.z;
		// mexPrintf("iDim = (%d, %d, %d, %d)\n", iDim.x, iDim.y, iDim.z, iDim.w);
		// mexPrintf("oDim = (%d, %d, %d, %d)\n", oDim.x, oDim.y, oDim.z, oDim.w);
		// mexPrintf("wDim = (%d, %d, %d, %d)\n", wDim.x, wDim.y, wDim.z, wDim.w);
		// mexPrintf("Device Compute Capability = %d\n", getComputeCapability(deviceID));
		this->cudnn.setDescriptors(iDim, oDim, wDim, deviceCC);		// delfault computePrecision  = CUDNN_DATA_FLOAT is used
		// this->layer[i].cudnn.testCudnnDescriptor();
	}
	
	// if(deviceCC >= 70)	mexPrintf("TENSOR CORE utilization activated\n\n");
}
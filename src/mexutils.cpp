#if defined SLAYERMEX

#include <mexutils.h>

std::string mex::getline()
{
	mexErrMsgTxt("mexutils.h getline() not working at the moment");
	char nullStr[] = {'\0'};
	// mxArray *mxStrOut = mxCreateString("testing ... ");
	// mwSize dim[] = {1, 256};
	mxArray *mxStrOut = mxCreateDoubleMatrix(1, 256, mxREAL);
	mxArray *mxPrompt = mxCreateString(nullStr);
	mxArray *mxSpec   = mxCreateString("s");
	mxArray *lhs[] = {mxStrOut};
	mxArray *rhs[] = {mxPrompt, mxSpec};
	mexCallMATLAB(1, lhs, 2, rhs, "input");
	mexCallMATLAB(0, NULL, 1, lhs, "disp");
	// mexCallMATLAB(0, NULL, 1, lhs, "whos");
	// std::string lineread = std::string(mxArrayToString(mxStrOut));
	
	char strbuf[256];
	int retval = mxGetString(mxStrOut, strbuf, 256);
	mexPrintf("%d returned\n", retval);
	mexPrintf("%s\n", (char*)mxGetPr(mxStrOut));
	std::string lineread(strbuf);
	
	// mexPrintf("data read is %s, length is %d", lineread.c_str(), lineread.size());
	
	mxDestroyArray(mxStrOut);
	mxDestroyArray(mxPrompt);
	mxDestroyArray(mxSpec);
	
	return lineread;
}

char* mex::getline(char* strbuf)
{
	std::string data = getline();
	const char* strsrc = data.c_str();
	char* strdst = strbuf;
	unsigned i=0;
	while( ( *strdst++ = *(strdst + i++) ) != '\0' );
	if(data.size() == 0)	return nullptr;
	else 					return strbuf;
}

double mex::getScalar(const mxArray* mxField, const char* name)
{
	mxArray *networkParams;
	static char strbuf[MAXBUF];
	networkParams = mxGetField(mxField, 0, name);
	if(networkParams == NULL)
	{
		sprintf(strbuf, "%s not found", name);
		mexErrMsgTxt(strbuf);
	}
	return mxGetScalar(networkParams);
}

double mex::getScalar(const mxArray* mxField, const char* name, double defaultVal)
{
	mxArray *networkParams;
	// static char strbuf[MAXBUF];
	networkParams = mxGetField(mxField, 0, name);
	if(networkParams == NULL)	return defaultVal;
	return mxGetScalar(networkParams);
}

std::vector<float> mex::getFVector(const mxArray* mxField, const char* name)
{
	mxArray *networkParams;
	static char strbuf[MAXBUF];
	networkParams = mxGetField(mxField, 0, name);
	if(networkParams == NULL)
	{
		sprintf(strbuf, "Error: %s not found", name);
		mexErrMsgTxt(strbuf);
	}
	std::vector<float> data(mxGetM(networkParams)*mxGetN(networkParams));
	double *fptr = mxGetPr(networkParams);
	for(int i=0; i<data.size(); ++i)	data[i] = static_cast<float>(fptr[i]);
	
	return data;
}

std::vector<float> mex::getFVector(const mxArray* mxField, const char* name, std::vector<float> defaultVal)
{
	mxArray *networkParams;
	static char strbuf[MAXBUF];
	networkParams = mxGetField(mxField, 0, name);
	if(networkParams == NULL)	return defaultVal;
	std::vector<float> data(mxGetM(networkParams)*mxGetN(networkParams));
	double *fptr = mxGetPr(networkParams);
	for(int i=0; i<data.size(); ++i)	data[i] = static_cast<float>(fptr[i]);
	
	return data;
}

std::vector<unsigned> mex::getUVector(const mxArray* mxField, const char* name)
{
	std::vector<float>    tempF = getFVector(mxField, name);
	std::vector<unsigned> tempU(tempF.begin(), tempF.end());
	return tempU;
}

std::string mex::getString(const mxArray* mxField, const char* name)
{
	mxArray *networkParams;
	static char strbuf[MAXBUF];
	networkParams = mxGetField(mxField, 0, name);
	if(networkParams == NULL)
	{
		sprintf(strbuf, "%s not found", name);
		mexErrMsgTxt(strbuf);
	}
	std::string path(mxArrayToString(networkParams));
	return path;
}

std::string mex::getString(const mxArray* mxField, const char* name, const char* defaultString)
{
	mxArray *networkParams;
	static char strbuf[MAXBUF];
	networkParams = mxGetField(mxField, 0, name);
	std::string str;
	if(networkParams == NULL)	str = std::string(defaultString);
	else 						str = std::string(mxArrayToString(networkParams));
	return str;
}

void silentFigure(unsigned figureID)
{
	std::stringstream sout;
	
	/*
	% this code makes figureID the current figure to plot
	% However, it does not force it to be the active window
    if ishandle(figureID)
        set(0, 'CurrentFigure', figureID);
    else
        figure(figureID);
    end
    */
    
	sout << "if ishandle(" << figureID << ")\n";
    sout << "\tset(0, 'CurrentFigure', " << figureID << ");\n";
    sout << "else\n";
    sout << "\tfigure(" << figureID << ");";
    sout << "end\n";

	mexEvalString(sout.str().c_str());	// call figure(figureID) in MATLAB space
}

void rasterPlot(float* s, unsigned Ts, unsigned figID, unsigned Ns, unsigned numel)
{
	std::vector<double> nVec;
	std::vector<double> tVec;
	for(unsigned i=0; i<numel; ++i)
	{
		if(s[i] > 0)
		{
			unsigned tID = i % Ns;
			unsigned nID = i / Ns;
			tVec.push_back(tID * Ts);
			nVec.push_back(nID);
		}
	}
	
	unsigned N = nVec.size();
	mxArray *mxnID = mxCreateDoubleMatrix(N, 1, mxREAL);
	mxArray *mxtID = mxCreateDoubleMatrix(N, 1, mxREAL);
	double *ptnID = mxGetPr(mxnID);
	double *pttID = mxGetPr(mxtID);
	
	for(unsigned i=0; i<N; ++i)
	{
		ptnID[i] = nVec[i];
		pttID[i] = tVec[i];
	}
	
	silentFigure(figID);
	
	mexEvalString("hold off;");
	mxArray *mxPlotSpecs = mxCreateString(".");
	mxArray *lhs[] = {mxtID, mxnID, mxPlotSpecs};
	mexCallMATLAB(0, NULL, 3, lhs, "plot");
	mexEvalString("xlabel('time')");
	mexEvalString("ylabel('neuron ID')");
	mexEvalString("drawnow");
	
	mxDestroyArray(mxnID);
	mxDestroyArray(mxtID);
	mxDestroyArray(mxPlotSpecs);
}

void mxCopy(mxArray* mxPtr, std::vector<float> data)
{
	mxSetM(mxPtr, static_cast<unsigned>(data.size()));
	mxSetN(mxPtr, 1);
	mxSetData(mxPtr, mxMalloc(sizeof(double) * static_cast<unsigned>(data.size())));
	double *ptr  = mxGetPr(mxPtr);					// get the pointer to output variable
	for(unsigned i=0; i< data.size(); ++i)
		ptr[i] = data[i];
}

void mxCopy(mxArray* mxPtr, std::vector<std::vector<float>> data)
{
	unsigned M = data[0].size();
	unsigned N = data.size();
	mxSetM(mxPtr, static_cast<unsigned>(M));
	mxSetN(mxPtr, static_cast<unsigned>(N));
	mxSetData(mxPtr, mxMalloc(sizeof(double) * M*N));
	double *ptr  = mxGetPr(mxPtr);					// get the pointer to output variable
	unsigned i=0;
	for(unsigned n=0; n<N; ++n)
		for(unsigned m=0; m<M; ++m)
			ptr[i++] = data[n][m];
}

void mxCopy2(mxArray* mxPtr, std::vector<float> data1, std::vector<float> data2)
{
	mxSetM(mxPtr, static_cast<unsigned>(data1.size()));
	mxSetN(mxPtr, 2);
	mxSetData(mxPtr, mxMalloc(sizeof(double) * static_cast<unsigned>(data1.size()) *2));
	double *ptr  = mxGetPr(mxPtr);					// get the pointer to output variable
	for(unsigned i=0; i< data1.size(); ++i)
	{
		ptr[i] 				= data1[i];
		ptr[i+data1.size()] = data2[i];
	}
}

void plotCost(double J, unsigned epoch, unsigned figureID, std::string plotTitle, std::string plotSpecs)
{
	// MATLAB: figure(figureID)
	std::stringstream sout;
	
	silentFigure(figureID);
	semilogy(epoch, J, plotSpecs);
	
	// MATLAB: title(plotTitle);
	// 		   xlabel('Epoch');
	//         ylabel('Accuracy');
	sout << "title('" << plotTitle << "');";
	mexEvalString(sout.str().c_str());
	mexEvalString("xlabel('Epoch')");
	mexEvalString("ylabel('J')");
	mexEvalString("drawnow");
	mexEvalString("hold on;");
}

void plotAcc(double Acc, unsigned epoch, unsigned figureID, std::string plotTitle, std::string plotSpecs)
{
	// MATLAB: figure(figureID)
	std::stringstream sout;
	
	silentFigure(figureID);
	plot(epoch, Acc, plotSpecs);
	
	// MATLAB: title(plotTitle);
	// 		   xlabel('Epoch');
	//         ylabel('Accuracy');
	sout << "title('" << plotTitle << "');";
	mexEvalString(sout.str().c_str());
	mexEvalString("xlabel('Epoch')");
	mexEvalString("ylabel('Accuracy')");
	mexEvalString("drawnow");
	mexEvalString("hold on;");
}


#endif
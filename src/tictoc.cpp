#include <tictoc.h>
#include <time.h>

#if defined _WIN32 || defined _WIN64
    double PCFreq = 0.0;
    __int64 CounterStart = 0;
	
	double GetTickCount()
	{
		LARGE_INTEGER li;
        if(!QueryPerformanceFrequency(&li))     return -1; // -1 meaning failure in reading query performance

        PCFreq = double(li.QuadPart)/1000.0;

        QueryPerformanceCounter(&li);
		
		return double(li.QuadPart) / PCFreq;
	}

    int tic()
    {
        LARGE_INTEGER li;
        if(!QueryPerformanceFrequency(&li))     return -1; // -1 meaning failure in reading query performance

        PCFreq = double(li.QuadPart)/1000.0;

        QueryPerformanceCounter(&li);
        CounterStart = li.QuadPart;
        return 0;
    }

    double toc()
    {
        LARGE_INTEGER li;
        QueryPerformanceCounter(&li);
        return double(li.QuadPart-CounterStart)/PCFreq;
    }
#elif defined __linux__
    double CounterStart;

    double GetTickCount()
    {
        struct timespec now;
        if(clock_gettime(CLOCK_MONOTONIC, &now))	return -1; // -1 meaning failure in reading query performance
        return (now.tv_sec*1000.0 + now.tv_nsec/1000000.0);
    }

    int tic()
    {
        CounterStart = GetTickCount();
        return (static_cast<int>(CounterStart) >= 0)?0:-1;
    }

    double toc()
    {
        return double(GetTickCount() - CounterStart);
    }
#else
    #error "unknown platform"
#endif

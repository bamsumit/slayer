#include <cudaKernels.h>
#include <slayerio.h>

// CUDA error check 
void checkCudaErrors_(cudaError_t err, const char *file, int line)
{
	if(err != cudaSuccess)
	{
		slayerio::cout << "Error in " << file << ", Line " << line << std::endl;
		slayerio::error("%s\n", cudaGetErrorString(err));
	}
}

// CUBLAS error check 
void checkCublasErrors_(cublasStatus_t status, const char *file, int line)
{
	if(status != CUBLAS_STATUS_SUCCESS)
	{
		slayerio::cout << "Error in " << file << ", Line " << line << std::endl;
		switch(status)
		{
			case CUBLAS_STATUS_NOT_INITIALIZED:	 	slayerio::error("CUBLAS_STATUS_NOT_INITIALIZED \n");		break;
			case CUBLAS_STATUS_ALLOC_FAILED: 		slayerio::error("CUBLAS_STATUS_ALLOC_FAILED    \n");		break;
			case CUBLAS_STATUS_INVALID_VALUE: 		slayerio::error("CUBLAS_STATUS_INVALID_VALUE   \n");		break;
			case CUBLAS_STATUS_ARCH_MISMATCH: 		slayerio::error("CUBLAS_STATUS_ARCH_MISMATCH   \n");		break;
			case CUBLAS_STATUS_MAPPING_ERROR: 		slayerio::error("CUBLAS_STATUS_MAPPING_ERROR   \n");		break;
			case CUBLAS_STATUS_EXECUTION_FAILED: 	slayerio::error("CUBLAS_STATUS_EXECUTION_FAILED\n");		break;
			case CUBLAS_STATUS_INTERNAL_ERROR: 		slayerio::error("CUBLAS_STATUS_INTERNAL_ERROR  \n");		break;
		}
	}
}

// CUDNN error check 
void checkCudnnErrors_(cudnnStatus_t status, const char *file, int line)
{
	if(status != CUDNN_STATUS_SUCCESS)
	{
		slayerio::cout << "Error in " << file << ", Line " << line << std::endl;
		slayerio::error("%s\n", cudnnGetErrorString(status));
	}
}

// CURAND error check
void checkCurandErrors_(curandStatus_t status, const char *file, int line)
{
	if(status != CURAND_STATUS_SUCCESS)
	{
		slayerio::error("Error in %s, Line %d\n", file, line);
	}
}

void handleCublasStatus(cublasStatus_t status, std::string location)
{
	if(status != CUBLAS_STATUS_SUCCESS)
	{
		slayerio::cout << "CUBLAS ERROR: In " << location << ", ";
		switch(status)
		{
			case CUBLAS_STATUS_NOT_INITIALIZED:	 	slayerio::error("CUBLAS_STATUS_NOT_INITIALIZED \n");		break;
			case CUBLAS_STATUS_ALLOC_FAILED: 		slayerio::error("CUBLAS_STATUS_ALLOC_FAILED    \n");		break;
			case CUBLAS_STATUS_INVALID_VALUE: 		slayerio::error("CUBLAS_STATUS_INVALID_VALUE   \n");		break;
			case CUBLAS_STATUS_ARCH_MISMATCH: 		slayerio::error("CUBLAS_STATUS_ARCH_MISMATCH   \n");		break;
			case CUBLAS_STATUS_MAPPING_ERROR: 		slayerio::error("CUBLAS_STATUS_MAPPING_ERROR   \n");		break;
			case CUBLAS_STATUS_EXECUTION_FAILED: 	slayerio::error("CUBLAS_STATUS_EXECUTION_FAILED\n");		break;
			case CUBLAS_STATUS_INTERNAL_ERROR: 		slayerio::error("CUBLAS_STATUS_INTERNAL_ERROR  \n");		break;
		}
	}
}

void handleCudaErrorCheck(std::string location)
{
	cudaError_t err = cudaGetLastError();
	if(err != cudaSuccess)
	{
		slayerio::error("CUDA ERROR: In %s, %s\n", location.c_str(), cudaGetErrorString(err));
	}
}

__global__ void sumReduceKernel(float* d_data, int N) 
{
	extern __shared__ float sdata[];
	// perform first level of reduction
	// each thread loads two elements from global memory and writes their sum to shared mem
	unsigned int tid = threadIdx.x;
	unsigned int i = blockIdx.x * blockDim.x * 2 + threadIdx.x;
	
	sdata[tid] = ( (i < N) ? d_data[i] : 0.0f ) + ( (i+blockDim.x < N) ? d_data[i+blockDim.x] : 0.0f );	// zero padding for off limit elements
	__syncthreads();
	// do reduction in shared mem
	
	for(unsigned int s=blockDim.x/2; s>0; s>>=1) 
	{
		if(tid<s) 
		{
			sdata[tid] += sdata[tid + s];
		}
		__syncthreads();
	}
	// write result for this block to global mem
	if (tid == 0) d_data[blockIdx.x] = sdata[0];
}

float sumReduce(float* d_data, int N)
{
	float* d_dataCpy;
	cudaMalloc(&d_dataCpy, sizeof(float)*N);
	cudaMemcpy( d_dataCpy, d_data, sizeof(float)*N, cudaMemcpyDeviceToDevice);
	
	unsigned thread = 128;
	unsigned block = N;
	cudaDeviceSynchronize();
	
	//unsigned count=0;
	do{
		block = ceil(0.5f * N/thread);	// just work on half of the data
		sumReduceKernel<<< block, thread, sizeof(float)*thread >>>(d_dataCpy, N);
		N=block;
		cudaDeviceSynchronize();
		// count++;
		// if(count > 500)	break;
	}while(block > 1);
	
	float sum;
	cudaMemcpy(&sum, d_dataCpy, sizeof(float), cudaMemcpyDeviceToHost);
	cudaDeviceSynchronize();
	
	cudaFree(d_dataCpy);
	
	return sum;
}

__global__ void addKernel(float* difference, float substrahend, unsigned numel)
{
	unsigned id = blockIdx.x * blockDim.x + threadIdx.x;
	
	if(id >= numel)	return;
	
	difference[id] += substrahend;
	
	return;
}

__global__ void diffKernel(float* difference, const float* substrahend, unsigned xDim, unsigned yDim)
{
	unsigned xID = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned yID = blockIdx.y * blockDim.y + threadIdx.y;
	
	if(xID >= xDim)	return;
	if(yID >= yDim)	return;
	
	unsigned id = xID + yID * xDim;
	
	difference[id] -= substrahend[id];
	
	return;
}

__global__ void multKernel(float* product, const float* arg1, unsigned xDim, unsigned yDim)
{
	unsigned xID = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned yID = blockIdx.y * blockDim.y + threadIdx.y;
	
	if(xID >= xDim)	return;
	if(yID >= yDim)	return;
	
	unsigned id = xID + yID * xDim;
	
	product[id] *= arg1[id];
	
	return;
}

__global__ void convertFp32ToFp16Kernel (half *out, float *in, int n, float scale) 
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < n) 
	{
		out[idx] = scale * in[idx];
	}
}

__global__ void convertFp16ToFp32Kernel (float *out, half *in, int n, float scale) 
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < n) 
	{
		out[idx] = scale * float(in[idx]);
		// if(isnan(float(in[idx])))	out[idx] = 0;
		// else				out[idx] = scale * float(in[idx]);
	}
}

__global__ void scaleKernel(float *data, float factor, int n)
{
	int idx = blockDim.x * blockIdx.x + threadIdx.x;
	if (idx < n) 
	{
		data[idx] *= factor;
	}
}

unsigned getComputeCapability(unsigned deviceID)
{
	cudaDeviceProp prop;
	checkCudaErrors( cudaGetDeviceProperties(&prop, deviceID) );
	// std::cout << "CC : " << prop.major * 10 + prop.minor << std::endl;
	return prop.major * 10 + prop.minor;
}

unsigned getComputeCapability(std::vector<unsigned> deviceID)
{
	unsigned minComputeCapability = 0;
	for(unsigned i=0; i<deviceID.size(); ++i)
	{
		unsigned computeCapability = getComputeCapability(deviceID[i]);
		if(i == 0)											minComputeCapability = computeCapability;
		else if(computeCapability < minComputeCapability)	minComputeCapability = computeCapability;
	}
	return minComputeCapability;
}

std::string getGPUspecs()
{
	std::stringstream sout;
	int nDevices;

	cudaGetDeviceCount(&nDevices);
	for (int i = 0; i < nDevices; i++) 
	{
		cudaDeviceProp prop;
		cudaGetDeviceProperties(&prop, i);
		int cores = 0;
		switch(prop.major)
		{
			case 2:
				if(prop.minor == 1)			cores = prop.multiProcessorCount * 48;
				else 						cores = prop.multiProcessorCount * 32;
				break;
			case 3: // Kepler
											cores = prop.multiProcessorCount * 192;
				break;
			case 5: // Maxwell
											cores = prop.multiProcessorCount * 128;
				break;
			case 6: // Pascal
				if(prop.minor == 1) 		cores = prop.multiProcessorCount * 128;
				else if(prop.minor == 0) 	cores = prop.multiProcessorCount * 64;
		}
		sout << "Device Number: " << i << std::endl;
		sout << "  Device name            : " << prop.name << std::endl;
		sout << "  Max Clock Rate         : " << prop.clockRate << " KHz" << std::endl;
		sout << "  Multi Processors       : " << prop.multiProcessorCount << std::endl;
		sout << "  Stream Processors      : " << cores << " (" << cores/prop.multiProcessorCount << " per MP)"<< std::endl;
		sout << "  Max Grid Size          : " << prop.maxGridSize[0]   << " x " << prop.maxGridSize[1]   << " x " << prop.maxGridSize[2]   << std::endl;
		sout << "  Max Threads Dimension  : " << prop.maxThreadsDim[0] << " x " << prop.maxThreadsDim[1] << " x " << prop.maxThreadsDim[2] << std::endl;
		sout << "  Max Threads per block  : " << prop.maxThreadsPerBlock << std::endl;
		sout << "  Max Threads per MP     : " << prop.maxThreadsPerMultiProcessor << std::endl;
		// sout << "  Max Blocks per MP      : " << prop.maxThreadsPerMultiProcessor / prop.maxThreadsPerBlock * prop.multiProcessorCount << std::endl;
		sout << "  Shared Memory per block: " << prop.sharedMemPerBlock << " bytes" << std::endl;
		sout << "  Total Global Memory    : " << prop.totalGlobalMem << " bytes" << std::endl;
		sout << "  Memory Clock Rate      : " << prop.memoryClockRate << " KHz" << std::endl;
		sout << "  Memory Bus Width       : " << prop.memoryBusWidth << " bit" << std::endl;
		sout << "  Peak Memory Bandwidth  : " << 2.0*prop.memoryClockRate*(prop.memoryBusWidth/8)/1.0e6 << " GB/s" << std::endl;
		sout << "  Compute Capabality     : " << prop.major << "." << prop.minor << std::endl;
		sout << "  Warp Size              : " << prop.warpSize << std::endl;
	}
	return sout.str();
}
#include <spikeKernels.h>
#include <cudaKernels.h>

__global__ void ahpKernel(float* d_u, const float* d_nu, unsigned nuSize)
{
	unsigned id = blockIdx.x * blockDim.x + threadIdx.x;
	if(id >= nuSize)	return;
	d_u[id] += d_nu[id];
}

__global__ void getSpikesKernel(float* d_s, float* d_u, const float* d_nu, unsigned nBatch, unsigned nNeurons, unsigned nuSize, unsigned batchStride, unsigned Ns, float theta, float Ts)
{
	unsigned batchID  = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned neuronID = blockIdx.y * blockDim.y + threadIdx.y;
	unsigned startID  = batchID * batchStride;
	
	if(batchID  >= nBatch)				return;
	if(neuronID >= nNeurons)	return;
	
	unsigned thread = 32;
	unsigned block  = ceil(1.0f * nuSize / thread);
	
	const float spike = 1.0f/Ts;
		
	for(unsigned i=0; i<batchStride; ++i)
	{
		unsigned linearID = startID + i + neuronID * Ns;
		if(d_u[linearID] >= theta)
		{
			d_s[linearID] = spike;
			// dynamic parallelism seems to be slower because of race condition!!!
			// ahpKernel<<< block, thread >>>(d_u + linearID, d_nu, nuSize);
			// cudaDeviceSynchronize();
			for(unsigned j=0; j<nuSize; ++j)
			{
				if(startID + i + j < Ns)	d_u[linearID + j] += d_nu[j];
			}
		}
		else	d_s[linearID] = 0.0f;
	}
}

void getSpikes(float* d_s, float* d_u, const float* d_nu, unsigned nNeurons, unsigned nuSize, unsigned Ns, unsigned batchStride, float theta, float Ts)
{
	unsigned nBatch = Ns/batchStride;
	
	dim3 thread, block;
	thread.x = 32;
	thread.y = 8;
	block.x  = ceil(1.0f * nBatch   / thread.x);
	block.y  = ceil(1.0f * nNeurons / thread.y);
	
	getSpikesKernel<<< block, thread >>>(d_s, d_u, d_nu, nBatch, nNeurons, nuSize, batchStride, Ns, theta, Ts);
}

__global__ void evalRhoKernel(float* d_rho, const float* d_u, float theta, float tau, unsigned nNeurons, unsigned Ns, float scale)
{
	unsigned timeID = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned nID    = blockIdx.y * blockDim.y + threadIdx.y;
	
	if(timeID >= Ns || nID >= nNeurons)	return;
	
	unsigned linearID = timeID + nID * Ns;
	
	d_rho[linearID] = scale/tau * exp(-fabs(theta - d_u[linearID])/tau);
}

__global__ void evalDeltaRefractoryKernel(float* delta, float* deltaBar, float* rho, float* nu, unsigned nNeurons, unsigned nuSize, unsigned Ns, float Ts)
{
	// unsigned tID = blockIdx.x * blockDim.x + threadIdx.x;
	// unsigned nID = blockIdx.y * blockDim.y + threadIdx.y;
	unsigned nID = blockIdx.x * blockDim.x + threadIdx.x;
	
	// if(tID >= Ns)		return;
	if(nID >= nNeurons)	return;
	
	for(int tID = Ns-1; tID >= 0; tID--)
	{
		float partialSum = 0.0f;
		unsigned n = tID + nID * Ns;
		unsigned Ks = ((tID + nuSize) < Ns) ? nuSize : (Ns - tID);
		for(unsigned k=1; k<Ks; ++k)	partialSum += nu[k] * delta[n + k];
		float den = (1 - rho[n] * nu[0] * Ts);
		den = ( fabs(den) < 1e-10 ) ? 1e-10 : den;	
		delta[n] = (deltaBar[n] + Ts * partialSum) * rho[n] / den;
	}
}

__global__ void evalDeltaRefractoryKernel(float* delta, float* deltaBar, float* rho, float* nu, unsigned nBatch, unsigned nNeurons, unsigned nuSize, unsigned batchStride, unsigned Ns, float Ts)
{
	unsigned bID = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned nID = blockIdx.y * blockDim.y + threadIdx.y;
	unsigned sID = bID * batchStride;
	
	if(bID >= nBatch)	return;
	if(nID >= nNeurons)	return;
	
	for(int tID = batchStride-1; tID >= 0; tID--)
	{
		unsigned n = sID + tID + nID * Ns;
		
		float partialSum = 0.0f;
		// unsigned n = tID + nID * Ns;
		unsigned Ks = ((tID + nuSize) < batchStride) ? nuSize : (batchStride - tID);
		for(unsigned k=1; k<Ks; ++k)	partialSum += nu[k] * delta[n + k];
		float den = (1 - rho[n] * nu[0] * Ts);
		den = ( fabs(den) < 1e-10 ) ? 1e-10 : den;	
		delta[n] = (deltaBar[n] + Ts * partialSum) * rho[n] / den;
	}
}

void evalDeltaRefractory(float* delta, float* rho, float* nu, unsigned nNeurons, unsigned nuSize, unsigned Ns, float Ts)
{
	unsigned threads = 32;
	evalDeltaRefractoryKernel<<< ceil(1.0f * nNeurons / threads), threads >>>(delta, delta, rho, nu, nNeurons, nuSize, Ns, Ts);
	checkCudaErrors( cudaDeviceSynchronize() );
}

void evalDeltaRefractory(float* delta, float* rho, float* nu, unsigned nNeurons, unsigned nuSize, unsigned batchStride, unsigned Ns, float Ts)
{
	unsigned nBatch = Ns/batchStride;
	
	dim3 thread, block;
	thread.x = 32;
	thread.y = 8;
	block.x  = ceil(1.0f * nBatch   / thread.x);
	block.y  = ceil(1.0f * nNeurons / thread.y);
	
	evalDeltaRefractoryKernel<<< block, thread >>>(delta, delta, rho, nu, nBatch, nNeurons, nuSize, batchStride, Ns, Ts);
	checkCudaErrors( cudaDeviceSynchronize() );
}
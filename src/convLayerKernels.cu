#include <convLayerKernels.h>
#include <slayerio.h>
#include <cudaKernels.h>

// This routine expands weights of convolution layer into equivalent bigger weight matrix of dense layer
__global__ void expandConvWKernel(float* d_Wexp, const float* d_W, dim3 uDim, dim3 aDim, dim4 wDim, unsigned Nout, unsigned Nin)
{
	// Expand the convolution weight matrix, d_W into a fully connected weight matrix, d_Wexp of dimension Nout x Nin
	
	// calcualte the threadID
	unsigned uID = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned aID = blockIdx.y * blockDim.y + threadIdx.y;
	unsigned WID = aID * Nout + uID;
	
	if(aID >= Nin)	return;
	if(uID >= Nout)	return;
	
	bool valid = true;
	unsigned xA = aID % aDim.x;
	unsigned yA =(aID / aDim.x)% aDim.y;
	unsigned zA = aID / aDim.x / aDim.y;
	
	unsigned xU = uID % uDim.x;
	unsigned yU =(uID / uDim.x)% uDim.y;
	unsigned zU = uID / uDim.x / uDim.y;
	
	unsigned xW = xA - xU;
	unsigned yW = yA - yU;
	unsigned zW = zA;
	unsigned nW = zU;
	
	if(xW >= wDim.x)	valid = false;
	if(yW >= wDim.y)	valid = false;
	
	unsigned wID = xW + wDim.x * ( yW + wDim.y * ( zW + wDim.z * nW ) );	
	d_Wexp[WID] = (valid) ? d_W[ wID] : 0.0f;
}

/***** Signal Forward Propagation *****/

// __global__ void multWaConvKernel(float* d_u, const float* d_W, const float* d_a, dim3 uDim, dim3 aDim, dim3 wDim, unsigned numU, unsigned numW, unsigned Ns)
__global__ void multWaConvKernel(float* d_u, const float* d_W, const float* d_a, dim4 uDim, dim4 aDim, dim4 wDim, unsigned numU, unsigned numW, unsigned Ns)
{
	// calcualte the threadID
	unsigned tID = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned nID = blockIdx.y * blockDim.y + threadIdx.y;
	
	if(tID >= Ns)	return;
	if(nID >= numU)	return;
	
	unsigned xU = nID % uDim.x;
	unsigned yU =(nID / uDim.x)% uDim.y;
	unsigned zU = nID / uDim.x / uDim.y;
	
	float result = 0.0f;
	for(unsigned i=0; i<numW; ++i)
	{			
		unsigned xW =  i % wDim.x;
		unsigned yW =( i / wDim.x )% wDim.y;
		unsigned zW =  i / wDim.x  / wDim.y;
		unsigned nW =  zU;
		unsigned wInd = xW + wDim.x * ( yW + wDim.y * ( zW + wDim.z * nW ) );
		
		unsigned xA = xU + xW;
		unsigned yA = yU + yW;
		unsigned zA = zW;
		
		unsigned idA = xA + aDim.x * (yA + aDim.y * zA);
		result += d_W[wInd] * d_a[tID + idA * Ns];
	}
	d_u[tID + nID * Ns] = result;
}

void multWaConv(float* d_u, const float* d_W, const float* d_a, dim4 uDim, dim4 aDim, dim4 wDim)
{
	// dim3 uDim, aDim, wDim;
	// aDim.x = aInfo.dim(0), 	aDim.y = aInfo.dim(1), 	aDim.z = aInfo.dim(2);
	// uDim.x = uInfo.dim(0), 	uDim.y = uInfo.dim(1), 	uDim.z = uInfo.dim(2);
	// wDim.x = uInfo.par(1),	wDim.y = uInfo.par(1),	wDim.z = aInfo.dim(2);
	
	// std::cout << "a -> " << d_a << std::endl;
	// std::cout << "u -> " << d_u << std::endl;
	// std::cout << "w -> " << d_W << std::endl;
	
	// std::cout << "aDim = " << aDim.x << ", " << aDim.y << ", " << aDim.z << ", " << aDim.w << std::endl;
	// std::cout << "uDim = " << uDim.x << ", " << uDim.y << ", " << uDim.z << ", " << uDim.w << std::endl;
	// std::cout << "wDim = " << wDim.x << ", " << wDim.y << ", " << wDim.z << ", " << wDim.w << std::endl;
		
	unsigned numU = uDim.x * uDim.y * uDim.z;
	unsigned numW = wDim.x * wDim.y * wDim.z;
	unsigned Ns   = uDim.w;
	dim3 thread, block;
	thread.x = 128;
	thread.y = 8;
	block.x = ceil(1.0f * Ns  / thread.x);
	block.y = ceil(1.0f * numU/ thread.y);	
	if(block.y >= 65535)	slayerio::error("Error: maximum blockDim.y exceeded at convLayerKernels.h/multWaConv.");
	if(block.z >= 65535)	slayerio::error("Error: maximum blockDim.z exceeded at convLayerKernels.h/multWaConv.");
	
	multWaConvKernel<<< block, thread >>>(d_u, d_W, d_a, uDim, aDim, wDim, numU, numW, Ns);
}

void multWaConv(float* d_u, const float* d_W, const float* d_a, slayerType uInfo, slayerType aInfo, unsigned Ns)
{
	// dim3 uDim, aDim, wDim;
	dim4 uDim, aDim, wDim;
	aDim.x = aInfo.dim(0), 	aDim.y = aInfo.dim(1), 	aDim.z = aInfo.dim(2);
	uDim.x = uInfo.dim(0), 	uDim.y = uInfo.dim(1), 	uDim.z = uInfo.dim(2);
	wDim.x = uInfo.par(1),	wDim.y = uInfo.par(1),	wDim.z = aInfo.dim(2);
		
	unsigned numU = uDim.x * uDim.y * uDim.z;
	unsigned numW = wDim.x * wDim.y * wDim.z;
	dim3 thread, block;
	thread.x = 128;
	thread.y = 8;
	block.x = ceil(1.0f * Ns  / thread.x);
	block.y = ceil(1.0f * numU/ thread.y);	
	if(block.y >= 65535)	slayerio::error("Error: maximum blockDim.y exceeded at convLayerKernels.h/multWaConv.");
	if(block.z >= 65535)	slayerio::error("Error: maximum blockDim.z exceeded at convLayerKernels.h/multWaConv.");
	
	multWaConvKernel<<< block, thread >>>(d_u, d_W, d_a, uDim, aDim, wDim, numU, numW, Ns);
}

// Following version uses expandConvWKernel(...) to expand the conv weight matrix into dense weight matrix and performs standard matrix multiplication
void multWaConv(cublasHandle_t &handle, float* d_u, const float* d_W, const float* d_a, slayerType uInfo, slayerType aInfo, unsigned Ns)
{
	dim3 uDim, aDim;
	dim4 wDim;
	aDim.x = aInfo.dim(0), 	aDim.y = aInfo.dim(1), 	aDim.z = aInfo.dim(2);
	uDim.x = uInfo.dim(0), 	uDim.y = uInfo.dim(1), 	uDim.z = uInfo.dim(2);
	wDim.x = uInfo.par(1),	wDim.y = uInfo.par(1),	wDim.z = aInfo.dim(2);	wDim.w = uInfo.dim(2);
	
	float *d_Wexp;
	unsigned Na = aDim.x * aDim.y * aDim.z;
	unsigned Nu = uDim.x * uDim.y * uDim.z;
	
	cudaMallocManaged(&d_Wexp , sizeof(float) * Nu * Na);
	handleCudaErrorCheck("convLayerKernels.h/multWaConv");
	
	// expand the convolution weight to fully connected weight
	// kernel to map to expanded weight
	dim3 thread, block;
	thread.x = 128;
	thread.y = 8;
	block.x = ceil(1.0f * Nu / thread.x);
	block.y = ceil(1.0f * Na / thread.y);
	if(block.y >= 65535)	slayerio::error("Error: maximum blockDim.y exceeded at convLayerKernels.h/multWaConv.");
	if(block.z >= 65535)	slayerio::error("Error: maximum blockDim.z exceeded at convLayerKernels.h/multWaConv.");
	expandConvWKernel<<< block, thread >>>(d_Wexp, d_W, uDim, aDim, wDim, Nu, Na);
	
	// perform standard forward propagation multiplication using cublas
	const float alf    = 1;
	const float bet    = 0;
	const float *alpha = &alf;
	const float *beta  = &bet;

	cudaDeviceSynchronize();
	cublasStatus_t status = cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_T, Ns, Nu, Na, alpha, d_a, Ns, d_Wexp, Nu, beta, d_u, Ns);
	
	handleCublasStatus(status, "convLayerKernels/multWaConv");
	cudaFree(d_Wexp);
}



/***** Signal Backpropagation *****/

__global__ void multWdeltaConvKernel(float* d_e, const float* d_W, const float* d_delta, dim3 eDim, dim3 dDim, dim3 wDim, unsigned numE, unsigned numW, unsigned Ns)
{
	// calcualte the threadID
	unsigned tID = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned nID = blockIdx.y * blockDim.y + threadIdx.y;
	
	if(tID >= Ns)	return;
	if(nID >= numE)	return;
	
	unsigned xE = nID % eDim.x;
	unsigned yE =(nID / eDim.x)% eDim.y;
	unsigned zE = nID / eDim.x / eDim.y;
	
	float result = 0.0f;
	for(unsigned i=0; i<numW; ++i)
	{			
		unsigned xW =  i % wDim.x;
		unsigned yW =( i / wDim.x )% wDim.y;
		unsigned zW = zE;
		unsigned nW =  i / wDim.x  / wDim.y;
		unsigned wInd = xW + wDim.x * ( yW + wDim.y * ( zW + wDim.z * nW ) );
		
		unsigned xD = xE - xW;
		unsigned yD = yE - yW;
		unsigned zD = nW;
		
		if(xD < dDim.x && yD < dDim.y && zD < dDim.z)	// no need to check if xD, yD and zD are > 0 because they are unsigned and are automatically underflowed to large values :)
		{
			unsigned idD = xD + dDim.x * (yD + dDim.y * zD);
			result += d_W[wInd] * d_delta[tID + idD * Ns];
		}
	}
	d_e[tID + nID * Ns] = result;
}

void multWdeltaConv(float* d_e, const float* d_W, const float* d_delta, slayerType eInfo, slayerType dInfo, unsigned Ns)
{
	dim3 dDim, eDim, wDim;
	eDim.x = eInfo.dim(0), 	eDim.y = eInfo.dim(1), 	eDim.z = eInfo.dim(2);
	dDim.x = dInfo.dim(0), 	dDim.y = dInfo.dim(1), 	dDim.z = dInfo.dim(2);
	wDim.x = dInfo.par(1),	wDim.y = dInfo.par(1),	wDim.z = eInfo.dim(2);
	
	unsigned numE = eInfo.dim(0) * eInfo.dim(1) * eInfo.dim(2);
	unsigned numW = dInfo.par(0) * dInfo.par(1) * dInfo.par(1);
	dim3 thread, block;
	thread.x = 128;
	thread.y = 8;
	block.x = ceil(1.0f * Ns  / thread.x);
	block.y = ceil(1.0f * numE/ thread.y);	
	if(block.y >= 65535)	slayerio::error("Error: maximum blockDim.y exceeded at convLayerKernels.h/multWdeltaConv.");
	if(block.z >= 65535)	slayerio::error("Error: maximum blockDim.z exceeded at convLayerKernels.h/multWdeltaConv.");
	
	multWdeltaConvKernel<<< block, thread >>>(d_e, d_W, d_delta, eDim, dDim, wDim, numE, numW, Ns);
}

// Following version uses expandConvWKernel(...) to expand the conv weight matrix into dense weight matrix and performs standard matrix multiplication
void multWdeltaConv(cublasHandle_t &handle, float* d_e, const float* d_W, const float* d_delta, slayerType eInfo, slayerType dInfo, unsigned Ns)
{
	dim3 eDim, dDim;
	dim4 wDim;
	eDim.x = eInfo.dim(0), 	eDim.y = eInfo.dim(1), 	eDim.z = eInfo.dim(2);
	dDim.x = dInfo.dim(0), 	dDim.y = dInfo.dim(1), 	dDim.z = dInfo.dim(2);
	wDim.x = dInfo.par(1),	wDim.y = dInfo.par(1),	wDim.z = eInfo.dim(2);	wDim.w = dInfo.dim(2);
	
	float *d_Wexp;
	unsigned Ne = eDim.x * eDim.y * eDim.z;
	unsigned Nd = dDim.x * dDim.y * dDim.z;
	
	cudaMallocManaged(&d_Wexp , sizeof(float) * eDim.x * eDim.y * eDim.z * dDim.x * dDim.y * dDim.z);
	handleCudaErrorCheck("convLayerKernels.h/multWdeltaConv");
	
	// expand the convolution weight to fully connected weight
	// kernel to map to expanded weight
	dim3 thread, block;
	thread.x = 128;
	thread.y = 8;
	block.x = ceil(1.0f * Nd/ thread.x);
	block.y = ceil(1.0f * Ne / thread.y);
	expandConvWKernel<<< block, thread >>>(d_Wexp, d_W, dDim, eDim, wDim, Nd, Ne);
	cudaDeviceSynchronize();
	
	// perform standard forward fully connected multiplication using cublas
	const float alf    = 1;
	const float bet    = 0;
	const float *alpha = &alf;
	const float *beta  = &bet;
	

	// Do the actual multiplication
	cublasStatus_t status = cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, Ns, Ne, Nd, alpha, d_delta, Ns, d_Wexp, Nd, beta, d_e, Ns);
	handleCublasStatus(status, "convLayerKernels.h/multWdeltaConv");
	
	cudaFree(d_Wexp);
}


/***** Weight Backpropagation *****/

__global__ void multAdeltaConvKernel(float* d_gradW, const float* d_delta, const float* d_a, dim3 aDim, dim3 dDim, dim4 wDim, unsigned numW, unsigned Ns, float Ts, unsigned tStride)
{
	// must avoid blockDim.y and blockDim.z greater than one
	// calcualte the threadID
	unsigned tPID = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned dPID = blockIdx.y * blockDim.y + threadIdx.y;
	unsigned wID  = blockIdx.z * blockDim.z + threadIdx.z;
	
	extern __shared__ float result[];
	result[threadIdx.x] = 0.0f;
	
	bool valid = true;
	if(tPID>= tStride)	valid = false;
	if(dPID>= dDim.x)	valid = false;
	if(wID >= numW)				valid = false;
	
	for(unsigned yD = 0; yD<dDim.y; ++yD)
	{
		unsigned zD = wID / wDim.x / wDim.y / wDim.z;
			   
		unsigned xA = dPID +   wID % wDim.x;
		unsigned yA = yD   + ( wID / wDim.x )%wDim.y;
		unsigned zA = ( wID / wDim.x / wDim.y )%wDim.z;
		
		unsigned aID = xA + aDim.x * (yA + aDim.y * zA);
		unsigned dID = dPID + yD*dDim.x + zD*dDim.x*dDim.y;
		
		unsigned tID = tPID;
		if(valid)	
		{
			while(tID < Ns)
			{
				// printf("tID = %d\n", tID);
				result[threadIdx.x] += d_delta[tID + dID*Ns] * d_a[tID + aID*Ns];
				tID += tStride;
			}
		}
	}
	
	__syncthreads();
	
	for(unsigned s=blockDim.x/2; s>0; s>>=1)
	{
		if(threadIdx.x < s)
		{
			result[threadIdx.x] += result[threadIdx.x + s];
		}
		__syncthreads();
	}
	
	// if(threadIdx.x == 0)	atomicAdd(d_gradW + wID, result[0] * Ts / dDim.x / dDim.y);	// to implement average gradient, scale by dDim.x * dDim.y
	if(threadIdx.x == 0)	atomicAdd(d_gradW + wID, result[0]);
	// average gradient seems more effective!!
	// The scaling is better done later.
	// When its done here, the non associative nature of floating point addition changes the resulting sum
}

void multAdeltaConv(cublasHandle_t &handle, float* d_gradW, const float* d_delta, const float* d_a, slayerType aInfo, slayerType dInfo, unsigned Ns, float Ts)
{
	dim3 dDim, aDim;
	dim4 wDim;
	aDim.x = aInfo.dim(0), 	aDim.y = aInfo.dim(1), 	aDim.z = aInfo.dim(2);
	dDim.x = dInfo.dim(0), 	dDim.y = dInfo.dim(1), 	dDim.z = dInfo.dim(2);
	wDim.x = dInfo.par(1),	wDim.y = dInfo.par(1),	wDim.z = aInfo.dim(2);	wDim.w = dInfo.dim(2);
	
	unsigned numW = wDim.x * wDim.y * wDim.z * wDim.w;
	dim3 thread, block;
	unsigned tStride = 256;
	thread.x = tStride;
	block.x = ceil(1.0f * tStride/thread.x);
	block.y = ceil(1.0f * dDim.x /thread.y);
	block.z = ceil(1.0f * numW   /thread.z);
	
	multAdeltaConvKernel<<< block, thread, thread.x * thread.y * sizeof(float) >>>(d_gradW, d_delta, d_a, aDim, dDim, wDim, numW, Ns, 1.0, tStride);	
	checkCudaErrors(cudaDeviceSynchronize());
	scale(d_gradW, 1.0f * Ts / dDim.x / dDim.y, numW);
}

#include <cudnnDescriptor.h>
#include <convLayerKernels.h>
#include <slayerio.h>
#include <tictoc.h>


cudnnDescriptor_t::cudnnDescriptor_t()
{
	fwdWorkspaceBytes = 0;
	bwdDataWorkspaceBytes = 0;
	bwdFilterWorkspaceBytes = 0;
	
	workspace = nullptr;
	
	halfInput = nullptr;
	halfOutput = nullptr;
	halfFilter = nullptr;
	
	checkCudnnErrors( cudnnCreate(&cudnnHandle) );
	
	checkCudnnErrors( cudnnCreateTensorDescriptor( &inputDesc ));
	checkCudnnErrors( cudnnCreateFilterDescriptor( &filterDesc ));
	checkCudnnErrors( cudnnCreateTensorDescriptor( &outputDesc ));
	checkCudnnErrors( cudnnCreateTensorDescriptor( &halfInputDesc ));
	checkCudnnErrors( cudnnCreateFilterDescriptor( &halfFilterDesc ));
	checkCudnnErrors( cudnnCreateTensorDescriptor( &halfOutputDesc ));
	checkCudnnErrors( cudnnCreateConvolutionDescriptor( &convDesc ));
}

cudnnDescriptor_t::~cudnnDescriptor_t()
{
	cudnnDestroy(cudnnHandle);
	cudnnDestroyTensorDescriptor(inputDesc);
	cudnnDestroyTensorDescriptor(outputDesc);
	cudnnDestroyFilterDescriptor(filterDesc);
	cudnnDestroyTensorDescriptor(halfInputDesc);
	cudnnDestroyTensorDescriptor(halfOutputDesc);
	cudnnDestroyFilterDescriptor(halfFilterDesc);
	cudnnDestroyConvolutionDescriptor(convDesc);
	cudaFree(workspace);
	cudaFree(halfInput);
	cudaFree(halfOutput);
	cudaFree(halfFilter);
}

void cudnnDescriptor_t::setDescriptors(dim4 _iDim, dim4 _oDim, dim4 _fDim, unsigned deviceComputeCapability, cudnnDataType_t computePrecision)
{
	// create cudnnHandle in device where setDescriptor is called
	cudnnDestroy(cudnnHandle);
	checkCudnnErrors( cudnnCreate(&cudnnHandle) );
	// store the tensor dimensions locally as well
	iDim = _iDim;
	oDim = _oDim;
	fDim = _fDim;
	// calculate padding dimension
	// It works for both zero padding and no zero padding
	dim3 pDim;
	pDim.x = (fDim.x + oDim.x - iDim.x -1)/2;
	pDim.y = (fDim.y + oDim.y - iDim.y -1)/2;
	
	// calculate stride dimension
	// Assuming NCHW format
	// The CUDNN functions fully support NCHW format for the tensors
	// as of CUDNN 7.2, convolutionBackward operations are available for NCHW tensor formats only
	dim4 oStr, iStr;
	oStr.x = 1;		oStr.y = oDim.x;	oStr.z = oStr.y * oDim.y;	oStr.w = oStr.z * oDim.z;
	iStr.x = 1;		iStr.y = iDim.x;	iStr.z = iStr.y * iDim.y;	iStr.w = iStr.z * iDim.z;
	
	// Assuming CHWN format
	// Nevertheless, SLAYER tensors are natively stored in CHWN format.
	// This is to allow coalesced memory access across time dimension for efficient convolution and correlation operations along time
	// as well as spike activation function calculation.
	// This class manages the conversion of tensor formats.
	dim4 oStrT, iStrT;
	oStrT.x = oDim.w;	oStrT.y = oStrT.x * oDim.x;	oStrT.z = oStrT.y * oDim.y;	oStrT.w = 1;
	iStrT.x = iDim.w;	iStrT.y = iStrT.x * iDim.x;	iStrT.z = iStrT.y * iDim.y;	iStrT.w = 1;
	
	// set tensor descriptors
	checkCudnnErrors( cudnnSetTensor4dDescriptorEx(inputDesc, 								// tensor
												   computePrecision,						// dataType
												   iDim.w, iDim.z, iDim.y, iDim.x,			// N, C, H, W
												   iStrT.w, iStrT.z, iStrT.y, iStrT.x) );	// (N, C, H, W)stride
	checkCudnnErrors( cudnnSetTensor4dDescriptorEx(outputDesc,								// tensor
												   computePrecision,						// dataType
												   oDim.w, oDim.z, oDim.y, oDim.x,			// N, C, H, W
												   oStrT.w, oStrT.z, oStrT.y, oStrT.x) );	// (N, C, H, W)stride
	checkCudnnErrors( cudnnSetFilter4dDescriptor  (filterDesc, 							// filterDescriptor
												   computePrecision,					// dataType
												   CUDNN_TENSOR_NCHW,
												   fDim.w, fDim.z, fDim.y, fDim.x) );	// K, C, H, W
    checkCudnnErrors( cudnnSetConvolution2dDescriptor(convDesc,					// convolutionDescriptor
													  pDim.y, pDim.x,			// pad_(height, width)
													  1, 1,						// stride_(vertical, horizontal)
													  1, 1,						// dilation_(height, width)
													  CUDNN_CROSS_CORRELATION,	// convolutionMode
													  computePrecision) );		// computePrecision				
	
	checkCudnnErrors( cudnnSetTensor4dDescriptorEx(halfInputDesc, 						// tensor
												   CUDNN_DATA_HALF,						// dataType
												   iDim.w, iDim.z, iDim.y, iDim.x,		// N, C, H, W
												   iStr.w, iStr.z, iStr.y, iStr.x) );	// (N, C, H, W)stride
	checkCudnnErrors( cudnnSetTensor4dDescriptorEx(halfOutputDesc,						// tensor
												   CUDNN_DATA_HALF,						// dataType
												   oDim.w, oDim.z, oDim.y, oDim.x,		// N, C, H, W
												   oStr.w, oStr.z, oStr.y, oStr.x) );	// (N, C, H, W)stride
	checkCudnnErrors( cudnnSetFilter4dDescriptor  (halfFilterDesc, 						// filterDescriptor
												   CUDNN_DATA_HALF,						// dataType
												   CUDNN_TENSOR_NCHW,
												   fDim.w, fDim.z, fDim.y, fDim.x) );	// K, C, H, W								

	cudaMallocManaged(&halfInput , sizeof(half) * iDim.x * iDim.y * iDim.z * iDim.w);
	cudaMallocManaged(&halfOutput, sizeof(half) * oDim.x * oDim.y * oDim.z * oDim.w);
	cudaMallocManaged(&halfFilter, sizeof(half) * fDim.x * fDim.y * fDim.z * fDim.w);
	
	// Choose proper convolution algoritm to use
	fwdAlgo       =  CUDNN_CONVOLUTION_FWD_ALGO_IMPLICIT_PRECOMP_GEMM;
	bwdDataAlgo   =  CUDNN_CONVOLUTION_BWD_DATA_ALGO_1;
	bwdFilterAlgo =  CUDNN_CONVOLUTION_BWD_FILTER_ALGO_1;
	// bwdDataAlgo   =  CUDNN_CONVOLUTION_BWD_DATA_ALGO_WINOGRAD_NONFUSED ;
	// bwdFilterAlgo =  CUDNN_CONVOLUTION_BWD_FILTER_ALGO_WINOGRAD_NONFUSED ;
	
	// This portion of code enables utilization of tensor cores
	if(deviceComputeCapability >= 70)
	{
		switch(computePrecision)
		{
			case CUDNN_DATA_FLOAT:
				checkCudnnErrors( cudnnSetConvolutionMathType(convDesc, CUDNN_TENSOR_OP_MATH_ALLOW_CONVERSION) );
				break;
			case CUDNN_DATA_HALF:
				checkCudnnErrors( cudnnSetConvolutionMathType(convDesc, CUDNN_TENSOR_OP_MATH) );
				break;
		}
	}
	allocateWorkspace();
}

void cudnnDescriptor_t::allocateWorkspace()
{
	// Allocates necessary workspace for utilization by cudnn routines during runtime
	if(workspace != nullptr)	cudaFree(workspace);
	checkCudnnErrors( cudnnGetConvolutionForwardWorkspaceSize       (cudnnHandle, halfInputDesc , halfFilterDesc, convDesc, halfOutputDesc, fwdAlgo      , &fwdWorkspaceBytes      ) );
	checkCudnnErrors( cudnnGetConvolutionBackwardDataWorkspaceSize  (cudnnHandle, halfFilterDesc, halfOutputDesc, convDesc, halfInputDesc , bwdDataAlgo  , &bwdDataWorkspaceBytes  ) );
	checkCudnnErrors( cudnnGetConvolutionBackwardFilterWorkspaceSize(cudnnHandle, halfInputDesc , halfOutputDesc, convDesc, halfFilterDesc, bwdFilterAlgo, &bwdFilterWorkspaceBytes) );
	
	// it allocaes a common workspace for fwd, bwdData and bwdFilter operations
	size_t workspaceBytes = fwdWorkspaceBytes;
	if(workspaceBytes < bwdDataWorkspaceBytes)		workspaceBytes = bwdDataWorkspaceBytes;
	if(workspaceBytes < bwdFilterWorkspaceBytes)	workspaceBytes = bwdFilterWorkspaceBytes;
	
	if(workspaceBytes > 0)	checkCudaErrors( cudaMallocManaged(&workspace, workspaceBytes) );
}

// convolution forward
// output = filter * input
void cudnnDescriptor_t::fwdConvolution(void* input, void* output, void* filter)
{
	// slayerio::cout << "evaluating cudnn.fwdConvoluton()" << std::endl;
	const float alpha = 1.0f, beta = 0.0f;
	
	// transform data from CHWN format to NCHW format
	checkCudnnErrors( cudnnTransformTensor(cudnnHandle, 
											&alpha, inputDesc, input, 
											&beta, halfInputDesc, halfInput) );
	// convert the weights into fp16
	convertFp32ToFp16((half*)halfFilter, (float*)filter, fDim.x * fDim.y * fDim.z * fDim.w);												  
	
	checkCudnnErrors( cudnnConvolutionForward(cudnnHandle, 
												  (void*)(&alpha), halfInputDesc, halfInput,
												  halfFilterDesc, halfFilter, 
												  convDesc, fwdAlgo,
												  workspace, fwdWorkspaceBytes, 
												  (void*)(&beta), halfOutputDesc, halfOutput) );
	
	// transform data from NCHW format to CHWN format
	checkCudnnErrors( cudnnTransformTensor(cudnnHandle, 
											&alpha, halfOutputDesc, halfOutput, 
											&beta,  outputDesc, output) );
	// float sumO = sumReduce((float*)output, oDim.x * oDim.y * oDim.z * oDim.w);
	// if( isnan(sumO) == 1 )	mexPrintf("\nNAN detected in fwdConvolution!!!!!\n\n\n\n\n\n\n\n\n\n");	
}

// convolution backward data
// dInput = TRANSPOSE(filter) * dOutput
void cudnnDescriptor_t::bwdConvolutionData(void* dInput, void* dOutput, void* filter)
{
	const float alpha = 1.0f, beta = 0.0f;
	const float prescaleIn  = 1.0f * oDim.z * oDim.w;
	const float prescaleOut = 1.0f / oDim.w;
	// const float prescaleIn  = 1.0f * oDim.z * oDim.w * 100;
	// const float prescaleOut = 1.0f / oDim.w / 100;
	// transform data from CHWN format to NCHW format
	checkCudnnErrors( cudnnTransformTensor(cudnnHandle, 
											&prescaleOut, outputDesc, dOutput, 
											&beta, halfOutputDesc, halfOutput) );
	// convert the weights into fp16
	convertFp32ToFp16((half*)halfFilter, (float*)filter, fDim.x * fDim.y * fDim.z * fDim.w, 1.0f/oDim.z);	
	
	checkCudnnErrors( cudnnConvolutionBackwardData(cudnnHandle, 
												  (void*)(&alpha), halfFilterDesc, halfFilter, 
												  halfOutputDesc, halfOutput,
												  convDesc, bwdDataAlgo,
												  workspace, bwdDataWorkspaceBytes, 
												  (void*)(&beta), halfInputDesc, halfInput) );
												  
	// transform data from NCHW format to CHWN format
	checkCudnnErrors( cudnnTransformTensor(cudnnHandle, 
											&prescaleIn, halfInputDesc, halfInput, 
											&beta,  inputDesc, dInput) );
	
	// float sumI = sumReduce((float*)dInput, iDim.x * iDim.y * iDim.z * iDim.w);
	// if( isnan(sumI) == 1 )	for(int i=1; i<5; ++i)	mexPrintf("\nNAN detected in bwdConvolutionData!!!!!\n\n\n\n\n\n\n\n\n\n");	
}

// convolution backward filter
// dFilter = input * TRANSPOSE(dOutput)
void cudnnDescriptor_t::bwdConvolutionFilter(void* input, void* dOutput, void* dFilter, float scale)
{
	const float alpha = 1.0f, beta = 0.0f;
	const float prescaleIn  = 1.0f / oDim.x / oDim.z;
	const float prescaleOut = 1.0f / oDim.y / oDim.w;
	// const float prescaleIn  = 1.0f / oDim.x / oDim.x / oDim.z / 100;
	// const float prescaleOut = 1.0f / oDim.y / oDim.y / oDim.w * 100;
	// transform data from CHWN format to NCHW format
	checkCudnnErrors( cudnnTransformTensor(cudnnHandle, 
											&prescaleIn, inputDesc, input, 
											&beta, halfInputDesc, halfInput) );
	checkCudnnErrors( cudnnTransformTensor(cudnnHandle, 
											&prescaleOut, outputDesc, dOutput, 
											&beta, halfOutputDesc, halfOutput) );
	
	checkCudnnErrors( cudnnConvolutionBackwardFilter(cudnnHandle, 
												  (void*)(&alpha), halfInputDesc, halfInput, 
												  halfOutputDesc, halfOutput,
												  convDesc, bwdFilterAlgo,
												  workspace, bwdFilterWorkspaceBytes, 
												  (void*)(&beta), halfFilterDesc, halfFilter) );
	
	// The gradients are not normalized by cudnn.
	// In practice, normalization by the average count (the spatial dimension of dOutput) gives better result
	// scale *= 1.0f / oDim.x / oDim.y;
	scale *= oDim.z * oDim.w;
	// scale *= oDim.x * oDim.y * oDim.z * oDim.w;
	// this scaling is performed during conversion from Fp32 to Fp16
	// The prescaling avoids sum overflow during computation and avoids NaN values
	
	// convert the weights into fp32
	convertFp16ToFp32((float*)dFilter, (half*)halfFilter, fDim.x * fDim.y * fDim.z * fDim.w, scale);	
	
	// float sumW = sumReduce((float*)dFilter, fDim.x * fDim.y * fDim.z * fDim.w);
	// if( isnan(sumW) == 1 )	
	// {
		// // mexPrintf("\nNAN detected in bwdConvolutionFilter!!!!!\n\n\n\n\n\n\n\n\n\n");
		// float numel = fDim.x * fDim.y * fDim.z * fDim.w;
		// memsetKernel <<< ceil(1.0f * numel / 128), 128 >>>((float*)dFilter, 0.0f, fDim.x * fDim.y * fDim.z * fDim.w);
	// }
}

void cudnnDescriptor_t::testCudnnDescriptor()
{
	unsigned nTrials = 1;
	
	float *u_gt;	// ground truth
	float *u;
	float *W;
	float *a;
	
	cudaSetDevice(0);
	
	slayerio::cout << "CUDA version  : " << cudnnGetCudartVersion() << std::endl;
	slayerio::cout << "CUDNN version : " << cudnnGetVersion() << std::endl;
	
	// create tensors
	cudnnDescriptor_t tensor;

	
	// convolution dimensions
	dim4 uDim, aDim, wDim;
	unsigned Ns = 350*16;
	uDim.x = 60;	uDim.y = 60;	uDim.z = 32;				uDim.w = Ns;
	aDim.x = 64;	aDim.y = 64;	aDim.z = 32;			aDim.w = Ns;
	wDim.x = 5;		wDim.y = 5;		wDim.z = aDim.z;		wDim.w = uDim.z;
	
	
	// allocate memory
	checkCudaErrors( cudaMallocManaged(&u    , sizeof(*u   ) * uDim.x * uDim.y * uDim.z * Ns    ) );
	checkCudaErrors( cudaMallocManaged(&u_gt , sizeof(*u_gt) * uDim.x * uDim.y * uDim.z * Ns    ) );
	checkCudaErrors( cudaMallocManaged(&a    , sizeof(*a   ) * aDim.x * aDim.y * aDim.z * Ns    ) );
	checkCudaErrors( cudaMallocManaged(&W    , sizeof(*W   ) * wDim.x * wDim.y * wDim.z * wDim.w) );
	
	// random initialization
	curandGenerator_t gen;
	checkCurandErrors( curandCreateGenerator(&gen, CURAND_RNG_PSEUDO_DEFAULT) );
	checkCurandErrors( curandSetPseudoRandomGeneratorSeed(gen, time(NULL)) );
	checkCurandErrors( curandGenerateNormal(gen, u_gt, uDim.x * uDim.y * uDim.z * Ns    , 0, 1) );
	checkCurandErrors( curandGenerateNormal(gen, u   , uDim.x * uDim.y * uDim.z * Ns    , 0, 1) );
	checkCurandErrors( curandGenerateNormal(gen, a   , aDim.x * aDim.y * aDim.z * Ns    , 0, 1) );
	checkCurandErrors( curandGenerateNormal(gen, W   , wDim.x * wDim.y * wDim.z * wDim.w, 0, 1) );
	
	// set tensor descriptors
	tensor.setDescriptors(aDim, uDim, wDim, CUDNN_DATA_FLOAT);	
	
	
	slayerio::cout << "\nFull precision calculation:\n";
	
	slayerio::cout << "\nCustom implementation:" << std::endl;
	for(unsigned i=0; i<=nTrials; ++i)
	{ // ignore the first profile which involves memory transfer
		if(i==1)	tic();	
		multWaConv(u_gt, W, a, uDim, aDim, wDim);
		checkCudaErrors(cudaDeviceSynchronize());
	}
	float cudaTime = toc();
	slayerio::cout << "multWa_gt : " << cudaTime/nTrials << "ms elapsed" << std::endl;
	
	// Set the math type to allow cuDNN to use Tensor Cores:
	slayerio::cout << "\nTensor Math CUDNN:" << std::endl;
	for(unsigned i=0; i<=nTrials; ++i)
	{ // ignore the first profile which involves memory transfer
		if(i==1)	tic();	
		tensor.fwdConvolution(a, u, W);										  
		checkCudaErrors(cudaDeviceSynchronize());
	}
	
	float cudnnTime = toc();
	slayerio::cout << "multWa    : " << cudnnTime/nTrials << "ms elapsed" << std::endl;
	slayerio::cout << std::endl << cudaTime/cudnnTime << "x speedup" << std::endl << std::endl;
	
	slayerio::cout << "ERROR : " << maxErr(u, u_gt, uDim.x * uDim.y * uDim.z * Ns) << std::endl;
	for(unsigned i=0; i < 10; ++i)
		slayerio::cout << u[i] - u_gt[i] << "   "  << u[i] << "   " << u_gt[i] << std::endl;
	
	for(unsigned i=0; i<=nTrials; ++i)
	{ // ignore the first profile which involves memory transfer
		if(i==1)	tic();	
		tensor.bwdConvolutionData(a, u, W);										  
		checkCudaErrors(cudaDeviceSynchronize());
	}
	
	slayerio::cout << "multWdelta: " << toc()/nTrials << "ms elapsed" << std::endl;
		
	cudaFree(u_gt);
	cudaFree(u);
	cudaFree(a);
	cudaFree(W);
	
	return;
}
#include <slnetwork.h>
#include <slayerio.h>
#include <random>

std::vector<float> slnetwork::srmKernel(float tau, float minVal)
{
	std::vector<float> eps;
	for(float t = 0; t<tEnd; t+= Ts)
	{
		static float epsVal; 
		switch(neuronType)	
		{
			case SRMALPHA:	epsVal = t/tau*exp(1-t/tau);	
							if(epsVal >= minVal || t<1.1*tau)	eps.push_back(epsVal);
							else								return eps;
							break;
			case LIF: 		epsVal = exp(-t/tau);			
							if(epsVal >= minVal)	eps.push_back(epsVal);
							else					return eps;
							break;
		}
		// if(epsVal >= minVal)	eps.push_back(epsVal);
		// else					break;
	}
	return eps;
}

std::vector<float> slnetwork::refKernel(float tau, float theta, float minVal)
{
	const static auto scaleRef = - config["neuron"]["scaleRef"].as<float>() * config["neuron"]["theta"].as<float>();
	std::vector<float> nu = srmKernel(tau, minVal/theta);
	// for(unsigned i=0; i<nu.size(); ++i)	nu[i] *= -2*theta;
	for(unsigned i=0; i<nu.size(); ++i)	nu[i] *= scaleRef;
	return nu;
}

std::vector<float> slnetwork::srmKernelDiff(float tau, float minVal)
{
	std::vector<float> epsDiff;
	for(float t = 0; t<tEnd; t+= Ts)
	{
		static float epsVal; 
		switch(neuronType)	
		{
			case SRMALPHA:	epsVal = (tau - t)*exp(1-t/tau)/tau/tau;	
							if(fabs(epsVal) >= minVal || t<1.1*tau)	epsDiff.push_back(epsVal);
							else									return epsDiff;
							break;
			case LIF: 		epsVal = -exp(-t/tau)/tau;					
							if(fabs(epsVal) >= minVal)	epsDiff.push_back(epsVal);
							else						return epsDiff;
							break;
		}
	}
	return epsDiff;
}

std::vector<float> slnetwork::refKernelDiff(float tau, float theta, float minVal)
{
	std::vector<float> nuDiff = srmKernelDiff(tau, minVal/theta);
	for(unsigned i=0; i<nuDiff.size(); ++i)	nuDiff[i] *= -theta;
	return nuDiff;
}



slnetwork::slnetwork()
:Ts(1), tEnd(0), tSample(0), nSample(0){}

slnetwork::slnetwork(slayerConfig configDescriptor)
{
	config = configDescriptor.get();
	Ts      = config["simulation"]["Ts"].as<float>();
	tSample = config["simulation"]["tSample"].as<float>();
	nSample = config["simulation"]["nSample"].as<unsigned>();
	tEnd    = tSample * nSample;
	maxIter = config["training"]["stopif"]["maxIter"].as<unsigned>();
	minCost = config["training"]["stopif"]["minCost"].as<float>();
	inPath      = config["training"]["path"]["in"].as<std::string>();
	outPath     = config["training"]["path"]["out"].as<std::string>();
	desiredPath = config["training"]["path"]["desired"].as<std::string>();
	trainList   = config["training"]["path"]["train"].as<std::string>();
	testList    = config["training"]["path"]["test"].as<std::string>();
	neuronType  = configDescriptor.getNeuronType();
	patternType = configDescriptor.getPatternType();
	updateMode  = configDescriptor.getUpdateMode();
	gradAlgo    = configDescriptor.getGradAlgo();
	testing     = config["training"]["testing"].as<bool>();
	classification = config["training"]["classification"].as<bool>();
	bool profile= (config["profile"].as<bool>() == true) ? true : false;
	
	// slayerio::cout << "config  " << config  << std::endl;
	// slayerio::cout << "Ts      " << Ts      << std::endl;
	// slayerio::cout << "tSample " << tSample << std::endl;
	// slayerio::cout << "nSample " << nSample << std::endl;
	// slayerio::cout << "tEnd    " << tEnd    << std::endl;
	// slayerio::cout << "maxIter " << maxIter << std::endl;
	// slayerio::cout << "minCost " << minCost << std::endl;
	// slayerio::cout << "inPath      " << inPath       << std::endl;
	// slayerio::cout << "outPath     " << outPath      << std::endl;
	// slayerio::cout << "desiredPath " << desiredPath  << std::endl;
	// slayerio::cout << "trainList   " << trainList    << std::endl;
	// slayerio::cout << "testList    " << testList     << std::endl;
	
	/***** Parse network architecture *****/
	std::vector<slayerType> layerDesc = slayerConfig::getNetworkInfo(config);
	
	/***** calculate simulation time values *****/
	// vector<float> t(unsigned(tEnd/Ts + 1.5)); 	// This will produce time samples from 0 to tEnd inclusive
	std::vector<float> t(unsigned(tEnd/Ts + 0.5));		// This will produce time samples from 0 to tEnd-Ts
	for(unsigned i=0; i<t.size(); ++i)	t[i] = Ts*i;
	layer.resize(layerDesc.size());
	
	/***** Initialize slayer layers *****/
	slayerio::cout << "Initializing SLAYER layers\n";
	for(unsigned i=0; i<layer.size(); ++i)	
	{
		// this->layer[i] = slayer(layerDesc[i], this->theta[i], this->sigma[i], this->tauRho, this->scaleRho,  t, srmKernel(tauSr), srmKernelDiff(tauSr), refKernel(tauRef, theta[i]), nSample);
		layer[i] = slayer(layerDesc[i], 
						  config["neuron"]["theta"].as<float>(), 
						  config["layer"][i]["sigma"].as<float>(), 
						  config["neuron"]["tauRho"].as<float>(), 
						  config["neuron"]["scaleRho"].as<float>(), 
						  gradAlgo,
						  t, 
						  srmKernel(config["neuron"]["tauSr"].as<float>()), 
						  srmKernelDiff(config["neuron"]["tauSr"].as<float>()), 
						  refKernel(config["neuron"]["tauRef"].as<float>(), config["neuron"]["theta"].as<float>()), 
						  nSample);
		layer[i].setDevices(config["preferredGPU"].as<std::vector<unsigned>>()[0]);
		layer[i].profile = profile;
		layer[i].pre     = (i == 0) ? nullptr : &layer[i+1];
		layer[i].post    = (i == layer.size() - 1) ? nullptr : &layer[i-1];
	}
	layerDes = slayer(layerDesc[layer.size()-1], 0, 0, 0, 0, gradAlgo, t, 
					  srmKernel(config["neuron"]["tauSr"].as<float>()), 
					  srmKernelDiff(config["neuron"]["tauSr"].as<float>()), 
					  refKernel(config["neuron"]["tauRef"].as<float>(), config["neuron"]["theta"].as<float>()), 
					  nSample);
	
	/***** Initialize training and testing scheduler *****/
	auto trainList = config["training"]["path"]["train"].as<std::string>();
	auto testList  = config["training"]["path"]["test"].as<std::string>();
	if(trScheduler.initialize(trainList, nSample) == -1)	slayerio::cout << "Error: could not open " << trainList << slayerErr;
	if(teScheduler.initialize(testList, nSample) == -1)		slayerio::cout << "Error: could not open " << testList  << slayerErr;
	
	/***** Calculate the parameter of the weights *****/
	unsigned nWeights = 0;
	unsigned nDelays = 0;
	for(unsigned i=0; i<layer.size(); i++)
	{
		nWeights += layer[i].info.nWeights * layer[i].info.nDelayedSyn;
		nDelays  += layer[i].info.nDelays  * layer[i].info.nDelayedSyn;
	}
	
	/***** Initialize the weight of the network *****/
	if(configDescriptor.randomInitialization())
	{
		auto randomSeed = config["seed"].as<unsigned>();
		std::default_random_engine 		 		randEng(randomSeed);
		std::normal_distribution<double> 		randn(0, 1);
		std::uniform_real_distribution<double>	rand (0, 1);
		
		weight.resize(nWeights);
		delay. resize(nDelays );
		
		// for(unsigned i=0; i<nWeights; ++i)	weight[i] = static_cast<float>(randn(randEng));
		unsigned pWgt = 0;
		for(unsigned l=0; l<layer.size(); ++l)
		{
			for(unsigned i=0; i< (layer[l].info.nWeights * layer[l].info.nDelayedSyn); ++i)	
			{
				weight[i+pWgt] = config["layer"][l]["wScale"].as<float>() * static_cast<float>(randn(randEng) + 0.1);	// offset of 0.1	
			}
			pWgt += layer[l].info.nWeights;
			
			// mexPrintf("\n");
		}
		bool delayLearning = ( config["training"]["learning"]["etaD"].as<float>() > 1e-6 )? true : false;		
		if(delayLearning)	for(unsigned i=0; i<nDelays ; ++i)	delay [i] = static_cast<float>(rand (randEng));
	}
	else
	{
		auto wSize = configDescriptor.weightSize();
		auto dSize = configDescriptor.delaySize();
		if(wSize!=nWeights)		slayerio::cout << "Error: Expecting " << nWeights << " weights, but the number of weights do not match." << slayerErr;
		if(dSize!=nDelays )		slayerio::cout << "Error: Expecting " << nDelays  << " delays, but the number of delays do not match."   << slayerErr;
		weight.resize(nWeights);
		delay .resize(nDelays);
		
		for(unsigned i=0; i<nWeights; ++i)	weight[i] = static_cast<float>(configDescriptor.weightPtr()[i]);		
		for(unsigned i=0; i<nDelays ; ++i)	delay [i] = static_cast<float>(configDescriptor.delayPtr()[i]);
		
		// for(unsigned i=0; i<10; ++i)	slayerio::print("%10g %10g\n", weight[i], delay[i]);
	}
	
	/***** Assign the weights to layer parameters *****/
	unsigned pWgt = 0;
	unsigned pDel = 0;
	for(unsigned l=0; l<layer.size(); ++l)
	{
		for(unsigned i=0; i<layer[l].info.nWeights * layer[l].info.nDelayedSyn; ++i)	layer[l].wInd[i] = i + pWgt;		
		for(unsigned i=0; i<layer[l].info.nDelays  * layer[l].info.nDelayedSyn; ++i)	layer[l].dInd[i] = i + pDel;		
		pWgt += layer[l].info.nWeights * layer[l].info.nDelayedSyn;
		pDel += layer[l].info.nDelays  * layer[l].info.nDelayedSyn;
		layer[l].setupSynapse(weight, delay);
	}
 
	/***** Initialize classification engine *****/
	// classifier.initialize(config["training"]["path"]["desired"].as<std::string>(), layer.back().info.nNeurons, patternType, tSample);
	classifier.initialize(config["training"]["error"], layer.back().info.nNeurons, patternType, tSample);
}

unsigned slnetwork::displayProfileData()
{
	if(config["profile"].as<bool>() == true)
	{
		unsigned printStrSize = 0;
		unsigned newlines = 0;
		const unsigned out = (layer.size()-1);

		newlines     += slayerio::print("\n");
		printStrSize += slayerio::print("Forward Propagation run time:");
		newlines     += slayerio::print("\n");
		printStrSize += slayerio::print("       %15s %15s %15s %15s", "eps*s", "eps'*s", "u=W a", "getSpikes");
		for(unsigned l=1; l <= out; ++l)
		{
			newlines     += slayerio::print("\n");
			printStrSize += slayerio::print("Layer %2d:  ", l);
			for(int i=0; i<layer[l].fpruntime.size(); ++i)
				printStrSize += slayerio::print("%12.6f ms ", layer[l].fpruntime[i]);
			printStrSize += slayerio::print("\t%10d spikes", layer[l].spikeCount());
			printStrSize += slayerio::print("\t%10d nan", layer[l].nanCount[0]);
		}
		
		newlines     += slayerio::print("\n");
		newlines     += slayerio::print("\n");
		printStrSize += slayerio::print("Error Calculation run time:");
		newlines     += slayerio::print("\n");
		printStrSize += slayerio::print("       %15s %15s", "s_e=s-s_d", "e=eps*s_e", "J", "getSpikes");
		newlines     += slayerio::print("\n");
		printStrSize += slayerio::print("Layer %2d:  ", out);
			for(int i=0; i<layer[out].erruntime.size(); ++i)
				printStrSize += slayerio::print("%12.6f ms ", layer[out].erruntime[i]);
		
		newlines     += slayerio::print("\n");
		newlines     += slayerio::print("\n");
		printStrSize += slayerio::print("Back Propagation run time:");
		newlines     += slayerio::print("\n");
		printStrSize += slayerio::print("       %15s %15s %15s %15s %15s %15s", "rho", "delta", "delta rho", "e=W'delta", "gradW", "gradD");
		for(int l=out-1; l>=0; --l)
		{
			newlines     += slayerio::print("\n");
			printStrSize += slayerio::print("Layer %2d:  ", l);
			for(int i=0; i<layer[l].bpruntime.size(); ++i)
				printStrSize += slayerio::print("%12.6f ms ", layer[l].bpruntime[i]);
			printStrSize += slayerio::print("\t(%10d %10d)nan", layer[l].nanCount[1], layer[l].nanCount[2]);
		}
		newlines     += slayerio::print("\n");
		
		#if defined mex_h
			return printStrSize + newlines;
		#elif defined __linux__ 
			return newlines;	// return number of lines printed
			// return 10 + out * 2;	// return number of lines printed
		#else
			return printStrSize + newlines;
		#endif			
	}	
	
	return 0;
}

void slnetwork::loadDesiredSpikes(std::vector<unsigned> classInd)
{
	const static auto desiredPath = config["training"]["path"]["desired"].as<std::string>();
	const static auto error       = config["training"]["error"];
	
	// parameter checks
	switch(patternType)
	{
		case NumSpikes:		
			if(!error["tgtSpikeRegion"])	slayerio::cout << "Target Spike Region not defined. Check training::error::tgtSpikeRegion in yaml config file." << slayerErr;
			if(!error["tgtSpikeCount"])		slayerio::cout << "Target Spike Count not defined. Check training::error::tgtSpikeCount in yaml config file."   << slayerErr;
			break;
		case ProbSpikes:
			if(!error["probSlidingWin"])	slayerio::cout << "Probability sliding window not defined. Check training::error::probSlidingWin in yaml config file." << slayerErr;
	}
	
	switch(patternType)
	{
		case SpikeTime:	 	layerDes.loadSpikes    (desiredPath, classInd, tSample);	break;
		case AppxSpikeTime: layerDes.loadAppxSpikes(desiredPath, classInd, layer[layer.size()-1], tSample);	break;
		// case NumSpikes: 	layerDes.loadNumSpikes (desiredPath, classInd, layer[layer.size()-1], tSample);	break;
		case NumSpikes: 	layerDes.loadNumSpikes (             classInd, layer[layer.size()-1], error["tgtSpikeRegion"], error["tgtSpikeCount"], tSample);	break;
		case AvgSpikes: 	layerDes.loadAvgSpikes (desiredPath, classInd, tSample);	break;
		case ProbSpikes:	layerDes.loadProbSpikes(             classInd, layer[layer.size()-1], tSample,  error["probSlidingWin"].as<float>()/ Ts);	break;
		default:			slayerio::error("Desired spike pattern type specification undefined. \nCheck patternType variable.\nAborting process now.");
	}
}

void slnetwork::updateParams(unsigned iter)
{
	const static auto etaW    = config["training"]["learning"]["etaW"].as<float>();
	const static auto etaD    = config["training"]["learning"]["etaD"].as<float>();
	const static auto lambda  = config["training"]["learning"]["lambda"].as<float>();
	const static auto beta1   = config["training"]["optimizer"]["beta1"].as<float>();
	const static auto beta2   = config["training"]["optimizer"]["beta2"].as<float>();
	const static auto epsilon = config["training"]["optimizer"]["epsilon"].as<float>();
	const static auto delayLearning = ( etaD > 1e-6 )? true : false;
	switch(updateMode)
	{
		case GradientDescent:	
			for(unsigned l=0; l<layer.size()-1; ++l)	
			{
				if(delayLearning) 	layer[l].gdUpdate(etaW, etaD, lambda);
				else 				layer[l].gdUpdate(etaW, lambda);
			}
			break;
		case Rmsprop:			
			for(unsigned l=0; l<layer.size()-1; ++l)	
			{
				if(delayLearning)	layer[l].rmsPropUpdate(etaW, etaD, lambda);
				else				layer[l].rmsPropUpdate(etaW, lambda);
			}
			break;
		case ADAM:			
			for(unsigned l=0; l<layer.size()-1; ++l)	
			{
				if(delayLearning)	layer[l].adamUpdate(etaW, etaD, lambda, beta1, beta2, iter, epsilon);
				else				layer[l].adamUpdate(etaW, lambda, beta1, beta2, iter, epsilon);
			}
			break;
		case NADAM:			
			for(unsigned l=0; l<layer.size()-1; ++l)	
			{
				if(delayLearning)	layer[l].nadamUpdate(etaW, etaD, lambda, beta1, beta2, iter, epsilon);
				else				layer[l].nadamUpdate(etaW, lambda, beta1, beta2, iter, epsilon);
			}
			break;
		default:
			slayerio::error("ERROR: updateMode is not recognized!!");
	}
	
	for(unsigned l=0; l<layer.size()-1; ++l)	
	{
		// layer[l].weightLimit();
		if(delayLearning)	layer[l].delayLimit();
	}
}

std::vector<float> slnetwork::getWvec()
{
	return weight;
}

std::vector<float> slnetwork::getDvec()
{
	return delay;
}

void slnetwork::updateWvec()
{
	const unsigned out = static_cast<unsigned>(this->layer.size()-1);
	for(unsigned l=0; l <= out; ++l)	this->layer[l].updateWvec(weight);
}

void slnetwork::updateDvec()
{
	const unsigned out = static_cast<unsigned>(this->layer.size()-1);
	for(unsigned l=0; l <= out; ++l)	this->layer[l].updateDvec(delay);
}

std::string slnetwork::writeFileName()
{
	std::stringstream name;
	name << config["seed"].as<std::string>();
	name << "-" << config["training"]["learning"]["etaW"].as<std::string>();
	name << "-" << config["training"]["learning"]["etaD"].as<std::string>();
	return config["training"]["path"]["out"].as<std::string>() + "/" + name.str();
}

void slnetwork::writeWeightDelay()
{
	std::string weightfile = writeFileName() + "-weight.bin";
	std::string delayfile  = writeFileName() + "-delay.bin";
	
	// std::ofstream wout(weightfile.c_str(), std::ios::out);
	// std::ofstream dout(delayfile.c_str() , std::ios::out);
	
	// for(unsigned i=0; i<weight.size(); ++i)	wout << weight[i] << '\n';
	// for(unsigned i=0; i< delay.size(); ++i)	dout << delay [i] << '\n';
	
	std::ofstream wout(weightfile.c_str(), std::ios::out | std::ios::binary);
	std::ofstream dout(delayfile.c_str() , std::ios::out | std::ios::binary);
	
	wout.write((char*)&weight[0], weight.size() * sizeof(weight[0]));
	dout.write((char*)&delay [0], delay .size() * sizeof(delay [0]));
	
	wout.close();
	dout.close();	
}

void slnetwork::writeNetworkConfig()
{
	std::ofstream netout((writeFileName()+"-network.yaml").c_str(), std::ios::out);
	netout << config;
	netout.close();
}
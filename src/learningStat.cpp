#include <learningStat.h>
#include <slayerio.h>

unsigned displayStats(const learningStat &trainingStat, const learningStat &testingStat, unsigned iter, float runtime)
{
	unsigned printStrSize = 0;
	printStrSize += slayerio::print("Iteration: %-10d", iter);
	printStrSize += slayerio::print("\t\t%8.3f ms elapsed in one iteration.", runtime);
	
	printStrSize += slayerio::print("\nTraining : "); 
	if(trainingStat.cost() > 0)			printStrSize += slayerio::print("Cost = %12.6f",   trainingStat.cost());
	if(trainingStat.minimumCost() > 0)	printStrSize += slayerio::print("(min = %12.6f)",  trainingStat.minimumCost());
	else								printStrSize += slayerio::print("       %12s "  ,  " ");
	printStrSize += slayerio::print("\tAccuracy = %f", trainingStat.accuracy());
	printStrSize += slayerio::print("(max = %f)",      trainingStat.maximumAccuracy());
	
	// if(testingStat.cost() > 0)		
	// {
		// printStrSize += slayerio::print("\nTesting  : "); 
		// if(trainingStat.cost() > 0)			if(testingStat.cost() > 0)			printStrSize += slayerio::print("Cost = %12.6f",  testingStat.cost());
											// else 								printStrSize += slayerio::print("Cost = %12s",    "Invalid");
		// if(trainingStat.minimumCost() > 0)	if(testingStat.minimumCost() > 0)	printStrSize += slayerio::print("(min = %12.6f)", testingStat.minimumCost());
											// else 								printStrSize += slayerio::print("(min = %12s)",   "Invalid");
		// printStrSize += slayerio::print("\tAccuracy = %f", testingStat.accuracy());
		// printStrSize += slayerio::print("(max = %f)",      testingStat.maximumAccuracy());
	// }
	if(testingStat.cost() > 0)		
	{
		printStrSize += slayerio::print("\nTesting  : "); 
		printStrSize += slayerio::print("Cost = %12.6f",  testingStat.cost());
		if(testingStat.minimumCost() > 0)	printStrSize += slayerio::print("(min = %12.6f)", testingStat.minimumCost());
		else 								printStrSize += slayerio::print("(min = %12s)",   "Invalid");
		printStrSize += slayerio::print("\tAccuracy = %f", testingStat.accuracy());
		printStrSize += slayerio::print("(max = %f)",      testingStat.maximumAccuracy());
	}
	else	printStrSize += slayerio::print("\n");
	printStrSize += slayerio::print("\n");
	
	// return printStrSize;
	
	#if defined mex_h
		return printStrSize;
	#elif defined _WIN32 || defined _WIN64
		return printStrSize;
	#elif defined __linux__
		return 3;
	#endif			
}
#include <denseLayerKernels.h>
#include <cudaKernels.h>
#include <slayerio.h>

/***** Signal Forward Propagation *****/
void multWa(cublasHandle_t &handle, float* d_u, const float* d_W, const float* d_a, unsigned Nout, unsigned Nin, unsigned Ns)
{
	const float alf    = 1;
	const float bet    = 0;
	const float *alpha = &alf;
	const float *beta  = &bet;
	
	cudaDeviceSynchronize();

	cublasStatus_t status = cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_T, Ns, Nout, Nin, alpha, d_a, Ns, d_W, Nout, beta, d_u, Ns);
	// cublasStatus_t status = cublasXtSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_T, Ns, Nout, Nin, alpha, d_a, Ns, d_W, Nout, beta, d_u, Ns);
	
	// handleCublasStatus(status, "denseLayerKernels.h/multWa");
	checkCublasErrors(status);
}

/***** Signal Backpropagation *****/
// void multWdelta(cublasHandle_t &handle, float* d_e, const float* d_W, const float* d_delta, unsigned Nout, unsigned Nin, unsigned Ns)
void multWdelta(cublasHandle_t &handle, float* d_e, const float* d_W, const float* d_delta, unsigned Ne, unsigned Nd, unsigned Ns)
{
	const float alf    = 1;
	const float bet    = 0;
	const float *alpha = &alf;
	const float *beta  = &bet;

	// Do the actual multiplication
	// cublasStatus_t status = cublasXtSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, Ns, Nout, Nin, alpha, d_delta, Ns, d_W, Nin, beta, d_e, Ns);
	// cublasStatus_t status = cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, Ns, Nout, Nin, alpha, d_delta, Ns, d_W, Nin, beta, d_e, Ns);
	cublasStatus_t status = cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, Ns, Ne, Nd, alpha, d_delta, Ns, d_W, Nd, beta, d_e, Ns);
	// handleCublasStatus(status, "denseLayerKernels.h/multWdelta");
}

/***** Weight Backpropagation *****/
void multAdelta(cublasHandle_t &handle, float* d_gradW, const float* d_delta, const float* d_a, unsigned Nout, unsigned Nin, unsigned Ns, float Ts)
{
	const float alf    = Ts;
	const float bet    = 0;
	const float *alpha = &alf;
	const float *beta  = &bet;

	// Do the actual multiplication
	// cublasStatus_t status = cublasXtSgemm(handle, CUBLAS_OP_T, CUBLAS_OP_N, Nout, Nin, Ns, alpha, d_delta, Ns, d_a, Ns, beta, d_gradW, Nout);
	cublasStatus_t status = cublasSgemm(handle, CUBLAS_OP_T, CUBLAS_OP_N, Nout, Nin, Ns, alpha, d_delta, Ns, d_a, Ns, beta, d_gradW, Nout);
	// handleCublasStatus(status, "denseLayerKernels.h/multAdelta");
}

/***** Delay Backpropagation *****/
void multAdotDelta(cublasHandle_t &handle, float* d_gradD, const float* d_e, float* d_aDot, unsigned nNeurons, unsigned nDelayedSyn, unsigned Ns, float Ts)
{
	dim3 thread, block;
	thread.x = 128;
	thread.y = 8;
	block.x = ceil(1.0f * Ns                    /thread.x);
	block.y = ceil(1.0f * nNeurons * nDelayedSyn/thread.y);
	if(block.y >= 65535)	slayerio::error("Error: maximum blockDim.y exceeded at cudaKernels.h/multAdotDelta.");
	if(block.z >= 65535)	slayerio::error("Error: maximum blockDim.z exceeded at cudaKernels.h/multAdotDelta.");
	
	multKernel<<< block, thread >>>(d_aDot, d_e, Ns, nNeurons * nDelayedSyn);
	
	float* d_ones;
	cudaMalloc(&d_ones, sizeof(float) * Ns);
	memsetKernel<<< ceil(1.0f * Ns/128), 128 >>>(d_ones, 1.0f, Ns);
		
	const float alf    = -Ts;
	const float bet    = 0;
	const float *alpha = &alf;
	const float *beta  = &bet;
	
	// cublasStatus_t status = cublasXtSgemm(handle, CUBLAS_OP_T, CUBLAS_OP_N, nNeurons * nDelayedSyn, 1, Ns, alpha, d_aDot, Ns, d_ones, Ns, beta, d_gradD, nNeurons * nDelayedSyn);
	cublasStatus_t status = cublasSgemv(handle, CUBLAS_OP_T, Ns, nNeurons * nDelayedSyn, alpha, d_aDot, Ns, d_ones, 1, beta, d_gradD, 1);
	
	cudaFree(d_ones);
	
	// handleCublasStatus(status, "denseLayerKernels.h/multAdotdelta");
	checkCublasErrors(status);
	
}
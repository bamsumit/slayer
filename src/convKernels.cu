#include <convKernels.h>
#include <cudaKernels.h>
#include <slayerio.h>

__global__ void convKernel(	float* output, 
							const float* input, const float* filter, 
							unsigned signalSize, unsigned filterSize, 
							unsigned nNeurons, unsigned nDelayedSyn, 
							float Ts)
{
	// calcualte the threadID
	// this is the index of the signal along time axis
	unsigned tID = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned nID = blockIdx.y * blockDim.y + threadIdx.y;
	unsigned kID = blockIdx.z * blockDim.z + threadIdx.z;
	
	if(tID >= signalSize)	return;
	if(nID >= nNeurons)		return;
	if(kID >= nDelayedSyn)	return;
	
	// declare local variables
	float result = 0.0f;
	// TODO
	// check if loadinng filter and signal into local memory helps
	
	// calculate convolution sum
	for(unsigned i=0; i<filterSize; ++i)
	{
		int id = tID - i;
		if(id >= 0)		result += input[id + nID * signalSize] * filter[i];
	}
	output[tID + ( nID + kID * nNeurons )* signalSize] = result * Ts;	
	return;
}

void conv(	float* output, 
			const float* input, const float* filter, 
			unsigned signalSize, unsigned filterSize, 
			unsigned nNeurons, unsigned nDelayedSyn, 
			float Ts)
{
	unsigned nThreads = 128;	// 128 seems better
	dim3 thread(nThreads, 8, 1);
	dim3 block(	ceil( 1.0f * signalSize /thread.x ), 
				ceil( 1.0f * nNeurons   /thread.y ), 
				ceil( 1.0f * nDelayedSyn/thread.z ) ); 
	if(block.y >= 65535)	slayerio::error("Error: maximum blockDim.y exceeded at convKernels.h/delayedConv.");
	if(block.z >= 65535)	slayerio::error("Error: maximum blockDim.z exceeded at convKernels.h/delayedConv.");
	
	convKernel<<< block, thread >>>( output, input, filter, 
											signalSize, filterSize, nNeurons,  nDelayedSyn, Ts);
}

__global__ void delayedConvKernel(	float* output, 
									const float* input, const float* filter, const float* delay, 
									unsigned signalSize, unsigned filterSize, 
									unsigned nNeurons, unsigned nDelayedSyn, 
									float Ts)
{
	// calcualte the threadID
	// this is the index of the signal along time axis
	unsigned tID = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned nID = blockIdx.y * blockDim.y + threadIdx.y;
	unsigned kID = blockIdx.z * blockDim.z + threadIdx.z;
	
	if(tID >= signalSize)	return;
	if(nID >= nNeurons)		return;
	if(kID >= nDelayedSyn)	return;
	
	// declare local variables
	unsigned dInd   = unsigned(delay[nID + kID * nNeurons]/Ts + 0.1f);
	float result = 0.0f;
	// TODO
	// check if loadinng filter and signal into local memory helps
	
	// calculate convolution sum
	for(unsigned i=0; i<filterSize; ++i)
	{
		int id = tID - i - dInd;
		if(id >= 0)		result += input[id + nID * signalSize] * filter[i];
	}
	output[tID + ( nID + kID * nNeurons )* signalSize] = result * Ts;	
	return;
}

void delayedConv(	float* output, 
					const float* input, const float* filter, const float* delay, 
					unsigned signalSize, unsigned filterSize, 
					unsigned nNeurons, unsigned nDelayedSyn, 
					float Ts)
{
	unsigned nThreads = 128;	// 128 seems better
	dim3 thread(nThreads, 8, 1);
	dim3 block(	ceil( 1.0f * signalSize /thread.x ), 
				ceil( 1.0f * nNeurons   /thread.y ), 
				ceil( 1.0f * nDelayedSyn/thread.z ) ); 
	if(block.y >= 65535)	slayerio::error("Error: maximum blockDim.y exceeded at convKernels.h/delayedConv.");
	if(block.z >= 65535)	slayerio::error("Error: maximum blockDim.z exceeded at convKernels.h/delayedConv.");
	
	delayedConvKernel<<< block, thread >>>( output, input, filter, delay, 
											signalSize, filterSize, nNeurons,  nDelayedSyn, Ts);
}

__global__ void corrKernel(	float* output, 
							const float* input, const float* filter, 
							unsigned signalSize, unsigned filterSize, 
							unsigned nNeurons, unsigned nDelayedSyn, 
							float Ts)
{
	// calcualte the threadID
	// this is the index of the signal along time axis
	unsigned tID = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned nID = blockIdx.y * blockDim.y + threadIdx.y;
	unsigned kID = blockIdx.z * blockDim.z + threadIdx.z;
	
	if(tID >= signalSize)	return;
	if(nID >= nNeurons)		return;
	if(kID >= nDelayedSyn)	return;
	
	// declare local variables
	float result = 0.0f;
	// TODO
	// check if loadinng filter and signal into local memory helps
	
	// calculate convolution sum
	for(unsigned i=0; i<filterSize; ++i)
	{
		int id = tID + i;
		if(id < signalSize)		result += input[id + (nID + kID * nNeurons) * signalSize] * filter[i];
	}
	output[tID + nID * signalSize] = result * Ts;
	return;
}

void corr(	float* output, 
			const float* input, const float* filter, 
			unsigned signalSize, unsigned filterSize, 
			unsigned nNeurons, unsigned nDelayedSyn, 
			float Ts)
{
	unsigned nThreads = 128;	// 128 seems better
	dim3 thread(nThreads, 8, 1);
	dim3 block(	ceil( 1.0f * signalSize /thread.x ), 
				ceil( 1.0f * nNeurons   /thread.y ), 
				ceil( 1.0f * nDelayedSyn/thread.z ) ); 
	if(block.y >= 65535)	slayerio::error("Error: maximum blockDim.y exceeded at convKernels.h/delayedCorr.");
	if(block.z >= 65535)	slayerio::error("Error: maximum blockDim.z exceeded at convKernels.h/delayedCorr.");
	
	corrKernel<<< block, thread >>>( output, input, filter, 
											signalSize, filterSize, nNeurons,  nDelayedSyn, Ts );
}

__global__ void delayedCorrKernel(	float* output, 
									const float* input, const float* filter, const float* delay, 
									unsigned signalSize, unsigned filterSize, 
									unsigned nNeurons, unsigned nDelayedSyn, 
									float Ts)
{
	// calcualte the threadID
	// this is the index of the signal along time axis
	unsigned tID = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned nID = blockIdx.y * blockDim.y + threadIdx.y;
	unsigned kID = blockIdx.z * blockDim.z + threadIdx.z;
	
	if(tID >= signalSize)	return;
	if(nID >= nNeurons)		return;
	if(kID >= nDelayedSyn)	return;
	
	// declare local variables
	unsigned dInd   = unsigned(delay[nID + kID * nNeurons]/Ts + 0.1f);
	float result = 0.0f;
	// TODO
	// check if loading filter and signal into local memory helps
	
	// calculate convolution sum
	for(unsigned i=0; i<filterSize; ++i)
	{
		int id = tID + i + dInd;
		if(id < signalSize)		result += input[id + (nID + kID * nNeurons) * signalSize] * filter[i];
	}
	output[tID + nID * signalSize] = result * Ts;
	return;
}

void delayedCorr(	float* output, 
					const float* input, const float* filter, const float* delay, 
					unsigned signalSize, unsigned filterSize, 
					unsigned nNeurons, unsigned nDelayedSyn, 
					float Ts)
{
	unsigned nThreads = 128;	// 128 seems better
	dim3 thread(nThreads, 8, 1);
	dim3 block(	ceil( 1.0f * signalSize /thread.x ), 
				ceil( 1.0f * nNeurons   /thread.y ), 
				ceil( 1.0f * nDelayedSyn/thread.z ) ); 
	if(block.y >= 65535)	slayerio::error("Error: maximum blockDim.y exceeded at convKernels.h/delayedCorr.");
	if(block.z >= 65535)	slayerio::error("Error: maximum blockDim.z exceeded at convKernels.h/delayedCorr.");
	
	delayedCorrKernel<<< block, thread >>>( output, input, filter, delay, 
											signalSize, filterSize, nNeurons,  nDelayedSyn, Ts );
}

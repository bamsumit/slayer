#include <initGPU.h>
#include <slayerio.h>

// std::vector<unsigned> getPreferredGPUs(const mxArray* mxField)
// {
	// mxArray *networkParams;
	// networkParams = mxGetField(mxField, 0, "preferredGPU");
	// if(networkParams == NULL)
	// {
		// mexPrintf("Default GPU list will be used\n");
		// std::vector<unsigned> preferredGPUs;
		// return preferredGPUs;
	// }

	// std::vector<unsigned> preferredGPUs(mxGetM(networkParams)*mxGetN(networkParams));
	// double *fptr = mxGetPr(networkParams);
	// for(int i=0; i<preferredGPUs.size(); ++i)	preferredGPUs[i] = static_cast<unsigned>(fptr[i] + 0.1);
	
	// mexPrintf("PreferredGPUs : ");
	// for(int i=0; i<preferredGPUs.size(); ++i)	mexPrintf("%d  ", preferredGPUs[i]);
	// mexPrintf("\n");
	
	// return preferredGPUs;
// }

std::vector<unsigned> getAvailableGPUs()
{
	std::vector<unsigned> deviceID;
	int nDevices;
	cudaGetDeviceCount(&nDevices);
	cudaDeviceProp prop0;
	for(int i=0; i<nDevices; ++i)
	{
		cudaDeviceProp prop;
		cudaGetDeviceProperties(&prop, i);
		if(prop.major < 6)	continue;
		if(deviceID.size() == 0)
		{
			deviceID.push_back(i);
			prop0 = prop;
		}
		else
		{
			#if defined _WIN32 || defined _WIN64
				if(_stricmp(prop0.name, prop.name)== 0)		deviceID.push_back(i);
			#elif defined __linux__
				if(strcasecmp(prop0.name, prop.name)== 0)	deviceID.push_back(i);
			#endif
		}
	}
	
	if(deviceID.size()==0)	slayerio::error("ERROR: No GPU with cc >= 6.0 found\n");
	
	return deviceID;
}

std::vector<unsigned> getAvailableGPUs(std::vector<unsigned> preferredGPUs)
{
	std::vector<unsigned> deviceID;
	
	cudaDeviceProp prop0;
	for(int i=0; i<preferredGPUs.size(); ++i)
	{
		cudaDeviceProp prop;
		cudaGetDeviceProperties(&prop, preferredGPUs[i]);
		if(prop.major < 6)	continue;
		if(deviceID.size() == 0)
		{
			deviceID.push_back(preferredGPUs[i]);
			prop0 = prop;
		}
		else
		{
			#if defined _WIN32 || defined _WIN64
				if(_stricmp(prop0.name, prop.name)== 0)		deviceID.push_back(preferredGPUs[i]);
			#elif defined __linux__
				if(strcasecmp(prop0.name, prop.name)== 0)	deviceID.push_back(preferredGPUs[i]);
			#endif
		}
	}
	
	// std::vector<unsigned> availableGPUs = getAvailableGPUs();
	
	// for(unsigned i=0; i<preferredGPUs.size(); ++i)
	// {
		// for(unsigned j=0; j<availableGPUs.size(); ++j)
		// {
			// if(preferredGPUs[i] == availableGPUs[j])		deviceID.push_back(availableGPUs[j]);
		// }
	// }
	
	if(deviceID.size()==0)	slayerio::error("ERROR: No GPU with cc >= 6.0 found in list of preferredGPUs\n");
	
	return deviceID;
}

void establishGPUPeerAccess(std::vector<unsigned> deviceID)
{
	// establish peer-to-peer access between multiple devices
	for(int i=0; i<deviceID.size(); ++i)
	{
		cudaSetDevice(deviceID[i]);		// set device i the current device
		for(int j=0; j<deviceID.size(); ++j)
		{
			if(i!=j)
			{
				int canAccessPeer = -1;
				cudaDeviceCanAccessPeer(&canAccessPeer, deviceID[i], deviceID[j]);	// check if device i can peer access device j
				// cudaDeviceDisablePeerAccess(deviceID[j]);
				cudaError_t errorFlag = cudaDeviceEnablePeerAccess(deviceID[j], 0);			// second argument of cudaDeviceEnablePeerAccess is always zero for now. to be used in future versions of cuda
				if(canAccessPeer != 1 || errorFlag != cudaSuccess)				// failure in peer-to-peer memory setup
				{	
					if(errorFlag != cudaErrorPeerAccessAlreadyEnabled)
					{
						slayerio::cout << "Error: could not establish peer access from device " << deviceID[i] << " to device " << deviceID[j] << std::endl;
						slayerio::cout << "canAccessPeer = " << canAccessPeer << std::endl;
						slayerio::cout << "cudaDeviceEnablePeerAccess = cudaError_t(" << errorFlag << ")" << std::endl;
						slayerio::error(" ");
					}
					cudaGetLastError();	// resets error status
				}
			}
		}
	}
	
	// for(unsigned i=0; i<deviceID.size(); ++i)	
	// {
		// int value;
		// cudaSetDevice(deviceID[i]);
		// cudaDeviceGetAttribute ( &value,  cudaDevAttrConcurrentManagedAccess, deviceID[i]);
		// mexPrintf("\ncudaDevAttrConcurrentManagedAccess[%d] : %d", deviceID[i], value);
	// }
	
	slayerio::print("\nSuccessfully established peer-to-peer access between ");
	for(unsigned i=0; i<deviceID.size(); ++i)	slayerio::print( (i == deviceID.size()-1) ? "\b\b & Device %d\n" : "Device %d, ", deviceID[i]);
}

void removeGPUPeerAccess(std::vector<unsigned> deviceID)
{
	for(int i=0; i<deviceID.size(); ++i)
	{
		cudaSetDevice(deviceID[i]);		// set device i the current device
		for(int j=0; j<deviceID.size(); ++j)
		{
			if(i!=j)
			{
				int canAccessPeer = -1;
				cudaDeviceCanAccessPeer(&canAccessPeer, deviceID[i], deviceID[j]);	// check if device i can peer access device j
				cudaDeviceDisablePeerAccess(deviceID[j]);
			}
		}
	}
	slayerio::print("\nSuccessfully disabled peer-to-peer access between ");
	for(unsigned i=0; i<deviceID.size(); ++i)	slayerio::print( (i == deviceID.size()-1) ? "\b\b & Device %d\n" : "Device %d, ", deviceID[i]);
}

std::vector<unsigned> initializeMultiGPU(std::vector<unsigned> preferredGPUs)
{
	std::vector<unsigned> deviceID;
	if(preferredGPUs.size() > 0)	deviceID = getAvailableGPUs(preferredGPUs);
	else							deviceID = getAvailableGPUs();
	establishGPUPeerAccess(deviceID);	
	return deviceID;
}

void deInitializeMultiGPU(std::vector<unsigned> deviceID)
{
	removeGPUPeerAccess(deviceID);
}
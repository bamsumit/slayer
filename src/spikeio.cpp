#include <spikeio.h>

unsigned read1DBinSpikes(std::string filePath, std::vector<unsigned> &neuronID, std::vector<float> &timeID, std::vector<char> &polarity)
{
    unsigned char *eventStream = nullptr;
    std::streampos size = 0;

	std::ifstream inFile(filePath.c_str(), std::ios::in|std::ios::binary|std::ios::ate);
    if (inFile.is_open())
    {
        size = inFile.tellg();
        eventStream = new unsigned char[size];
        inFile.seekg(0, std::ios::beg);
        inFile.read ((char*)(eventStream), size);
        inFile.close();
    }
	else	return 0;

    neuronID.resize(size/5);
    polarity.resize(size/5);
    timeID.resize(size/5);

	for(unsigned i=0; i<size; i+=5)
    {
		neuronID[i/5] = (static_cast<unsigned>(eventStream[i]) << 8) + eventStream[i+1];
        polarity[i/5] = eventStream[i+2] >> 7;
        unsigned time = (static_cast<unsigned>(eventStream[i+2] & 0x7F) << 16) +
                        (static_cast<unsigned>(eventStream[i+3]) << 8) +
                                               eventStream[i+4];
        timeID  [i/5] = time/1000.0f;
    }
	
	delete [] eventStream;

    return static_cast<unsigned>(size/5);
}

void write1DBinSpikes(std::string filePath, std::vector<unsigned> &neuronID, std::vector<float> &timeID, std::vector<char> &polarity)
{
    std::streampos size = 5*neuronID.size();
    unsigned char *eventStream = new unsigned char[size];


    for(unsigned i=0; i<size; i+=5)
    {
        unsigned timeIDnow = static_cast<unsigned>(timeID[i/5] * 1000);
        eventStream[i]   = (neuronID[i/5] & 0xFF00) >> 8;
        eventStream[i+1] =  neuronID[i/5] & 0x00FF;
        eventStream[i+2] =((timeIDnow & 0x7F0000) >> 16) + polarity[i/5]*0x80;
        eventStream[i+3] = (timeIDnow & 0xFF00) >> 8;
        eventStream[i+4] =  timeIDnow & 0x00FF;
    }

    std::ofstream outFile(filePath.c_str(), std::ofstream::binary);
    outFile.write((char*)(eventStream), size);
    outFile.close();
	delete [] eventStream;
}

unsigned read2DBinSpikes(std::string filePath, std::vector<unsigned char> &xPos, std::vector<unsigned char> &yPos, std::vector<float> &timeID, std::vector<char> &polarity)
{
    unsigned char *eventStream = nullptr;
    std::streampos size = 0;

	std::ifstream inFile(filePath.c_str(), std::ios::in|std::ios::binary|std::ios::ate);
    if (inFile.is_open())
    {
        size = inFile.tellg();
        eventStream = new unsigned char[size];
        inFile.seekg(0, std::ios::beg);
        inFile.read ((char*)(eventStream), size);
        inFile.close();
    }
	else	return 0;

        xPos.resize(size/5);
        yPos.resize(size/5);
    polarity.resize(size/5);
      timeID.resize(size/5);

	for(unsigned i=0; i<size; i+=5)
    {
		    xPos[i/5] = eventStream[i];
		    yPos[i/5] = eventStream[i+1];
        polarity[i/5] = eventStream[i+2] >> 7;
        unsigned time = (static_cast<unsigned>(eventStream[i+2] & 0x7F) << 16) +
                        (static_cast<unsigned>(eventStream[i+3]) << 8) +
                                               eventStream[i+4];
        timeID  [i/5] = time/1000.0f;
    }
	
	delete [] eventStream;

    return static_cast<unsigned>(size/5);
}


unsigned read1DNumSpikes(std::string filePath, std::vector<unsigned> &neuronID, std::vector<float> &tSt, std::vector<float> &tEn, std::vector<unsigned> &nSpikes)
{
    unsigned char *eventStream = nullptr;
    std::streampos size = 0;

	std::ifstream inFile(filePath.c_str(), std::ios::in|std::ios::binary|std::ios::ate);
    if (inFile.is_open())
    {
        size = inFile.tellg();
        eventStream = new unsigned char[size];
        inFile.seekg(0, std::ios::beg);
        inFile.read ((char*)(eventStream), size);
        inFile.close();
    }
	else	return 0;

    neuronID.resize(size/10);
         tSt.resize(size/10);
         tEn.resize(size/10);
     nSpikes.resize(size/10);

	for(unsigned i=0; i<size; i+=10)
    {
		neuronID[i/10]  = (static_cast<unsigned>(eventStream[i]) << 8) + eventStream[i+1];
        unsigned tStart = (static_cast<unsigned>(eventStream[i+2]) << 16) +
                          (static_cast<unsigned>(eventStream[i+3]) << 8) +
                                                 eventStream[i+4];
        unsigned tEnd   = (static_cast<unsigned>(eventStream[i+5]) << 16) +
                          (static_cast<unsigned>(eventStream[i+6]) << 8) +
                                                 eventStream[i+7];
		nSpikes[i/10]   = (static_cast<unsigned>(eventStream[i+8]) << 8) + eventStream[i+9];
        
        tSt[i/10] = tStart/1000.0f;
        tEn[i/10] = tEnd/1000.0f;
    }
	
	delete [] eventStream;

    return static_cast<unsigned>(size/10);
}

void write1DNumSpikes(std::string filePath, std::vector<unsigned> &neuronID, std::vector<float> &tSt, std::vector<float> &tEn, std::vector<int> &nSpikes)
{
    std::streampos size = 10*neuronID.size();
    unsigned char *eventStream = new unsigned char[size];


    for(unsigned i=0; i<size; i+=10)
    {
        unsigned tStart = static_cast<unsigned>(tSt[i/10] * 1000);
        unsigned tEnd   = static_cast<unsigned>(tSt[i/10] * 1000);
        eventStream[i]   = (neuronID[i/10] & 0xFF00) >> 8;
        eventStream[i+1] =  neuronID[i/10] & 0x00FF;
        eventStream[i+2] = (tStart & 0x7F0000) >> 16;
        eventStream[i+3] = (tStart & 0xFF00) >> 8;
        eventStream[i+4] =  tStart & 0x00FF;
        eventStream[i+5] = (  tEnd & 0x7F0000) >> 16;
        eventStream[i+6] = (  tEnd & 0xFF00) >> 8;
        eventStream[i+7] =    tEnd & 0x00FF;
		eventStream[i+8] = (nSpikes[i/10] & 0xFF00) >> 8;
        eventStream[i+9] =  nSpikes[i/10] & 0x00FF;
    }

    std::ofstream outFile(filePath.c_str(), std::ofstream::binary);
    outFile.write((char*)(eventStream), size);
    outFile.close();
	
	delete [] eventStream;
}

unsigned generate1DNumSpikes(std::vector<unsigned> &neuronID, std::vector<float> &tSt, std::vector<float> &tEn, std::vector<unsigned> &nSpikes, 
							 unsigned classID, unsigned nNeurons, int trueSpikes, int falseSpikes, float regionStart, float regionStop)
{
	neuronID.resize(nNeurons);
         tSt.resize(nNeurons);
         tEn.resize(nNeurons);
     nSpikes.resize(nNeurons);
	 
	 for(unsigned i=0; i<nNeurons; i++)
    {
		neuronID[i] = i;
        tSt[i]      = regionStart;
        tEn[i]      = regionStop;
		nSpikes[i]  = (classID == i) ? trueSpikes : falseSpikes;
    }
	
	return nNeurons;
}
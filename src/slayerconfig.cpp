#include <slayerconfig.h>
#include <slayerio.h>
#include <chrono>
#include <time.h>
#include <initGPU.h>

void slayerConfig::setDefaultConfig()
{
	// defaultConfig["neuron"]["scaleRef"] = 2;
	// defaultConfig["neuron"]["scaleRho"] = 1;
	// defaultConfig["neuron"]["tauRho"]  = 1;
	// defaultConfig["arch"]["wScale"] = 1;
	// defaultConfig["arch"]["sigma"] = 0;
	// defaultConfig["training"]["learning"]["etaD"] = 0;
	// defaultConfig["training"]["learning"]["lambda"] = 0;
	// defaultConfig["training"]["testing"] = false;
	// defaultConfig["training"]["error"]["probSlidingWin"] = 20;
	// defaultConfig["training"]["optimizer"]["beta1"] = 0.9;
	// defaultConfig["training"]["optimizer"]["beta2"] = 0.999;
	// defaultConfig["training"]["optimizer"]["epsilon"] = 1e-8;
	// defaultConfig["training"]["gradAlgo"] = "SLAYER";
	// defaultConfig["profile"] = false;
	setDefaultConfig("defaultConfig.yaml");
	// #if defined _WIN32 || defined _WIN64
		// defaultConfig["seed"] = static_cast<unsigned>(std::chrono::system_clock::now().time_since_epoch().count());
	// #elif defined __linux__
		// defaultConfig["seed"] = static_cast<unsigned>(time(NULL));
	// #endif
	// defaultConfig["preferredGPU"] = getAvailableGPUs();
	// slayerio::cout << "Classification = " << defaultConfig["training"]["classification"].as<std::string>() << std::endl;
}

void slayerConfig::setDefaultConfig(std::string path)
{
	try
	{
		defaultConfig = YAML::LoadFile(path.c_str());
	}
	catch(YAML::BadFile)
	{
		slayerio::cout << "Error loading defualtConfig. Make sure " << path << " is in your local directory." << slayerErr;
	}
	
	#if defined _WIN32 || defined _WIN64
		defaultConfig["seed"] = static_cast<unsigned>(std::chrono::system_clock::now().time_since_epoch().count());
	#elif defined __linux__
		defaultConfig["seed"] = static_cast<unsigned>(time(NULL));
	#endif
	defaultConfig["preferredGPU"] = getAvailableGPUs();
}

void slayerConfig::setUnknownValuesToDefault()
{
	if(!config["neuron"]["scaleRef"]                 ) config["neuron"]["scaleRef"]                  = defaultConfig["neuron"]["scaleRef"]                 ;
	if(!config["neuron"]["scaleRho"]                 ) config["neuron"]["scaleRho"]                  = defaultConfig["neuron"]["scaleRho"]                 ;
	if(!config["neuron"]["tauRho"]                   ) config["neuron"]["tauRho"]                    = defaultConfig["neuron"]["tauRho"]                   ;
	if(!config["training"]["learning"]["etaD"]       ) config["training"]["learning"]["etaD"]        = defaultConfig["training"]["learning"]["etaD"]       ;
	if(!config["training"]["learning"]["lambda"]     ) config["training"]["learning"]["lambda"]      = defaultConfig["training"]["learning"]["lambda"]     ;
	if(!config["training"]["testing"]                ) config["training"]["testing"]                 = defaultConfig["training"]["testing"]                ;
	if(!config["training"]["classification"]         ) config["training"]["classification"]          = defaultConfig["training"]["classification"]         ;
	if(!config["training"]["error"]["probSlidingWin"]) config["training"]["error"]["probSlidingWin"] = defaultConfig["training"]["error"]["probSlidingWin"];
	if(!config["training"]["optimizer"]["beta1"]     ) config["training"]["optimizer"]["beta1"]      = defaultConfig["training"]["optimizer"]["beta1"]     ;
	if(!config["training"]["optimizer"]["beta2"]     ) config["training"]["optimizer"]["beta2"]      = defaultConfig["training"]["optimizer"]["beta2"]     ;
	if(!config["training"]["optimizer"]["epsilon"]   ) config["training"]["optimizer"]["epsilon"]    = defaultConfig["training"]["optimizer"]["epsilon"]   ;
	if(!config["training"]["gradAlgo"]               ) config["training"]["gradAlgo"]                = defaultConfig["training"]["gradAlgo"]               ;
	YAML::Node defaultLayer;
	for(unsigned l=0; l<config["layer"].size(); ++l)
	{
		defaultLayer[l]["dim"] = config["layer"][l]["dim"];
		if(!config["layer"][l]["wScale"]) 	defaultLayer[l]["wScale"] = defaultConfig["arch"]["wScale"];
		else								defaultLayer[l]["wScale"] = config["layer"][l]["wScale"];
		if(!config["layer"][l]["sigma"] ) 	defaultLayer[l]["sigma"]  = defaultConfig["arch"]["sigma"] ;
		else 								defaultLayer[l]["sigma"]  = config["layer"][l]["sigma"] ;
	}
	config.remove("layer");
	config["layer"] = defaultLayer;
	if(randomInitialization())	
		if(!config["seed"]) 		config["seed"]     = defaultConfig["seed"];
	
	// config["defaultGPU"] is handled separately for different constructor definitions
	if(!config["profile"])	config["profile"] = defaultConfig["profile"];
	
	// config["simulation"]["tEnd"] = config["simulation"]["tSample"].as<double>() * config["simulation"]["nSample"].as<unsigned>(); // tEnd is automatically calculated by slnetwork
}

slayerConfig::slayerConfig() : weight(nullptr), delay(nullptr), wSize(0), dSize(0)
{
	setDefaultConfig();
}

slayerConfig::slayerConfig(YAML::Node data) : weight(nullptr), delay(nullptr), wSize(0), dSize(0), config(data)
{
	setDefaultConfig();
	setUnknownValuesToDefault();
	
	if(config["preferredGPU"])
	{
		std::vector<unsigned> gpuList;
		if(config["preferredGPU"].IsSequence())		gpuList = config["preferredGPU"].as<std::vector<unsigned>>();
		else										gpuList.push_back(config["preferredGPU"].as<unsigned>());
		config["preferredGPU"] = getAvailableGPUs(gpuList);
	}
	else	config["preferredGPU"] = getAvailableGPUs();
}

#if defined mex_h
	slayerConfig::slayerConfig(int nrhs, const mxArray *prhs[])
	{
		setDefaultConfig();
		
		/* Input */
		switch(nrhs)
		{
			case 3:
				#define weight_IN prhs[1]
				#define delay_IN  prhs[2]
			case 2:
				#define randomSeed_IN prhs[1]
			case 1: 
				#define network_IN prhs[0]
				break;
			default:
				slayerio::error("Error: unexpected number of input arguments.\nnumInputs: %d", nrhs);
		}
		
		// slayerio::cout << "Trying to read MATLAB structure" << std::endl;
		
		loadYAMLNode(network_IN);
		
		// slayerio::cout << "Read MATLAB structure complete" << std::endl << config << std::endl;

		switch(nrhs)
		{
			case 3:
				weight = mxGetPr(weight_IN);
				delay  = mxGetPr(delay_IN);
				wSize = static_cast<int>(mxGetM(weight_IN)*mxGetN(weight_IN));
				dSize = static_cast<int>(mxGetM(delay_IN )*mxGetN(delay_IN ));
				break;
			default:
				weight = nullptr;
				delay  = nullptr;
				wSize  = 0;
				dSize  = 0;
		}
		
		switch(nrhs)
		{
			case 2:
				config["seed"] = static_cast<int>(mxGetScalar(randomSeed_IN));
				break;
		}
		
		setUnknownValuesToDefault();
	}
	
	void slayerConfig::loadYAMLNode(const mxArray* mxField)
	{
		// find which format the structure is in...
		bool oldFormat = false;
		
		mxArray *param = mxGetField(mxField, 0, "layer");
		if(param == nullptr)	oldFormat = true;
		
		if(oldFormat == true)
		{
			// slayerio::cout << mex::getScalar(mxField, "Ts");
			config["simulation"]["Ts"]          = mex::getScalar(mxField, "Ts");			   					
			config["simulation"]["tSample"]     = mex::getScalar(mxField, "tSample");	   					
			config["simulation"]["nSample"]     = static_cast<unsigned>(mex::getScalar(mxField, "nSample")); 
			// config[]["tEnd"]        = this->tSample * this->nSample;								   			
			
			
			config["neuron"]["type"]     = mex::getString(mxField, "neuronType");
			config["neuron"]["tauSr"]    = mex::getScalar(mxField, "tauSr");		
			config["neuron"]["tauRef"]   = mex::getScalar(mxField, "tauRef");	
			config["neuron"]["scaleRef"] = mex::getScalar(mxField, "scaleRef", defaultConfig["neuron"]["scaleRef"].as<double>());	
			config["neuron"]["scaleRho"] = mex::getScalar(mxField, "scaleRho", defaultConfig["neuron"]["scaleRho"].as<double>());	
			config["neuron"]["tauRho"]   = mex::getScalar(mxField, "tauRho",   defaultConfig["neuron"]["tauRho"].as<double>());  	
			
			config["training"]["learning"]["etaW"]	  = mex::getScalar(mxField, "eta");		
			config["training"]["learning"]["etaD"]	  = mex::getScalar(mxField, "etaD",    defaultConfig["training"]["learning"]["etaD"].as<double>());	   		
			config["training"]["learning"]["lambda"]  = mex::getScalar(mxField, "lambda",  defaultConfig["training"]["learning"]["lambda"].as<double>());	   		
			
			config["training"]["testing"]        = static_cast<bool>(mex::getScalar(mxField, "testing",        defaultConfig["training"]["testing"].as<bool>()));	
			config["training"]["classification"] = static_cast<bool>(mex::getScalar(mxField, "classification", defaultConfig["training"]["classification"].as<bool>()));	

			config["training"]["error"]["type"] = mex::getString(mxField, "patternType");   				   		
			config["training"]["error"]["probSlidingWin"] = mex::getScalar(mxField, "probSlidingWin", defaultConfig["training"]["error"]["probSlidingWin"].as<double>());	
			
			if(config["training"]["error"]["type"].as<std::string>().compare("NumSpikes") == 0)
			{
				auto *tgtSpikeRegion = mxGetField(mxField, 0, "tgtSpikeRegion");
				auto *tgtSpikeCount  = mxGetField(mxField, 0, "tgtSpikeCount");
				if(tgtSpikeRegion == NULL)	slayerio::cout << "Target Spike Region not defined. Set tgtSpikeRegion.start and tgtSpikeRegion.stop fields." << slayerErr;
				if(tgtSpikeCount  == NULL)	slayerio::cout << "Target Spike Count not defined. Set tgtSpikeCount.true and tgtSpikeCount.false fields."    << slayerErr;
				config["training"]["error"]["tgtSpikeRegion"]["start"] = mex::getScalar(tgtSpikeRegion, "start");
				config["training"]["error"]["tgtSpikeRegion"]["stop"]  = mex::getScalar(tgtSpikeRegion, "stop");
				config["training"]["error"]["tgtSpikeCount"]["true"]   = mex::getScalar(tgtSpikeCount , "true");
				config["training"]["error"]["tgtSpikeCount"]["false"]  = mex::getScalar(tgtSpikeCount , "false");
			}
			
			config["training"]["stopif"]["maxIter"] = static_cast<unsigned>(mex::getScalar(mxField, "maxIter"));	   		
			config["training"]["stopif"]["minCost"] = mex::getScalar(mxField, "minCost");
	   		
			config["training"]["path"]["out"]     = mex::getString(mxField, "outPath");						   		
			config["training"]["path"]["in"]      = mex::getString(mxField, "inPath");						   		
			config["training"]["path"]["desired"] = mex::getString(mxField, "desiredPath");					   		
			config["training"]["path"]["train"]   = mex::getString(mxField, "trainList");   				   	   		
			config["training"]["path"]["test"]    = mex::getString(mxField, "testList");   				   	   		
			
			config["training"]["optimizer"]["name"]    = mex::getString(mxField, "updateMode");							
			config["training"]["optimizer"]["beta1"]   = mex::getScalar(mxField, "beta1",   defaultConfig["training"]["optimizer"]["beta1"].as<double>());	   		
			config["training"]["optimizer"]["beta2"]   = mex::getScalar(mxField, "beta2",   defaultConfig["training"]["optimizer"]["beta2"].as<double>());  		
			config["training"]["optimizer"]["epsilon"] = mex::getScalar(mxField, "epsilon", defaultConfig["training"]["optimizer"]["epsilon"].as<double>()); 	
			
			config["training"]["gradAlgo"]    = mex::getString(mxField, "gradAlgo", defaultConfig["training"]["gradAlgo"].as<std::string>().c_str());		

			config["preferredGPU"] = mex::getFVector(mxField, "preferredGPU", defaultConfig["preferredGPU"].as<std::vector<float>>());

			// extract architecture information from string
			
			mxArray *mxNNarch = mxGetField(mxField, 0, "nnArch");
			if(mxNNarch ==  nullptr)	slayerio::error("nnArch not found.\n");
			std::vector<float> theta  = mex::getFVector(mxField, "theta");	
			std::vector<float> sigma  = mex::getFVector(mxField, "sigma");	
			std::vector<float> wScale = mex::getFVector(mxField, "wScale");	
			
			char strbuf[MAX_STRING_BUFFER_SIZE];
			char *nnArchStr = mxArrayToString(mxNNarch);
			std::vector<std::string> arch;
			// count the number of separator characters in the nnarch description
			unsigned ind = 0;
			unsigned i=0;
			while(nnArchStr[ind] != '\0')
			{
				if(nnArchStr[ind] == '-')	
				{
					strbuf[i] = '\0';
					arch.push_back(strbuf);
					i = 0;
				}
				else
				{
					strbuf[i++] = nnArchStr[ind];
				}
				ind++;
			}
			strbuf[i] = '\0';
			arch.push_back(strbuf);
			
			unsigned nLayers = arch.size();
			
			if( theta.size()!= nLayers ||
				sigma.size()!= nLayers ||
			   wScale.size()!= nLayers )
				slayerio::error("Error: The architecture parameters do not match.\nlength(theta) = %d\nlength(sigma) = %d\nlength(wScale) = %d\nExpected %d", theta.size(), sigma.size(), wScale.size(), nLayers);
			
			for(unsigned l=0; l<nLayers; ++l)
			{
				config["layer"][l]["dim"]    = arch[l];
				config["layer"][l]["wScale"] = wScale[l];
				config["layer"][l]["sigma"]  = sigma[l];
			}
			
			config["neuron"]["theta"] = theta[0];
			config["profile"] = static_cast<bool>(mex::getScalar(mxField, "profile", defaultConfig["profile"].as<bool>()));	
		}
		else
		{
			// TODO
		}
		
		std::vector<unsigned> gpuList;
		if(config["preferredGPU"].IsSequence())		gpuList = config["preferredGPU"].as<std::vector<unsigned>>();
		else										gpuList.push_back(config["preferredGPU"].as<unsigned>());
		config["preferredGPU"] = getAvailableGPUs(gpuList);	
	}
#endif

tgtPattern slayerConfig::getPatternType()
{
	std::string pattern = config["training"]["error"]["type"].as<std::string>();
	if     (pattern.compare("SpikeTime")     == 0)		return SpikeTime;
	else if(pattern.compare("AppxSpikeTime") == 0)		return AppxSpikeTime;
	else if(pattern.compare("NumSpikes")     == 0)		return NumSpikes;
	else if(pattern.compare("ProbSpikes")    == 0)		return ProbSpikes;
	else	slayerio::cout << "Error: " << pattern << " is not a valid pattern type\n" << slayerErr;
}

updateModeType slayerConfig::getUpdateMode()
{
	std::string updateMode = config["training"]["optimizer"]["name"].as<std::string>();
	if     (updateMode.compare("GradientDescent") == 0)		return GradientDescent;
	else if(updateMode.compare("Rmsprop") 	 	  == 0)		return Rmsprop;
	else if(updateMode.compare("ADAM") 	 	      == 0)		return ADAM;
	else if(updateMode.compare("NADAM") 	 	  == 0)		return NADAM;
	else 	slayerio::cout << "Error: " << updateMode << " is not a valid update mode\n" << slayerErr;
}


gradAlgoType slayerConfig::getGradAlgo()
{
	std::string gradAlgo = config["training"]["gradAlgo"].as<std::string>();
	if     (gradAlgo.compare("SLAYER") 		      == 0)		return SLAYER;
	else if(gradAlgo.compare("SLAYER_ZERO")       == 0)		return SLAYER_ZERO;
	else if(gradAlgo.compare("SLAYER_REFRACTORY") == 0)		return SLAYER_REFRACTORY;
	else	slayerio::cout << "Error: " << gradAlgo << " is not a valid gradient algorithm\n" << slayerErr;
}

srkType slayerConfig::getNeuronType()
{
	std::string neuronType = config["neuron"]["type"].as<std::string>();
	if     (neuronType.compare("SRMALPHA")  == 0)	return SRMALPHA;
	else if(neuronType.compare("LIF") 	 	== 0)	return LIF;
	else if(neuronType.compare("TNLIF") 	== 0)	return TNLIF;
	else 	slayerio::cout << "Error: " << neuronType << " is not a valid neuron type\n" << slayerErr;
}

YAML::Node slayerConfig::get()
{
	return config;
}

std::vector<slayerType> slayerConfig::getNetworkInfo()
{
	return getNetworkInfo(config);
}

std::vector<slayerType> slayerConfig::getNetworkInfo(YAML::Node snnConfig)
{
	std::vector<slayerType> layerDesc;
	unsigned param1, param2;
	unsigned nDelayedSyn = 1;
	auto const& layer = snnConfig["layer"];
	layerDesc.resize(layer.size());
	
	// slayerio::cout << "Parsing Architecture ..." << std::endl;
	
	for(int l=0; l<layer.size(); ++l)
	{
		auto dimStr = layer[l]["dim"].as<std::string>();
		// slayerio::cout << "Layer " << l << " " << dimStr << std::endl;
		if(dimStr.find('a')!= std::string::npos)	// aggregation layerDesc
		{
			if(l==0)	slayerio::cout << "Error: First layerDesc cannot be aggregation type." << slayerErr;
			sscanf(dimStr.c_str(), "%ua", &param1);
			layerDesc[l].type = Aggregation;
			layerDesc[l].parameter.push_back(param1);
			layerDesc[l].dimension[0] = layerDesc[l-1].dimension[0]/param1;
			layerDesc[l].dimension[1] = layerDesc[l-1].dimension[1]/param1;
			layerDesc[l].dimension[2] = layerDesc[l-1].dimension[2];
			layerDesc[l].nNeurons = 1;
			for(unsigned j=0; j<3; j++)	layerDesc[l].nNeurons *= layerDesc[l].dimension[j];
		}
		else if(dimStr.find('c')!= std::string::npos)	// convolution layerDesc	
		{
			if(l==0)	slayerio::cout << "Error: First layerDesc cannot be convolution type." << slayerErr;
			char padding = 0;
			// sscanf(architecture[i].c_str(), "%uc%u", &param1, &param2);
			sscanf(dimStr.c_str(), "%uc%u%c", &param1, &param2, &padding);
			layerDesc[l].type = Convolution;
			layerDesc[l].parameter.push_back(param1);
			layerDesc[l].parameter.push_back(param2);
			// assuming stride of 1 and no zero-padding
			// need to change accordingly to include them
			#if defined CUSTOM_PROPAGATION
				if(padding == 'z')	slayerio::cout << "Zero padding is not supported in convolution layer.\n" << slayerErr;
				
				layerDesc[l].dimension[0] = layerDesc[l-1].dimension[0] - param2 + 1;
				layerDesc[l].dimension[1] = layerDesc[l-1].dimension[1] - param2 + 1;
				layerDesc[l].dimension[2] = param1;
			#else
				if(padding == 'z')
				{
					layerDesc[l].dimension[0] = layerDesc[l-1].dimension[0];
					layerDesc[l].dimension[1] = layerDesc[l-1].dimension[1];
					layerDesc[l].dimension[2] = param1;
				}
				else
				{
					layerDesc[l].dimension[0] = layerDesc[l-1].dimension[0] - param2 + 1;
					layerDesc[l].dimension[1] = layerDesc[l-1].dimension[1] - param2 + 1;
					layerDesc[l].dimension[2] = param1;
				}
			#endif
			layerDesc[l].nNeurons = 1;
			for(unsigned j=0; j<3; j++)	layerDesc[l].nNeurons *= layerDesc[l].dimension[j];
		}
		else if(dimStr.find('o')!= std::string::npos)	// output layerDesc	
		{
			sscanf(dimStr.c_str(), "%uo", &param1);
			layerDesc[l].type = Output;
			layerDesc[l].parameter.push_back(param1);
			layerDesc[l].dimension[0] = param1;
			layerDesc[l].dimension[1] = 1;
			layerDesc[l].dimension[2] = 1;
			layerDesc[l].nNeurons = param1;
		}
		else if(dimStr.find('x')!= std::string::npos)	// planar layerDesc	
		{
			int param3 = 1;
			sscanf(dimStr.c_str(), "%ux%ux%u", &param1, &param2, &param3);
			// mexPrintf("\nnum = %d\n%d x %d x %d\n", num, param1, param2, param3);
			// sscanf(architecture[i].c_str(), "%ux%u", &param1, &param2);
			layerDesc[l].type = Planar;
			layerDesc[l].parameter.push_back(param1);
			layerDesc[l].parameter.push_back(param2);
			layerDesc[l].dimension[0] = param1;
			layerDesc[l].dimension[1] = param2;
			layerDesc[l].dimension[2] = param3;	// 1;
			layerDesc[l].nNeurons = param1*param2*param3;
			if(param3 == 0 || param3 > 2)	slayerio::cout << "Error: third dimension of planar layer can either be 1(unipolar) or 2(bipolar)"; // << slayerErr;
			if(param2 == 1)	layerDesc[l].type = Linear;	// for linear layer with bipolar spikes, it assumes the description is of the type 20x1x2 => 20 neurons with bipolar input spikes 
		}
		else if(l==(layer.size()-1))	// last layerDesc is output layerDesc anyway
		{
			sscanf(dimStr.c_str(), "%u", &param1);
			layerDesc[l].type = Output;
			layerDesc[l].parameter.push_back(param1);
			layerDesc[l].dimension[0] = param1;
			layerDesc[l].dimension[1] = 1;
			layerDesc[l].dimension[2] = 1;
			layerDesc[l].nNeurons = param1;
		}
		else // linear layerDesc	
		{
			sscanf(dimStr.c_str(), "%u", &param1);
			layerDesc[l].type = Linear;
			layerDesc[l].parameter.push_back(param1);
			layerDesc[l].dimension[0] = param1;
			layerDesc[l].dimension[1] = 1;
			layerDesc[l].dimension[2] = 1;
			layerDesc[l].nNeurons = param1;
		}
	}
	
	// slayerio::cout << "Parsing Architecture complete" << std::endl;
	
	for(unsigned l=0; l<layerDesc.size(); ++l)
	{
		if(layerDesc[l].type == Output)
		{
			layerDesc[l].nWeights    = 0;	// no weights and delays
			layerDesc[l].nDelays 	 = 0;
			layerDesc[l].nDelayedSyn = nDelayedSyn;
		}
		else if(layerDesc[l+1].type == Linear || 
				layerDesc[l+1].type == Planar || 
				layerDesc[l+1].type == Output)	
		{
			layerDesc[l].nWeights    = layerDesc[l].nNeurons * layerDesc[l+1].nNeurons;
			layerDesc[l].nDelays     = layerDesc[l].nNeurons;
			layerDesc[l].nDelayedSyn = nDelayedSyn;
		}
		else if(layerDesc[l+1].type == Convolution)
		{
			layerDesc[l].nWeights = layerDesc[l+1].parameter[0] * 
									layerDesc[l+1].parameter[1] * 
									layerDesc[l+1].parameter[1] * 
									layerDesc[l]  .dimension[2];
			layerDesc[l].nDelays  = layerDesc[l].nNeurons;
			layerDesc[l].nDelayedSyn = 1;
			// slayerio::print("\nWARNING!!! Multiple delayed synaptic connection is not used in convolution and aggregation layer.\n");
		}
		else if(layerDesc[l+1].type == Aggregation)
		{
			// layerDesc[l].nWeights = layerDesc[l].dimension[2] * layerDesc[l+1].parameter[0] * layerDesc[l+1].parameter[0];
			// but all these weights are constant, equal to 1.2*theta
			layerDesc[l].nWeights = 1;
			layerDesc[l].nDelays  = layerDesc[l].nNeurons;
			layerDesc[l].nDelayedSyn = 1;
		}
	}
	
	return layerDesc;
}

void slayerConfig::display()
{
	display(config);
}

void slayerConfig::display(YAML::Node snnConfig, bool displayArchDetails)
{
	unsigned width = 10;
	if(snnConfig["seed"])	slayerio::cout << "Random Seed  = " << snnConfig["seed"].as<unsigned>() << std::endl;
	slayerio::cout << "Simulation Parameters: \n";
	auto const& simulation = snnConfig["simulation"];
	slayerio::print("  %-*s = %gms\n", width, "Ts"     , simulation["Ts"]     .as<double>());
	slayerio::print("  %-*s = %gms\n", width, "tSample", simulation["tSample"].as<double>());
	slayerio::print("  %-*s = %g\n"  , width, "nSample", simulation["nSample"].as<double>());
	
	slayerio::cout << "Neuron: \n";
	auto const& neuron = snnConfig["neuron"];
	slayerio::print("  %-*s = %s\n",         width, "Type"     , neuron["type"]    .as<std::string>().c_str());
	slayerio::print("  %-*s = %g\n",         width, "tauSr"    , neuron["tauSr"]   .as<double>());
	slayerio::print("  %-*s = %g\n",         width, "tauRef"   , neuron["tauRef"]  .as<double>());
	slayerio::print("  %-*s = %g\n",         width, "theta"    , neuron["theta"]   .as<double>());
	slayerio::print("  %-*s = %g x theta\n", width, "scaleRef" , neuron["scaleRef"].as<double>());
	slayerio::print("  %-*s = %g x theta\n", width, "tauRho"   , neuron["tauRho"]  .as<double>());
	slayerio::print("  %-*s = %g\n",         width, "scaleRho" , neuron["scaleRho"].as<double>());
	
	slayerio::cout << "Architecture\n";
	auto const& layer = snnConfig["layer"];
	if(displayArchDetails)
	{
		auto layerDesc = getNetworkInfo(snnConfig);
		for(int l=0; l<layer.size(); ++l)
		{
			if(layer[l]["sigma"].as<int>() == 0)
				slayerio::print("  layer %02d: %-82s wScale: %5g\n", l,
							   (layerDesc[l].description()+",").c_str(),
								layer[l]["wScale"].as<double>());
				
			else
				slayerio::print("  layer %02d: %-81s  wScale: %5g, sigma(NOT USED): %5g\n", l,
							   (layerDesc[l].description()+",").c_str(),
								layer[l]["wScale"].as<double>(),
								layer[l]["sigma"].as<double>());
		}
	}
	else
	{
		for(int l=0; l<layer.size(); ++l)
		{
			slayerio::print("  layer %02d: %10s, wScale: %5g, sigma: %5g\n", l,
							layer[l]["dim"].as<std::string>().c_str(),
							layer[l]["wScale"].as<double>(),
							layer[l]["sigma"].as<double>());
		}
	}
	
	slayerio::cout << "Training parameters:\n";
	slayerio::print("  %-*s = %s\n", width, "Grad Algo" , snnConfig["training"]["gradAlgo"].as<std::string>().c_str());
	auto const& error = snnConfig["training"]["error"];
	slayerio::print("  %-*s = %s\n", width, "error" , error["type"].as<std::string>().c_str());
	auto const& optimizer = snnConfig["training"]["optimizer"];
	slayerio::print("  %-*s = %s\n", width, "optimizer" , optimizer["name"].as<std::string>().c_str());
	slayerio::print("  %-*s = %s\n", width, "Testing" , snnConfig["training"]["testing"].as<std::string>().c_str());
	slayerio::print("  %-*s = %s\n", width, "Classify", snnConfig["training"]["classification"].as<std::string>().c_str());
	auto const& eta = snnConfig["training"]["learning"];
	slayerio::print("  %-*s = %g\n", width, "etaW"  , eta["etaW"]  .as<double>());
	slayerio::print("  %-*s = %g\n", width, "etaD"  , eta["etaD"]  .as<double>());
	slayerio::print("  %-*s = %g\n", width, "lambda", eta["lambda"].as<double>());
	if(error["type"].as<std::string>().compare("NumSpikes") == 0)
	{
		if(!error["tgtSpikeRegion"])	slayerio::cout << "Target Spike Region not defined. Check training::error::tgtSpikeRegion in yaml config file." << slayerErr;
		if(!error["tgtSpikeCount"])		slayerio::cout << "Target Spike Count not defined. Check training::error::tgtSpikeCount in yaml config file."   << slayerErr;
		slayerio::print("  %-*s = desired time   {start:%gms, stop:%gms}\n", width, "region", error["tgtSpikeRegion"]["start"].as<double>(), error["tgtSpikeRegion"]["stop" ].as<double>());
		slayerio::print("  %-*s = desired spikes {true :%d, false:%d}\n"   , width, "count" , error["tgtSpikeCount" ]["true" ].as<int>()   , error["tgtSpikeCount" ]["false"].as<int>());
	}
	if(error["probSlidingWin"] && error["type"].as<std::string>().compare("ProbSpikes") == 0)
		slayerio::print("  %-*s = %g x Ts\n", width, "Prob Win" , error["probSlidingWin"].as<double>());
	if(optimizer["name"].as<std::string>().compare("ADAM")  == 0 || 
	   optimizer["name"].as<std::string>().compare("NADAM") == 0)
	{
		if(optimizer["beta1"])		slayerio::print("  %-*s = %g\n", width, "beta1"   , optimizer["beta1"]  .as<double>());
		if(optimizer["beta2"])		slayerio::print("  %-*s = %g\n", width, "beta2"   , optimizer["beta2"]  .as<double>());
		if(optimizer["epsilon"])	slayerio::print("  %-*s = %g\n", width, "epsilon" , optimizer["epsilon"].as<double>());
	}
	auto const& stopif = snnConfig["training"]["stopif"];
	slayerio::print("  %-*s = %g\n", width, "maxIter", stopif["maxIter"].as<double>());
	slayerio::print("  %-*s = %g\n", width, "minCost", stopif["minCost"].as<double>());
	
	slayerio::cout << "Paths :\n";
	auto const& path = snnConfig["training"]["path"];
	slayerio::print("  %-*s = %s\n", width, "Out"     , path["out"     ].as<std::string>().c_str());
	slayerio::print("  %-*s = %s\n", width, "In"      , path["in"      ].as<std::string>().c_str());
	slayerio::print("  %-*s = %s\n", width, "Desired" , path["desired" ].as<std::string>().c_str());
	slayerio::print("  %-*s = %s\n", width, "train"   , path["train"   ].as<std::string>().c_str());
	slayerio::print("  %-*s = %s\n", width, "test"    , path["test"    ].as<std::string>().c_str());
	
	auto const& gpus = snnConfig["preferredGPU"];
	slayerio::print("%-*s = ", width+2, "GPU(s)");
	if(gpus.IsSequence())	
	{
		std::cout << "[";
		for(auto gpuID : gpus.as<std::vector<unsigned>>())	slayerio::cout << gpuID << ", ";
		slayerio::cout << "\b\b]" << std::endl;
	}
	else	slayerio::cout << gpus.as<unsigned>() << std::endl;
	
	if(snnConfig["profile"])	slayerio::print("%-*s = %s\n", width+2, "Profile", snnConfig["profile"].as<std::string>().c_str());
	
	slayerio::print("\n");
}
	
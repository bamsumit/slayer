#include <poolLayerKernels.h>
#include <cudaKernels.h>
#include <slayerio.h>

/***** Signal Forward Propagation *****/

__global__ void multWaAggrKernel(float* d_u, const float* d_W, const float* d_a, dim3 uDim, dim3 aDim, unsigned poolWidth, unsigned numU, unsigned Ns)
{
	// calcualte the threadID
	unsigned tID = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned nID = blockIdx.y * blockDim.y + threadIdx.y;
	
	if(tID >= Ns)	return;
	if(nID >= numU)	return;
	
	unsigned xU = nID % uDim.x;
	unsigned yU =(nID / uDim.x)% uDim.y;
	unsigned zU = nID / uDim.x / uDim.y;
	
	float result = 0.0f;
	float weight = *d_W;
	unsigned zA = zU;
	for(unsigned dy = 0; dy<poolWidth; ++dy)
	{
		unsigned yA = dy + poolWidth*yU;
		for(unsigned dx = 0; dx<poolWidth; ++dx)
		{
			unsigned xA = dx + poolWidth*xU;
			
			unsigned idA = xA + aDim.x * (yA + aDim.y * zA);
			if(xA < aDim.x && yA < aDim.y && zA < aDim.z)	result += d_a[tID + idA * Ns];
		}
	}
	
	d_u[tID + nID * Ns] = result * weight;
}

void multWaAggr(float* d_u, const float* d_W, const float* d_a, slayerType uInfo, slayerType aInfo, unsigned Ns)
{
	dim3 uDim, aDim;
	aDim.x = aInfo.dim(0), 	aDim.y = aInfo.dim(1), 	aDim.z = aInfo.dim(2);
	uDim.x = uInfo.dim(0), 	uDim.y = uInfo.dim(1), 	uDim.z = uInfo.dim(2);
	
	unsigned numU = uDim.x * uDim.y * uDim.z;
	
	dim3 thread, block;
	thread.x = 128;
	thread.y = 8;
	block.x = ceil(1.0f * Ns  / thread.x);
	block.y = ceil(1.0f * numU/ thread.y);	
	if(block.y >= 65535)	slayerio::error("Error: maximum blockDim.y exceeded at poolLayerKernels.h/multWaAggr.");
	if(block.z >= 65535)	slayerio::error("Error: maximum blockDim.z exceeded at poolLayerKernels.h/multWaAggr.");
	
	// unsigned poolWidth = uInfo.par(0);
	// mexPrintf("\nW = %g\npoolWidth = %d", *W, poolWidth);
	
	multWaAggrKernel<<< block, thread >>>(d_u, d_W, d_a, uDim, aDim, uInfo.par(0), numU, Ns);
}

/***** Signal Backpropagation *****/

__global__ void multWdeltaAggrKernel(float* d_e, const float* d_W, const float* d_delta, dim3 eDim, dim3 dDim, unsigned poolWidth, unsigned numE, unsigned Ns)
{
	// calcualte the threadID
	unsigned tID = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned nID = blockIdx.y * blockDim.y + threadIdx.y;
	
	if(tID >= Ns)	return;
	if(nID >= numE)	return;
	
	unsigned xE = nID % eDim.x;
	unsigned yE =(nID / eDim.x)% eDim.y;
	unsigned zE = nID / eDim.x / eDim.y;
	
	unsigned xD = xE / poolWidth;
	unsigned yD = yE / poolWidth;
	unsigned zD = zE;
	
	float weight = *d_W;
	unsigned idD = xD + dDim.x * (yD + dDim.y * zD);
	
	if(xD < dDim.x && yD < dDim.y && zD < dDim.z)	d_e[tID + nID * Ns] = weight * d_delta[tID + idD * Ns];
}

void multWdeltaAggr(float* d_e, const float* d_W, const float* d_delta, slayerType eInfo, slayerType dInfo, unsigned Ns)
{
	dim3 dDim, eDim, wDim;
	eDim.x = eInfo.dim(0), 	eDim.y = eInfo.dim(1), 	eDim.z = eInfo.dim(2);
	dDim.x = dInfo.dim(0), 	dDim.y = dInfo.dim(1), 	dDim.z = dInfo.dim(2);
	
	unsigned numE = eInfo.dim(0) * eInfo.dim(1) * eInfo.dim(2);
	
	dim3 thread, block;
	thread.x = 128;
	thread.y = 8;
	block.x = ceil(1.0f * Ns  / thread.x);
	block.y = ceil(1.0f * numE/ thread.y);	
	if(block.y >= 65535)	slayerio::error("Error: maximum blockDim.y exceeded at poolLayerKernels.h/multWdeltaConv.");
	if(block.z >= 65535)	slayerio::error("Error: maximum blockDim.z exceeded at poolLayerKernels.h/multWdeltaConv.");
	
	// unsigned poolWidth = dInfo.par(0);
	// mexPrintf("\nW = %g\npoolWidth = %d", *d_W, poolWidth);
	
	multWdeltaAggrKernel<<< block, thread >>>(d_e, d_W, d_delta, eDim, dDim, dInfo.par(0), numE, Ns);
}
#include <optimizerKernels.h>
#include <slayerio.h>

__global__ void gradientDescentKernel(float* __restrict__ param, float* __restrict__ gradient, const float eta, const float lambda, const unsigned numel)
{
	unsigned id = blockIdx.x * blockDim.x + threadIdx.x;
	
	if(id >= numel)	return;
	
	gradient[id] += lambda * param[id]; // L2 regularization
	
	param[id] -= eta * gradient[id];	// update
	gradient[id] = 0;					// reset
}

__global__ void rmsPropKernel(float* __restrict__ d_param, float* __restrict__ d_grad, float* d_meansquare, const float eta, const float lambda, const unsigned numel)
{
	unsigned id = blockIdx.x * blockDim.x + threadIdx.x;
	
	if(id >= numel)	return;
	
	d_grad[id]      += lambda * d_param[id]; // L2 regularization
	
	d_meansquare[id] = 0.9*d_meansquare[id] + 0.1*d_grad[id]*d_grad[id];
	d_grad[id]      /= sqrt(d_meansquare[id]) + 1e-6; 
	d_param[id]     -= eta * d_grad[id];	// update
	d_grad[id]       = 0;					// reset 
	
	return;
}

__global__ void adamKernel(float* __restrict__ d_param, float* __restrict__ d_grad, float* d_meansquare, float* d_momentum, 
						   const float eta, const float lambda, const float beta1, const float beta2, const float epsilon, const unsigned iter, const unsigned numel)
{
	unsigned id = blockIdx.x * blockDim.x + threadIdx.x;
	
	if(id >= numel)	return;
	
	d_grad[id]      += lambda * d_param[id]; // L2 regularization
	
	d_momentum[id]   = beta1*d_momentum[id]   + (1-beta1)*d_grad[id];
	d_meansquare[id] = beta2*d_meansquare[id] + (1-beta2)*d_grad[id]*d_grad[id];
	float mhat       = d_momentum[id]   / (1- __powf(beta1, iter + 1)); // to avoid indexing from 0 which will yield inf values
	float vhat       = d_meansquare[id] / (1- __powf(beta2, iter + 1)); // to avoid indexing from 0 which will yield inf values
	
	d_param[id]     -= eta * mhat / (sqrt(vhat) + epsilon);	// update
	d_grad[id]       = 0.0f;					// reset 
	
	return;
}

__global__ void nadamKernel(float* __restrict__ d_param, float* __restrict__ d_grad, float* __restrict__ d_meansquare, float* __restrict__ d_momentum, 
							const float eta, const float lambda, const float beta1, const float beta2, const float epsilon, const unsigned iter, const unsigned numel)
{
	unsigned id = blockIdx.x * blockDim.x + threadIdx.x;
	
	if(id >= numel)	return;
	
	d_grad[id]      += lambda * d_param[id]; // L2 regularization
	
	d_momentum[id]   = beta1*d_momentum[id]   + (1-beta1)*d_grad[id];
	d_meansquare[id] = beta2*d_meansquare[id] + (1-beta2)*d_grad[id]*d_grad[id];
	float mhat       = d_momentum[id]   / (1- __powf(beta1, iter + 1)); // to avoid indexing from 0 which will yield inf values
	float vhat       = d_meansquare[id] / (1- __powf(beta2, iter + 1)); // to avoid indexing from 0 which will yield inf values
	
	d_param[id]     -= eta * ( beta1*mhat + (1-beta1)/(1- __powf(beta1, iter + 1)) * d_grad[id] ) / (sqrt(vhat) + epsilon);	// update
	d_grad[id]       = 0.0f;					// reset 
	
	return;
}

__global__ void delayLimitKernel(float* delay, const unsigned numel)
{
	unsigned id = blockIdx.x * blockDim.x + threadIdx.x;
	
	if(id >= numel)	return;
	
	if(delay[id] < 0)	delay[id] = 0;
	
	return;
}
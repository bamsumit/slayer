#include <sampleScheduler.h>

sampleScheduler::sampleScheduler()
{}

int sampleScheduler::initialize(std::string filename, unsigned numSample)
{
	samplesPerBatch = numSample;
	// mexPrintf("Opening %s\n", filename.c_str());
	fin.open(filename.c_str(), std::ios::in);
	if(fin.is_open())	return  0;
	else 				return -1;
}

sampleScheduler::~sampleScheduler()
{
	fin.close();
}

// int sampleScheduler::loadSamples(std::vector<unsigned> &inID, std::vector<unsigned> &classID)
// {
	// unsigned eofDetected = 0;
	// if(   inID.size() != samplesPerBatch)	   inID.resize(samplesPerBatch);
	// if(classID.size() != samplesPerBatch)	classID.resize(samplesPerBatch);
	// // mexPrintf("nSample: %d\n", samplesPerBatch);
	// for(unsigned i=0; i<samplesPerBatch; ++i)
	// {
		// fin >> inID[i] >> classID[i];
		// if(fin.eof())
		// {
			// // mexPrintf("\b EOF detected, Reading again... ");
			// fin.clear();
			// fin.seekg(0, std::ios::beg);
			// fin >> inID[i] >> classID[i];
			// eofDetected = 1;
		// }
		// // mexPrintf("%d %d\n", inID[1], classID[i]);
	// }
	// if(eofDetected)	fin.seekg(0, std::ios::beg);
	// return 1-eofDetected;
// }

int sampleScheduler::loadSamples(std::vector<unsigned> &inID, std::vector<unsigned> &classID)
{	// better than the above commented code.
	// ignores comments starting with #
	unsigned eofDetected = 0;
	std::string line;
	if(   inID.size() != samplesPerBatch)	   inID.resize(samplesPerBatch);
	if(classID.size() != samplesPerBatch)	classID.resize(samplesPerBatch);
	// mexPrintf("nSample: %d\n", samplesPerBatch);
	for(unsigned i=0; i<samplesPerBatch; ++i)
	{
		std::getline(fin, line);
		while(	fin.eof() || 
				line.c_str()[0] == '#' || 
				line.size() == 0)
		{
			if(fin.eof())	
			{
				eofDetected = 1;
				fin.clear();
				fin.seekg(0, std::ios::beg);
			}
			std::getline(fin, line);
		}
		std::stringstream sin(line);
		sin >> inID[i] >> classID[i];
		
		// std::cout << line;
		// mexPrintf("%d %d\n", inID[1], classID[i]);
	}
	if(eofDetected)	fin.seekg(0, std::ios::beg);
	return 1-eofDetected;
}

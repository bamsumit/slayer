#include <classification.h>
#include <spikeio.h>
#include <slayerio.h>

unsigned classificationEngine::getNumSpikes(std::string desiredPath, unsigned classID)
{
	std::stringstream sout;
	sout << desiredPath << classID << ".bsn";
	std::vector<unsigned> neuronID;
	std::vector<unsigned> numSpikes;
	std::vector<float> tSt;
	std::vector<float> tEn;
	if(read1DNumSpikes(sout.str(), neuronID, tSt, tEn, numSpikes) == 0)
	{
		slayerio::error("In classification.h/getNumSpikes()\nError Reading Num Spike Files \n Aborting!!!");
	}
	
	unsigned maxNumSpikes = numSpikes[0];
	unsigned minNumSpikes = numSpikes[0];
	for(unsigned i=1; i<numSpikes.size(); ++i)	
	{
		maxNumSpikes = (numSpikes[i] > maxNumSpikes) ? numSpikes[i] : maxNumSpikes;
		minNumSpikes = (numSpikes[i] < minNumSpikes) ? numSpikes[i] : minNumSpikes;
	}
	return (maxNumSpikes + minNumSpikes)/2;
}

std::vector< std::vector<unsigned> > classificationEngine::getNumSpikes(const slayer &outputLayer)
{
	std::vector< std::vector<unsigned> > numSpikes(outputLayer.info.nNeurons);
	for(unsigned id=0; id<numSpikes.size(); ++id)
	{
		unsigned i=0;
		numSpikes[id].push_back(0);
		for(unsigned tid=0; tid<outputLayer.Ns; ++tid)
		{
			if(outputLayer.t[tid] > (i+1)*tSample)
			{
				i++;
				numSpikes[id].push_back(0);
			}
			// numSpikes[id][i] += outputLayer.s[id][tid] > 0.001;
			numSpikes[id][i] += outputLayer.s[id * outputLayer.Ns + tid] > 0.001;
		}
	}
	// for(unsigned i=0; i<numSpikes.size(); i++)
	// {
		// for(unsigned j=0; j<numSpikes[i].size(); j++)
		// {
			// mexPrintf("%d  ", numSpikes[i][j]);
		// }
		// mexPrintf("\n");
	// }
	
	return numSpikes;
}

void classificationEngine::initialize(std::string desiredPath, unsigned numOutputs, tgtPattern pattern, float Tsample)
{
	patternType = pattern;
	tSample = Tsample;
	unsigned numClasses = (numOutputs == 1)? 2: numOutputs;
	idealClass.resize(numClasses);
	threshold = 0;
	for(unsigned cID=0; cID < numClasses; ++cID)
	{
		//idealClass = slayer(layerDesc, 0, 0, t, )
		switch(patternType)
		{
			case SpikeTime:	 	
			case AppxSpikeTime: /*idealClass[cID].loadSpikes(desiredPath, ??, tSample);*/	break;
			case NumSpikes: 	threshold = getNumSpikes(desiredPath, cID);
								break;
			case AvgSpikes: 	break;
			case ProbSpikes:	break;
			default:			slayerio::error("In classification.h/initialize\nDesired spike pattern type specification undefined. \nCheck patternType variable.\nAborting process now.");
		}
	}
	// mexPrintf("\nThreshold = %d\n", threshold);
}

void classificationEngine::initialize(YAML::Node error, unsigned numOutputs, tgtPattern pattern, float Tsample)
{
	patternType = pattern;
	tSample = Tsample;
	unsigned numClasses = (numOutputs == 1)? 2: numOutputs;
	idealClass.resize(numClasses);
	threshold = 0;
	for(unsigned cID=0; cID < numClasses; ++cID)
	{
		//idealClass = slayer(layerDesc, 0, 0, t, )
		switch(patternType)
		{
			case SpikeTime:	 	
			case AppxSpikeTime: /*idealClass[cID].loadSpikes(desiredPath, ??, tSample);*/	break;
			case NumSpikes: 	threshold = ( error["tgtSpikeCount"]["true"].as<unsigned>() + error["tgtSpikeCount"]["false"].as<unsigned>() ) / 2;
								break;
			case AvgSpikes: 	break;
			case ProbSpikes:	break;
			default:			slayerio::error("In classification.h/initialize\nDesired spike pattern type specification undefined. \nCheck patternType variable.\nAborting process now.");
		}
	}
	// mexPrintf("\nThreshold = %d\n", threshold);
}

std::vector<unsigned> classificationEngine::outputClass(const slayer &outputLayer, const std::vector<unsigned> &groundTruth)
{
	std::vector< std::vector<unsigned> > numSpikes = getNumSpikes(outputLayer);
	std::vector<unsigned> classID(numSpikes[0].size());
	
	switch(patternType)
	{
		case SpikeTime:	 	
		case AppxSpikeTime:
			slayerio::error("ERROR: Not implemented classicication Engine for SpikeTime and AppxSpikeTime spike patterns.");
			break;
		case NumSpikes:
			if(numSpikes.size() == 1)
				for(unsigned samples = 0; samples<classID.size(); ++samples)
					classID[samples] = numSpikes[0][samples] >= threshold;
			else
				for(unsigned samples = 0; samples<classID.size(); ++samples)
				{
					classID[samples] = 0;
					for(unsigned i=1; i<numSpikes.size(); ++i)
					{
						if(numSpikes[i][samples] > numSpikes[classID[samples]][samples])	classID[samples] = i;
					}
				}
			break;
		case ProbSpikes:	// more spikes means higher probability
			if(numSpikes.size() == 1)
				slayerio::error("ERROR: ProbSpikes pattern type needs at least two output layer neurons.");
			for(unsigned samples = 0; samples<classID.size(); ++samples)
			{
				classID[samples] = 0;
				for(unsigned i=1; i<numSpikes.size(); ++i)
				{
					if(numSpikes[i][samples] > numSpikes[classID[samples]][samples])	classID[samples] = i;
				}
			}
			break;
	}
	
	return classID;
}

unsigned classificationEngine::numCorrectIn(const std::vector<unsigned> &classID, const std::vector<unsigned> &groundTruth)
{
	unsigned nCorrect = 0;
	for(unsigned i=0; i<classID.size(); ++i)	nCorrect += classID[i] == groundTruth[i];
	
	return nCorrect;
}

unsigned classificationEngine::numCorrectIn(const slayer &outputLayer, const std::vector<unsigned> &groundTruth)
{
	return numCorrectIn(outputClass(outputLayer, groundTruth), groundTruth);
}
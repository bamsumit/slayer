#include <slayerio.h>
#include <stdarg.h>

// namespace slayerio
// {
	// consoleOut cout;
// }

slayerio::consoleOut slayerio::cout;
slayerio::consoleIn  slayerio::cin;

int slayerio::print(const char* format, ...)
{
	char strbuf[MAX_STRING_BUFFER_SIZE];
	va_list args;
	va_start(args, format);
	vsprintf(strbuf, format, args);
	int numchars;
	#if defined mex_h
		numchars = mexPrintf("%s", strbuf);
	#else
		numchars = printf("%s", strbuf);
	#endif
	va_end(args);
	return numchars;
}

int slayerio::scan(const char* format, ...)
{
	// might be buggy for multiple line inputs
	// expects the inputs to be on single line!
	char strbuf[MAX_STRING_BUFFER_SIZE];
	va_list args;
	va_start(args, format);
	#if defined mex_h
		mex::getline(strbuf);
	#else
		std::cin.getline(strbuf, MAX_STRING_BUFFER_SIZE);
	#endif
	int numargs = vsscanf(strbuf, format, args);
	return numargs;
}

int slayerio::error(const char* format, ...)
{
	char strbuf[MAX_STRING_BUFFER_SIZE];
	va_list args;
	va_start(args, format);
	vsprintf(strbuf, format, args);
	#if defined mex_h
		// mexPrintf("Error at file : %s, line : %s\n", file, line);
		mexErrMsgTxt(strbuf);
	#else
		// printf("Error at file : %s, line : %s\n%s", file, line, strbuf);
		printf("%s", strbuf);
		exit(EXIT_CODE);
	#endif
	va_end(args);
	return -1;
}
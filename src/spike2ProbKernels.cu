#include <spike2ProbKernels.h>

__global__ void countSpikesKernel(unsigned* d_nSpikes, const float* d_s, unsigned windowLength, unsigned nNeurons, unsigned Ns)
{
	unsigned timeID = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned nID    = blockIdx.y * blockDim.y + threadIdx.y;
	
	if(timeID >= Ns)	return;
	if(nID >= nNeurons)	return;
	
	unsigned numSamples = (timeID > windowLength) ? windowLength : timeID;	// adjust the sliding window for boundary values
	
	unsigned nSpikes = 0;
	for(unsigned i=0; i<numSamples; ++i)	nSpikes += (d_s[timeID - i + Ns * nID] > 1e-3);		// count the number of spikes in specified window region
	
	d_nSpikes[timeID + Ns * nID] = nSpikes;	// store the running window spike count
}

__global__ void sumSpikesKernel(unsigned* totalSpikes, const unsigned* nSpikes, unsigned nNeurons, unsigned Ns)
{
	unsigned timeID = blockIdx.x * blockDim.x + threadIdx.x;
	
	if(timeID >= Ns)	return;
	
	unsigned sum = 0;
	for(unsigned nID = 0; nID < nNeurons; ++nID)	sum += nSpikes[timeID + Ns * nID];
	totalSpikes[timeID] = sum;
}

__global__ void spikeProbabilityKernel(float* probability, const unsigned* nSpikes, const unsigned* totalSpikes, unsigned nNeurons, unsigned Ns, float offset)
{
	unsigned timeID = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned nID    = blockIdx.y * blockDim.y + threadIdx.y;
	
	if(timeID >= Ns)	return;
	if(nID >= nNeurons)	return;
	probability[timeID + Ns * nID] = ( nSpikes[timeID + Ns * nID] + offset) / ( totalSpikes[timeID] + nNeurons * offset );
	// probability[timeID + Ns * nID] = (totalSpikes[timeID] > 0) ? 1.0f * nSpikes[timeID + Ns * nID] / totalSpikes[timeID] : 1.0f/nNeurons;
	// probability[timeID + Ns * nID] = (totalSpikes[timeID] > 0) ? 1.0f * nSpikes[timeID + Ns * nID] / totalSpikes[timeID] : 0.0f;
}

__global__ void spikeProbErrorKernel(float* e, const float* probability, const unsigned* classID, const unsigned *totalSpikes, unsigned nNeurons, unsigned Ns, float offset)
{
	unsigned timeID = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned nID    = blockIdx.y * blockDim.y + threadIdx.y;
	
	if(timeID >= Ns)	return;
	if(nID >= nNeurons)	return;
	
	float targetProbability = (classID[timeID] == nID);
	e[timeID + Ns * nID] = (totalSpikes[timeID] > 0) ? ( probability[timeID + Ns * nID] -  targetProbability)/ (totalSpikes[timeID] + nNeurons * offset) : 0;
}

__global__ void crossEntropyLossKernel(float* L, const unsigned* classID, const float* probability, unsigned Ns)
{
	unsigned timeID = blockIdx.x * blockDim.x + threadIdx.x;
	
	if(timeID >= Ns)	return;
	
	float p = probability[timeID + classID[timeID] * Ns];
	L[timeID] = -log(p);
}

float crossEntropyLoss(const unsigned* classID, const float* probability, unsigned Ns, float Ts)
{
	float *loss;
	cudaMallocManaged(&loss, sizeof(float) * Ns);
	crossEntropyLossKernel<<< ceil(1.0f * Ns / 128), 128 >>>(loss, classID, probability, Ns);
	cudaDeviceSynchronize();
	
	float totalLoss = sumReduce(loss, Ns) * Ts;
	
	cudaFree(loss);
	
	return totalLoss;
}
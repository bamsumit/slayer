#include <slayerType.h>

slayerType& slayerType::operator = (const slayerType &info)
{
	if(this == &info)	return *this;
	parameter   = info.parameter;
	type        = info.type;
	nNeurons    = info.nNeurons;
	nWeights    = info.nWeights;
	nDelays     = info.nDelays;
	nDelayedSyn = info.nDelayedSyn;
	for(unsigned i=0; i<3; ++i)	dimension[i] = info.dimension[i];
	return *this;
}
	
std::string slayerType::description()
{	// This function returns a string describing the layer
	char strbuf[MAXBUF];
	switch(type)
	{
	case Linear:
		if(dimension[2] == 1)	// only positive spikes
						sprintf(strbuf, "Linear,      %15d neurons, %10d weights", dimension[0], nWeights);
		else			sprintf(strbuf, "Linear,      %9d x %3d neurons, %10d weights (bipolar spikes)", 
																 dimension[0], dimension[2], nWeights);							break;
	case Planar:		
		if(dimension[2] == 1)	// only positive spikes
						sprintf(strbuf, "Planar,      %9d x %3d neurons, %10d weights", dimension[0], dimension[1], nWeights);	
		else			sprintf(strbuf, "Planar,      %3d x %3d x %3d neurons, %10d weights (bipolar spikes)", 
																		dimension[0], dimension[1], dimension[2], nWeights);	break;
	case Convolution:	sprintf(strbuf, "Convolution, %3d x %3d x %3d neurons, %10d weights, %3d x (%2d x %2d) filters",             
								dimension[0], dimension[1], dimension[2], nWeights,                           
								parameter[0], parameter[1], parameter[1]); 							 							break;
	case Aggregation:	sprintf(strbuf, "Aggregation, %3d x %3d x %3d neurons, %10d weights,       (%2d x %2d) filters",                       
								dimension[0], dimension[1], dimension[2], nWeights,                           
								parameter[0], parameter[0]);				 						 							break;
	case Output:		sprintf(strbuf, "Output,      %15d neurons", dimension[0]);												break;
	}
	return std::string(strbuf);
}

std::string slayerType::typeStr()
{
	std::stringstream sout;
	switch(type)
	{
	case Linear:		sout << "Linear";		break;
	case Planar:		sout << "Planar";		break;
	case Convolution:	sout << "Convolution";	break;
	case Aggregation:	sout << "Aggregation";	break;
	case Output:		sout << "Output";		break;
	}
	return sout.str();
}
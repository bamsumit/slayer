#include <slayer.h>
#include <slnetwork.h>
#include <learningStat.h>
#include <slayerio.h>
#include <tictoc.h>
#include <string>
#include <iomanip>
#include <yaml-cpp/yaml.h>

int main(int argc, char *argv[])
{
	// slnetwork snn(slayerConfig(YAML::LoadFile("snn.yaml")));
	slayerio::cout << "SNN Configuration File: " << argv[1] << std::endl;
	
	slnetwork snn(slayerConfig(YAML::LoadFile(argv[1])));
	
	snn.configDisplay();
	
	const unsigned out = static_cast<unsigned>(snn.layer.size()-1);
	
	std::vector<unsigned> inputInd(snn.nSample);		// to store the indices of the input samples for a minibatch
	std::vector<unsigned> classInd(snn.nSample);		// to store the indices of the class values corresponding to input samples for a minibatch
	
	double tocSum = 0;
	double meanElapsedTime;
	
	// std::vector<float> TrCost;
	// std::vector<float> TeCost;
	// std::vector<float> TrAcc;						// Training accuracy
	// std::vector<float> TeAcc;						// Training accuracy
	learningStat TrStat;
	learningStat TeStat;
		
	bool converged = false;
	unsigned lastStrSize = 0;
	
	snn.writeNetworkConfig();
	std::ofstream costout((snn.writeFileName()+"-cost.txt"    ).c_str(), std::ios::out);
	std::ofstream accout ((snn.writeFileName()+"-accuracy.txt").c_str(), std::ios::out);
	
	timeit profiler;
	
	for(unsigned iter=0; iter<=snn.maxIter; ++iter)
	{	
		if(snn.trScheduler.loadSamples(inputInd, classInd))	// if there is sample for the current epoch
		{	// Note:: if there are not enough samples (i.e. less than snn.nSample) to stride in last few samples, they will be excluded
			
			// Forward Propagation
			snn.layer[0].loadSpikes(snn.inPath, inputInd, snn.tSample);
			for(unsigned l=1; l <= out; ++l)	snn.layer[l].fwdProp();
			
			// Accuracy calculation
			TrStat.correctSamples += (snn.classification == true) ? snn.classifier.numCorrectIn(snn.layer[out], classInd) : 0;
			TrStat.numSamples     += classInd.size();
			
			// Back Propagation
			snn.loadDesiredSpikes(classInd);
			TrStat.costSum += snn.layer[out].error(snn.layerDes, snn.patternType);
			for(int l=out-1; l>=0; --l)	snn.layer[l].backProp();
			
			// Update parameters
			snn.updateParams(iter);
			
			// Print learning figures
			tocSum += profiler.record();			
			meanElapsedTime = tocSum / TrStat.numSamples * classInd.size();
			slayerio::clear(lastStrSize);	
			lastStrSize += displayStats(TrStat, TeStat, iter, meanElapsedTime);
			lastStrSize += snn.displayProfileData();
			
			// Early stopping
			if(TrStat.cost() < snn.minCost && TrStat.cost() > 0)	converged = true; 
			if(converged) 	
			{
				slayerio::print("\nLearning Converged.\n");		
				break;
			}
		}
		else
		{
			// TrCost.push_back(TrStat.cost());
			// TrAcc .push_back(TrStat.accuracy());
			costout << std::setw(16) << TrStat.cost();
			if(snn.classification)	accout  << std::setw(16) << TrStat.accuracy();
			// plotAcc(static_cast<float>(correctTrain)/numTrain, TrAcc.size(), 2, "Accuracy", ".b");
			// plotCost(Jtrain/numTrain, TrAcc.size(), 1, "Cost", ".b");
			
			
			if(snn.testing)
			{
				TrStat.updateMinCost();
				TrStat.updateMaxAcc();

				TeStat.reset();
				while(snn.teScheduler.loadSamples(inputInd, classInd))	// while there are samples in the training list
				{	// Note:: if there are not enough samples (i.e. less than snn.nSample) to stride in last few samples, they will be excluded
					// tic();
					
					snn.layer[0].loadSpikes(snn.inPath, inputInd, snn.tSample);
					for(unsigned l=1; l <= out; ++l)	snn.layer[l].fwdProp();
					
					snn.loadDesiredSpikes(classInd);
					TeStat.costSum += snn.layer[out].error(snn.layerDes, snn.patternType);
					
					// Accuracy calculation
					TeStat.correctSamples += (snn.classification == true) ? snn.classifier.numCorrectIn(snn.layer[out], classInd) : 0;
					TeStat.numSamples     += static_cast<unsigned>(classInd.size());
					
					// Print learning figures
					slayerio::clear(lastStrSize);
					lastStrSize += displayStats(TrStat, TeStat, iter, profiler.record());
					lastStrSize += snn.displayProfileData();
				}
				
				// TeCost.push_back(TeStat.cost());
				// TeAcc .push_back(TeStat.accuracy());
				costout << " " << std::setw(16) << TeStat.cost();
				if(snn.classification)	accout  << " " << std::setw(16) << TeStat.accuracy();
				
				if(TeStat.accuracy() > TeStat.maximumAccuracy())
				{// update learned parameters for better learning output
					snn.updateWvec();
					snn.updateDvec();
					snn.writeWeightDelay();
				}
				
				TeStat.updateMinCost();
				TeStat.updateMaxAcc();
				// plotAcc(static_cast<float>(correctTest)/numTest, TeAcc.size(), 2, "Accuracy", ".r");
				// plotCost(Jtest/numTest, TeAcc.size(), 1, "Cost", ".r");	
			}
			else
			{
				// if there is no testing, update the weight based on minimum cost
				if(TrStat.cost() < TrStat.minimumCost())
				{// update learned parameters for better learning output
					snn.updateWvec();
					snn.updateDvec();
					snn.writeWeightDelay();
				}
				
				TrStat.updateMinCost();
				TrStat.updateMaxAcc();
			}
			
			TrStat.reset();
			tocSum = 0;
			
			costout << std::endl;
			accout  << std::endl;
						
			if(iter < snn.maxIter - 1)	iter--;
		}
		if(iter == snn.maxIter)	slayerio::print("\nMaxIter reached.\n");
	}
	
	costout.close();
	accout .close();
		
	return 0;
}
/*
 * Author: Sumit Bam Shrestha
 * 26/09/2017 1:30 PM
 * Code for backpropagation in spiking neural network
 */
 
 
#include <mex.h>
#include <slayer.h>
#include <slnetwork.h>
#include <learningStat.h>
#include <tictoc.h>
#include <string>
#include <iomanip>
#include <yaml-cpp/yaml.h>

/*
 * for Ctrl+C code stopping feature
 * currently able to make it work in windows only
 */
#if defined _WIN32 || defined _WIN64 || defined __linux__
	extern "C" bool utIsInterruptPending();
	extern "C" bool utSetInterruptPending(bool);
	inline bool isCtrlCpressed()
	{	
		bool flag = utIsInterruptPending();
		utSetInterruptPending(false);
		return flag;
	}
// #elif defined __linux__
	// inline bool isCtrlCpressed()
	// {	return 0;	}	// for now this feature is available in windows only
#endif

/*
 * Mex function:
 * Usage: 
 * J = spikeLearn(network, weights, delay)
 * J = spikeLearn(network, randomSeed)
 * J = spikeLearn(network)
 */
 
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	/***** INITIALIZATION AND MEX LINKUPS *****/
    /* Macros for the ouput arguments */
	#define J_OUT plhs[0]
	J_OUT = mxCreateDoubleMatrix(0, 0, mxREAL);
	if(nlhs == 2 || nlhs == 4)	
	{
		#define ACC_OUT plhs[1]
		ACC_OUT = mxCreateDoubleMatrix(0, 0, mxREAL);
		if(nlhs == 4)
		{
			#define WEIGHT_OUT plhs[2]
			#define DELAY_OUT  plhs[3]
			WEIGHT_OUT = mxCreateDoubleMatrix(0, 0, mxREAL);
			DELAY_OUT  = mxCreateDoubleMatrix(0, 0, mxREAL);
		}
	}
	if(nlhs > 2 && nlhs !=4)	mexErrMsgTxt("Error: Cannot handle output arguments more than 2 that are not 4. Exiting process\n");


	/***** GET THE INPUT PARAMETERS *****/
	// slnetwork snn(nrhs, prhs);
	slnetwork snn(slayerConfig(nrhs, prhs));
		
	snn.configDisplay();
	
	const unsigned out = static_cast<unsigned>(snn.layer.size()-1);
	
	std::vector<unsigned> inputInd(snn.nSample);		// to store the indices of the input samples for a minibatch
	std::vector<unsigned> classInd(snn.nSample);		// to store the indices of the class values corresponding to input samples for a minibatch
	
	double tocSum = 0;
	double meanElapsedTime;
	
	std::vector<float> TrCost;
	std::vector<float> TeCost;
	std::vector<float> TrAcc;						// Training accuracy
	std::vector<float> TeAcc;						// Training accuracy
	learningStat TrStat;
	learningStat TeStat;
		
	bool converged = false;
	unsigned lastStrSize = 0;
	timeit profiler;
	
	snn.writeNetworkConfig();
	std::ofstream costout((snn.writeFileName()+"-cost.txt"    ).c_str(), std::ios::out);
	std::ofstream accout ((snn.writeFileName()+"-accuracy.txt").c_str(), std::ios::out);
	
	mexEvalString("close all;");
	
	for(unsigned iter=0; iter<=snn.maxIter; ++iter)
	{	
		if(isCtrlCpressed())
		{
			mexPrintf("\nCtrl+C detected at iteration %d.\nAborting operation now.\n", iter);
			break;
		}
		// In Linux server, drawnow resets Ctrl+C interrupt. So always call drawnow immediately after interrupt read.
		mexEvalString("drawnow;");
		
		if(snn.trScheduler.loadSamples(inputInd, classInd))	// if there is sample for the current epoch
		{	// Note:: if there are not enough samples (i.e. less than snn.nSample) to stride in last few samples, they will be excluded
			
			// Forward Propagation
			snn.layer[0].loadSpikes(snn.inPath, inputInd, snn.tSample);
			for(unsigned l=1; l <= out; ++l)	snn.layer[l].fwdProp();
			
			// Accuracy calculation
			TrStat.correctSamples += (snn.classification == true) ? snn.classifier.numCorrectIn(snn.layer[out], classInd) : 0;
			TrStat.numSamples     += classInd.size();
			
			// Back Propagation
			snn.loadDesiredSpikes(classInd);
			TrStat.costSum += snn.layer[out].error(snn.layerDes, snn.patternType);
			for(int l=out-1; l>=0; --l)	snn.layer[l].backProp();
			
			// Update parameters
			snn.updateParams(iter);
			
			// Print learning figures
			tocSum += profiler.record();			
			meanElapsedTime = tocSum / TrStat.numSamples * classInd.size();
			slayerio::clear(lastStrSize);	
			lastStrSize += displayStats(TrStat, TeStat, iter, meanElapsedTime);
			lastStrSize += snn.displayProfileData();
			
			// Early stopping
			if(TrStat.cost() < snn.minCost && TrStat.cost() > 0)	converged = true; 
			if(converged) 	
			{
				slayerio::print("\nLearning Converged.\n");		
				break;
			}
		}
		else
		{
			TrCost.push_back(TrStat.cost());
			TrAcc .push_back(TrStat.accuracy());
			costout << std::setw(16) << TrStat.cost();
			if(snn.classification)	accout  << std::setw(16) << TrStat.accuracy();
			if(snn.classification)	plotAcc(TrStat.accuracy(), TrAcc.size(), 2, "Accuracy", ".b");
			plotCost(TrStat.cost(), TrAcc.size(), 1, "Cost", ".b");
			
			if(snn.classification == false && TrAcc.size()%100 == 0)	snn.layer[out].plotRaster(5);
			
			if(snn.testing)
			{
				TrStat.updateMinCost();
				TrStat.updateMaxAcc();
				
				TeStat.reset();
				while(snn.teScheduler.loadSamples(inputInd, classInd))	// while there are samples in the training list
				{	// Note:: if there are not enough samples (i.e. less than snn.nSample) to stride in last few samples, they will be excluded
					
					mexEvalString("drawnow;");
					
					snn.layer[0].loadSpikes(snn.inPath, inputInd, snn.tSample);
					for(unsigned l=1; l <= out; ++l)	snn.layer[l].fwdProp();
					
					snn.loadDesiredSpikes(classInd);
					TeStat.costSum += snn.layer[out].error(snn.layerDes, snn.patternType);
					
					// Accuracy calculation
					TeStat.correctSamples += (snn.classification == true) ? snn.classifier.numCorrectIn(snn.layer[out], classInd) : 0;
					TeStat.numSamples     += static_cast<unsigned>(classInd.size());
					
					// Print learning figures
					slayerio::clear(lastStrSize);
					lastStrSize += displayStats(TrStat, TeStat, iter, profiler.record());
					lastStrSize += snn.displayProfileData();
				}
				
				TeCost.push_back(TeStat.cost());
				TeAcc .push_back(TeStat.accuracy());
				costout << " " << std::setw(16) << TeStat.cost();
				if(snn.classification)	accout  << " " << std::setw(16) << TeStat.accuracy();
				plotCost(TeStat.cost(), TeAcc.size(), 1, "Cost", ".r");
				if(snn.classification)	plotAcc(TeStat.accuracy(), TeAcc.size(), 2, "Accuracy", ".r");
				
				if(TeAcc.back() > TeStat.maximumAccuracy())
				{// update learned parameters for better learning output
					snn.updateWvec();
					snn.updateDvec();
					snn.writeWeightDelay();
				}
				
				TeStat.updateMinCost();
				TeStat.updateMaxAcc();
			}
			else
			{
				// if there is no testing, update the weight based on minimum cost
				if(TrStat.cost() < TrStat.minimumCost())
				{// update learned parameters for better learning output
					snn.updateWvec();
					snn.updateDvec();
					snn.writeWeightDelay();
				}
				
				TrStat.updateMinCost();
				TrStat.updateMaxAcc();
			}
			
			TrStat.reset();
			tocSum = 0;
			
			if(iter < snn.maxIter - 1)	iter--;
		}
		if(iter == snn.maxIter)	mexPrintf("\nMaxIter reached.\n");
	}
	mexEvalString("figure(1), hold off");
	if(snn.classification)	mexEvalString("figure(2), hold off");
	
	std::vector<float> weight = snn.getWvec();
	std::vector<float> delay  = snn.getDvec();
	
	/***** ASSIGN OUTPUT PARAMETERS *****/
	// for now assign spike times to J
	// snn.layer[0].copySpikeTo(J_OUT);
	// snn.layer[1].copySpikeTo(J_OUT);
	// snn.layer[out-1].copySpikeTo(J_OUT);
	// snn.layer[out].copySpikeTo(J_OUT);
	// snn.layerDes.copySpikeTo(J_OUT);
	// snn.layer[0].copyTo(J_OUT, A);
	// snn.layer[1].copyTo(J_OUT, U);
	// snn.layer[1].copyTo(J_OUT, DELTA);
	// snn.layer[0].copyTo(J_OUT, E);
	// snn.layer[2].copyTo(J_OUT, E);
	// snn.layer[1].copyTo(J_OUT, SPIKE);
	// snn.layer[out-1].copyTo(J_OUT, DELTA);
	// snn.layer[out-1].copyTo(J_OUT, A);
	// snn.layer[out-1].copyTo(J_OUT, E);
	// snn.layer[out-1].copyTo(J_OUT, U);
	// snn.layer[out-1].copyTo(J_OUT, ADOT);
	// snn.layer[out].copyTo(J_OUT, DELTA);
	// snn.layer[out].copyTo(J_OUT, E);
	// snn.layer[out].copyTo(J_OUT, U);
	// snn.layerDes.copyTo(J_OUT, U);
	if(snn.testing)	mxCopy2(J_OUT, TrCost, TeCost);
	else			mxCopy (J_OUT, TrCost);
	
	if(nlhs == 2 || nlhs == 4)	
	{
		if(snn.testing)	mxCopy2(ACC_OUT, TrAcc, TeAcc);
		else			mxCopy (ACC_OUT, TrAcc);
		if(nlhs == 4)
		{
			mxCopy(WEIGHT_OUT, weight);
			mxCopy(DELAY_OUT , delay);
		}
	}
	
	costout.close();
	accout .close();
		
	return;
}
 

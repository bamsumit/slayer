/*
 * Author: Sumit Bam Shrestha
 * 20/04/2018 6:00 PM
 * Code for running spiking neural network
 */
 
 
#include <mex.h>
#include <slayer.h>
#include <slnetwork.h>
#include <learningStat.h>
#include <tictoc.h>
#include <string>
#include <iomanip>
#include <yaml-cpp/yaml.h>

/*
 * for Ctrl+C code stopping feature
 * currently able to make it work in windows only
 */
#if defined _WIN32 || defined _WIN64 || defined __linux__
	extern "C" bool utIsInterruptPending();
	extern "C" bool utSetInterruptPending(bool);
	inline bool isCtrlCpressed()
	{	
		bool flag = utIsInterruptPending();
		utSetInterruptPending(false);
		return flag;
	}
// #elif defined __linux__
	// inline bool isCtrlCpressed()
	// {	return 0;	}	// for now this feature is available in windows only
#endif

/*
 * Mex function:
 * Usage: 
 * spikes = spikeLearn(network, weights, delay)
 */

void plotConfusionMat(std::vector<std::vector<double>> mat, unsigned figureID, std::string plotTitle)
{
	static double az = 0;	// azimuth
	double el = 30;			// elevation
	// MATLAB: figure(figureID)
	std::stringstream sout;
	sout << "figure(" << figureID << ");";
	mexEvalString(sout.str().c_str());	// call figure(figureID) in MATLAB space
	bar3(mat);
	// MATLAB: title(plotTitle);
	sout.str().clear();
	sout << "title('" << plotTitle << "');";
	mexEvalString(sout.str().c_str());
	mexEvalString("xlabel('True Class')");
	mexEvalString("ylabel('Predicted Class')");
	mexEvalString("zlabel('Count')");
	// MATLAB: axis([-1 M+1 -1 N+1 0 Inf]);
	sout.str().clear();
	sout << "axis([-1 " << mat[0].size() + 1 << " -1 " << mat.size() + 1 << " 0 Inf]);";
	mexEvalString(sout.str().c_str());
	mexEvalString("axis vis3d;");
	// MATLAB: view(az, el);
	sout.str().clear();
	sout << "view(" << az << ", " << el << ");";
	mexEvalString(sout.str().c_str());
	mexEvalString("drawnow");
	mexEvalString("pause(0.1)");
	az += 5;
}
 
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	/***** INITIALIZATION AND MEX LINKUPS *****/
    /* Macros for the ouput arguments */
	#define SPIKE_OUT plhs[0]
	SPIKE_OUT = mxCreateDoubleMatrix(0, 0, mxREAL);
	if(nlhs > 1)	mexErrMsgTxt("Error: Cannot handle output arguments more than 1. Exiting process\n");

	/***** GET THE INPUT PARAMETERS *****/
	// slnetwork snn(nrhs, prhs);
	slnetwork snn(slayerConfig(nrhs, prhs));
		
	snn.configDisplay();
	
	const unsigned out = static_cast<unsigned>(snn.layer.size()-1);
	
	std::vector<unsigned> inputInd(snn.nSample);		// to store the indices of the input samples for a minibatch
	std::vector<unsigned> classInd(snn.nSample);		// to store the indices of the class values corresponding to input samples for a minibatch
	
	learningStat Stat;
	
	mexEvalString("drawnow;");
		
	std::vector<std::vector<float>> spikeData;	
	
	// declare confusion matrix
	std::vector<std::vector<double>> confMat(snn.layer[out].info.numNeurons(), std::vector<double>( snn.layer[out].info.numNeurons() ));
	for(unsigned m=0; m<confMat.size(); ++m)
		for(unsigned n=0; n<confMat[m].size(); ++n)
			confMat[m][n] = 0.0;
	
	mexEvalString("close all;");
	
	while(snn.teScheduler.loadSamples(inputInd, classInd))	// while there are samples in the training list
	{	// Note:: if there are not enough samples (i.e. less than snn.nSample) to stride in last few samples, they will be excluded
	
		if(isCtrlCpressed())
		{
			mexPrintf("\nCtrl+C detected.\nAborting operation now.\n");
			break;
		}
		// In Linux server, drawnow resets Ctrl+C interrupt. So always call drawnow immediately after interrupt read.
		mexEvalString("drawnow;");
		
		// Forward Propagation
		snn.layer[0].loadSpikes(snn.inPath, inputInd, snn.tSample);
		for(unsigned l=1; l <= out; ++l)	snn.layer[l].fwdProp();
		
		// Copy Spike Data
		spikeData.push_back(snn.layer[out].get(SPIKE));
		// snn.layer[out].plotRaster(5);
		
		// Accuracy calculation
		if(snn.classification == true)
		{
			std::vector<unsigned> classOut = snn.classifier.outputClass(snn.layer[out], classInd);
			Stat.correctSamples += snn.classifier.numCorrectIn(classOut, classInd);
			Stat.numSamples     += classInd.size();
			// update confusion matrix
			for(unsigned i=0; i<classInd.size(); ++i)
				confMat[classOut[i]][classInd[i]]++;
			
			// plot confusion matrix
			plotConfusionMat(confMat, 1, "Confusion Matrix");
			
			mexCallMATLAB(0, NULL, 0, NULL, "why");
		}
	}
	
	// Accuracy calculation
	if(snn.classification)	slayerio::print("%d out of %d correct\n", Stat.correctSamples, Stat.numSamples);
	
	/***** ASSIGN OUTPUT PARAMETERS *****/
	mxCopy(SPIKE_OUT, spikeData);
	mxSetN(SPIKE_OUT, mxGetM(SPIKE_OUT) * mxGetN(SPIKE_OUT) / snn.layer[out].info.numNeurons());
    mxSetM(SPIKE_OUT, snn.layer[out].info.numNeurons());
	
	// deInitializeMultiGPU(deviceID);
		
	return;
}
 

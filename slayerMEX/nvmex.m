function nvmex(cuFileName)
% NVMEX Compiles and links a CUDA file for MATLAB usage
% NVMEX(FILENAME) will create a MEX-File (also with the name FILENAME) by
% invoking the CUDA compiler, nvcc, and then linking with the MEX
% function in MATLAB.
% Copyright 2009 The MathWorks, Inc.
% !!! Modify the paths below to fit your own installation !!!


if ispc % Windows
    CUDA_PATH              = 'C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v9.0';
	CUDA_LIB_Location      = 'C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v9.0\lib\x64';  	%'C:\CUDA\lib';
    SLAYER_PATH            = '..';
	YAML_PATH              = '';
	Host_Compiler_Location = '-ccbin "C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\bin"'; %'-ccbin "C:\Program Files\Microsoft Visual Studio 9.0\VC\bin"';
    PIC_Option             = '';
    nvccPath               = '';
	libutPath              = 'C:\Program Files\MATLAB\R2014b\extern\lib\win64\microsoft\libut.lib';
    libutPathStr           = ['-L' libutPath];
else % Mac and Linux (assuming gcc is on the path)
	CUDA_PATH 			   = '/usr/local/cuda';
    CUDA_LIB_Location      = '/usr/local/cuda/lib64';
	SLAYER_PATH 		   = '..';
	YAML_PATH 			   = '/usr/local';
    Host_Compiler_Location = '';
    PIC_Option             = ' --compiler-options -fPIC ';
    nvccPath               = '/usr/local/cuda/bin/'; 
	libutPathStr           = '';
end

% !!! End of things to modify !!!

[~, filename]   = fileparts(cuFileName);
HEADERS = ['-I ' CUDA_PATH '/include -I ' ...
                 YAML_PATH '/include -I ' ...
                 SLAYER_PATH '/include -I ' ...
                 addquotes([matlabroot filesep 'extern' filesep 'include'])];
LIBS = ['-L ' CUDA_LIB_Location];
DLLS = ['-lcudadevrt -lcublas -lcudnn -lcurand'];
CXXFLAGS = ['-arch=sm_60 -rdc=true -std=c++11 -O2 -use_fast_math'];
YAML_LIBS = [YAML_PATH '/lib/libyaml-cpp.a'];
DIRECTIVES = ['-DSLAYERMEX'];
    
genmake(CUDA_PATH, CUDA_LIB_Location, SLAYER_PATH , YAML_PATH, Host_Compiler_Location, matlabroot, nvccPath, PIC_Option, CXXFLAGS, DIRECTIVES);

if strcmp(filename, 'clean') == 1
    system('make clean');
    return
end

% system('make');
disp('Invoking make (This might take some time)');
[status, result] = system('make');
disp(result)
if status ~= 0
    error 'Error invoking make';
end 

src = dir([SLAYER_PATH filesep 'libmex' filesep '*.o' ]);
libmexfiles = [];
for i=1:length(src)
    if length(libmexfiles)==0
        libmexfiles =                  [addquote([SLAYER_PATH filesep 'libmex' filesep src(i).name])];
    else
        libmexfiles = [libmexfiles ', ' addquote([SLAYER_PATH filesep 'libmex' filesep src(i).name])];
    end
end

nvccCommandLine = [nvccPath 'nvcc --compile ' ...
                    cuFileName ' ' Host_Compiler_Location ...
                    ' -o ' filename '.o ' ...
                    PIC_Option ' ' CXXFLAGS ' ' HEADERS ' -DSLAYERMEX'];

% this is necessary if you are using dynamic parallelism and the object
% file is linked by external compiler
% nvccDlinkCmd    = [nvccPath 'nvcc -arch=sm_35 -dlink ' filename '.o -o d' filename '.o ' PIC_Option];
nvccDlinkCmd    = [nvccPath 'nvcc ' ...
                   filename '.o ' Host_Compiler_Location ...
                   ' -o d' filename '.o' ...
                   PIC_Option ' ' CXXFLAGS ' -dlink'];

mexCommandLine = ['mex (' addquote([filename '.o']) ', ' ...
                             addquote(['d' filename '.o']) ', ' ...
                             libmexfiles ', ' ...
                             addquote(['-L' CUDA_LIB_Location]) ', ' ...
                             addquote(['-L.']) ', ' ...
                             addquote('-lcudart') ', ' ...
                             addquote('-lcublas') ', ' ...
                             addquote('-lcudnn') ', ' ...
                             addquote('-lcudadevrt') ', ' ...
                             addquote('-lcublas_device') ', ' ...
                             addquote('-lcurand') ', ' ...
                             addquote('-lyaml-cpp') ', ' ...
                             addquote(libutPathStr) ', ' addquote('-lut') ')'];

disp(nvccCommandLine);
[status, result] = system(nvccCommandLine);
disp(nvccDlinkCmd);
[~, ~]           = system(nvccDlinkCmd);    % for dynamic parallelism
assignin('caller', 'compileStatus', status);
assignin('caller', 'compileResult', result);
if status ~= 0
    disp(result);
    error 'Error invoking nvcc';
end
disp(mexCommandLine);
eval(mexCommandLine);

clear mex;
end

function str = addquote(string)
str = ['''' string ''''];
end

function str = addquotes(string)
str = ['"' string '"'];
end

function genmake(CUDA_PATH, CUDA_LIB_LOCATION, SLAYER_PATH , YAML_PATH, HOST_COMPILER_LOCATION, MATLAB_ROOT, NVCC_PATH, PIC_OPTION, CXXFLAGS, DIRECTIVES)          

fid = fopen('makefile', 'w');

fprintf(fid, 'CUDA_PATH         := %s\n', CUDA_PATH         );
fprintf(fid, 'CUDA_LIB_LOCATION := %s\n', CUDA_LIB_LOCATION );
fprintf(fid, 'SLAYER_PATH       := %s\n', SLAYER_PATH       );
fprintf(fid, 'YAML_PATH         := %s\n', YAML_PATH         );
fprintf(fid, 'HOST_COMPILER_LOCATION:= %s\n', HOST_COMPILER_LOCATION);
fprintf(fid, 'MATLAB_ROOT       := %s\n', MATLAB_ROOT       );
fprintf(fid, 'NVCC_PATH         := %s\n', NVCC_PATH         );
fprintf(fid, 'PIC_OPTION        := %s\n', PIC_OPTION        );
fprintf(fid, 'CXXFLAGS          := %s\n', CXXFLAGS          );
fprintf(fid, 'DIRECTIVES        := %s\n', DIRECTIVES        );

fprintf(fid, '\n');
fprintf(fid, 'CXX       := nvcc\n');
fprintf(fid, 'TARGET    := slayermex\n');
fprintf(fid, 'SRC_CPP   := $(wildcard $(SLAYER_PATH)/src/*.cpp)\n');
fprintf(fid, 'OBJ_CPP   := $(patsubst $(SLAYER_PATH)/src/%%.cpp, $(SLAYER_PATH)/libmex/%%.o, $(SRC_CPP))\n');
fprintf(fid, 'SRC_CU    := $(wildcard $(SLAYER_PATH)/src/*.cu)\n');
fprintf(fid, 'OBJ_CU    := $(patsubst $(SLAYER_PATH)/src/%%.cu, $(SLAYER_PATH)/libmex/%%.o, $(SRC_CU))\n');
fprintf(fid, 'DLINK_CU  := $(patsubst $(SLAYER_PATH)/src/%%.cu, $(SLAYER_PATH)/libmex/d%%.o, $(SRC_CU))\n');
fprintf(fid, 'HEADERS   := -I $(CUDA_PATH)/include -I$(YAML_PATH)/include -I $(SLAYER_PATH)/include -I $(MATLAB_ROOT)/extern/include\n');
fprintf(fid, 'LIBS      := -L $(CUDA_LIB_LOCATION)\n');

fprintf(fid, '\n');
fprintf(fid, 'all: $(TARGET)\n');
fprintf(fid, '\n');
fprintf(fid, '$(SLAYER_PATH)/libmex/%%.o: $(SLAYER_PATH)/src/%%.cpp\n');
fprintf(fid, '\t$(NVCC_PATH)$(CXX) --compile $(HOST_COMPILER_LOCATION) $(PIC_OPTION) $(CXXFLAGS) $(HEADERS) $(LIBS) $< -o $@ $(DIRECTIVES)\n');
fprintf(fid, '\n');
fprintf(fid, '$(SLAYER_PATH)/libmex/%%.o: $(SLAYER_PATH)/src/%%.cu\n');
fprintf(fid, '\t$(NVCC_PATH)$(CXX) --compile $(HOST_COMPILER_LOCATION) $(PIC_OPTION) $(CXXFLAGS) $(HEADERS) $(LIBS) $< -o $@ $(DIRECTIVES)\n');
fprintf(fid, '\n');
fprintf(fid, '$(SLAYER_PATH)/libmex/d%%.o: $(SLAYER_PATH)/libmex/%%.o\n');
fprintf(fid, '\t$(NVCC_PATH)$(CXX) $(HOST_COMPILER_LOCATION) $(PIC_OPTION) $(CXXFLAGS) -dlink $< -o $@\n');
fprintf(fid, '\n');
fprintf(fid, '$(TARGET): $(OBJ_CPP) $(OBJ_CU) $(DLINK_CU)\n');
fprintf(fid, '\n');
fprintf(fid, '.phony: clean\n');
fprintf(fid, '\n');
fprintf(fid, 'clean:\n');
fprintf(fid, '\trm *.o\n');
fprintf(fid, '\trm $(SLAYER_PATH)/libmex/*.o\n');

fclose(fid);
end
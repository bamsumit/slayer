clear snn;
clear mex;

% unzip the input and output binary spike files
if ~exist('NMNISTsmall')
    unzip NMNISTsmall.zip;
end

rseed = randi(10000);
fprintf('\n\nSeed = %d\n', rseed);
rng(rseed); %rng(50); % seed for random generator

snn.Ts = 1;
snn.tSample = 350;  % 300ms sample data
snn.nSample = 10;
snn.tEnd = snn.tSample * snn.nSample;

snn.nnArch = '34x34x2-500-500-10'; %'34x34-5c3-2a-500-10';
snn.theta  = 10*ones(1, 4);
snn.sigma  = [0 0 0 0];
snn.wScale = [0.5 1 1 0]; % [0.5 1 1 0];

snn.eta    = 0.01;
snn.etaD   = 0.01;
snn.tauSr  = 1;
snn.tauRef = 1;
snn.neuronType = 'SRMALPHA';
snn.maxIter= 200 * 1000 / snn.nSample;
snn.minCost= 0.001; 
snn.outPath= 'OutFiles/';   % is not being used currently
snn.inPath = 'NMNISTsmall/';
snn.desiredPath = 'NMNISTsmall/Desired_10_60/';
snn.trainList = 'NMNISTsmall/train1K.txt';
snn.testList = 'NMNISTsmall/test100.txt';
snn.testing = true;
snn.patternType = 'NumSpikes';%'AppxSpikeTime';% 'SpikeTime';
snn.tgtSpikeRegion.start = 0;
snn.tgtSpikeRegion.stop  = 300;
snn.tgtSpikeCount.true = 60;
snn.tgtSpikeCount.false = 10;

snn.updateMode = 'NADAM'; % 'Rmsprop'; % 'GradientDescent';
snn.preferredGPU = 0;

addpath('../slayerMEX');
% J = spikelearn(snn, weight, delay);
% [J Acc] = spikelearn(snn, rseed);
[J Acc weight delay] = spikelearn(snn, rseed);
% J = spikelearn(snn);
rmpath('../slayerMEX');

% figure(1), semilogy(J);
% figure(2), plot(Acc);
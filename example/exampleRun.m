clear all;

load('exampleNetwork.mat');

% unzip the input and output binary spike files
if ~exist('NMNISTsmall')
    unzip NMNISTsmall.zip;
end

snn.Ts = 1;
snn.tSample = 350;  % 300ms sample data
snn.nSample = 10;
snn.tEnd = snn.tSample * snn.nSample;

snn.inPath = 'NMNISTsmall/';
snn.desiredPath = 'NMNISTsmall/Desired_10_60/';
snn.trainList = 'NMNISTsmall/train1K.txt';
snn.testList = 'NMNISTsmall/test100.txt';
snn.preferredGPU = 0;

addpath('../slayerMEX/');
spike = spikerun(snn, weight, delay);
rmpath('../slayerMEX/');
% spike = reshape(spike, [size(spike, 1)/snn.tEnd size(spike, 2)*snn.tEnd]);
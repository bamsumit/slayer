clear snn;
clear mex;

rseed = randi(10000);

snn.Ts = 1; %resolution of the time
snn.tSample = 1900;
snn.nSample = 1; %minibatch

snn.nnArch = '200-256-200';
snn.theta  = 10*ones(1, 3); % threshould 
snn.sigma  = zeros(size(snn.theta));
snn.wScale = 3*ones(size(snn.theta)); 

snn.eta    = 1;
snn.etaD   = 0; %no delay learning
snn.tauSr  = 1; %time constant(spike response)
snn.tauRef = 1; %time constant(refractory freq)
snn.tauRho = 1;
% snn.scaleRho = 0.5;
snn.neuronType = 'SRMALPHA';
snn.maxIter= 10000;
snn.minCost= 0.001; 
snn.inPath = 'oxford/input/';
snn.outPath= 'OutFiles/';
snn.desiredPath = 'oxford/target/';
snn.patternType = 'SpikeTime'; 
snn.trainList = 'oxford/input/train.txt';
snn.testList = 'oxford/input/test.txt';
snn.testing = false;
snn.updateMode = 'NADAM'; % 'NADAM'; % 'Rmsprop'; % 'GradientDescent';
snn.gradAlgo = 'SLAYER'; %'SLAYER_REFRACTORY';
snn.classification = false;
snn.preferredGPU = 0;
snn.profile = false; %true;

addpath('../slayerMEX');
[J Acc weight delay] = spikelearn(snn, rseed);
rmpath('../slayerMEX');